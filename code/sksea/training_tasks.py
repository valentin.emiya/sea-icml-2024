"""
Functions for reproducing experiments in Sparse Convex Optimization via Adaptively Regularized Hard Thresholding paper
https://arxiv.org/pdf/2006.14571.pdf
"""
import itertools
# Python imports
from collections import defaultdict
from copy import deepcopy
import gc
import inspect
import os
from pathlib import Path
import socket
from time import time
from typing import List, Dict, Callable, Iterable

# Modules imports
import click
from loguru import logger
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
import ray

# Script imports
from sksea.algorithms import (els_fast, niht, omp_fast, ompr_fast, sea, omp, ompr, els, amp, iht, es, PAS, sea_fast,
                              rea, htp, htp_fast)
from sksea.dataset_operator import DatasetOperator, Task, RESULT_PATH
from sksea.plot_icml import plot_ml_paper
from sksea.utils import ALGOS_PAPER_TT, PAPER_LAYOUT

ROOT = Path(os.path.abspath(inspect.getfile(inspect.currentframe()))).parent
PLOT_PATH = ROOT / "results/training_tasks_plot"
PLOT_PATH_PAPER = ROOT / "results/training_tasks_plot_paper"
ALL = 'ALL'
ALGOS_TYPE = ('seafast', 'sea', 'iht', 'omp', 'es', 'amp')


def run_experiments(algorithms, datasets=DatasetOperator.REGRESSIONS_NAME, factors=(1, 2, 4, 8, 16, 100), resume=True,
                    reverse=False, sparsities=(0,)) -> None:
    """
    Run all simulations on regression datasets with the provided algorithms

    :param (Dict[str, Callable]) algorithms: Dictionary linking each algorithm to use to its name
    :param (Iterable[DatasetOperator) datasets:
    :param (Iterable[int]) factors:
    :param (bool) resume: If True, don't overwrite previous results
    """
    RESULT_PATH.mkdir(exist_ok=True, parents=True)
    for name in datasets:
        dataop = DatasetOperator(name, no_loading=True)
        for algo_name, algo in algorithms.items():
            if 0 in sparsities:
                if reverse:
                    iterator = range(dataop.k_max, 0, -1)
                else:
                    iterator = range(1, dataop.k_max + 1)
            else:
                iterator = sparsities
            for n_nonzero in iterator:
                if 'SEA' in algo_name or 'IHT' in algo_name or 'HTP' in algo_name or 'REA' in algo_name:  # For SEA and IHT we use various iteration number
                    for factor in factors:
                        sea_name = f'{algo_name}x{factor}'
                        n_iter = n_nonzero * factor
                        solve_problem(dataop, lambda *args, **kwargs: algo(*args, **kwargs, n_iter=n_iter), n_nonzero,
                                      sea_name, resume)
                else:
                    solve_problem(dataop, algo, n_nonzero, algo_name, resume)
                gc.collect()  # Force linop_s (from optimize function of algorithm file) to be removed from the memory


def solve_problem(dataop, algo, n_nonzero, algo_name, resume) -> None:
    """
    Solve the problem stored in linop with the provided algorithm

    :param (DatasetOperator) dataop: Linear operator representing the problem matrix and the labels.
        Everything must be already preprocessed
    :param (Callable) algo: Algorithm to run in order to solve min_x 1/2 ||D * x - y||_2^2 w.r.t ||x||_0 = n_nonzero
        D is linop and y is linop.y
    :param (int) n_nonzero: Size of the wanted support
    :param (str) algo_name: Name of the provided algorithm
    :param (bool) resume: If True, don't overwrite previous results
    """
    idx = 0
    out_file = RESULT_PATH / f"{algo_name}_{idx}_{dataop.name}_{n_nonzero}.npz"
    if out_file.is_file() and resume:
        logger.info(f"{algo_name} for solving {dataop.name} with k={n_nonzero} was already done")
    else:
        logger.info(f"Using {algo_name} for solving {dataop.name} with k={n_nonzero}")
        dataop.load()
        start_time = time()
        out = algo(linop=dataop.linop, y=dataop.y, n_nonzero=n_nonzero, f=dataop.f,
                   grad_f=dataop.grad_f, is_mse=dataop.task == Task.REGRESSION,
                   optimizer="cg")
        time_spent = time() - start_time
        outputs = out if isinstance(out[0], tuple) else (out,)
        for idx, (x, res_norm, *args) in enumerate(outputs):
            np.savez(RESULT_PATH / f"{algo_name}_{idx}_{dataop.name}_{n_nonzero}.npz",
                     x=x, time_spent=np.array([time_spent]),
                     algo_name=algo_name, dataset=dataop.name, res_norm=np.array(res_norm),
                     history=args)


def get_algo_dict(use_seafast=False, use_sea=False, use_omp=False, use_iht=False, use_amp=False, use_es=False,
                  n_iter=None, sea_params=None, lip_factor=2 * 0.9, **params) -> Dict[str, Callable]:
    """
    Get a dictionary with all available algorithms and all SEA variants.

    :param (bool) use_seafast: If True, add SEAFAST and its variants to the algorithms' dictionary
    :param (bool) use_sea: If True, add SEA and its variants to the algorithms' dictionary
    :param (bool) use_omp: If True, add OMP and its variants to the algorithms' dictionary
    :param (bool) use_iht: If True, add IHT to the algorithms' dictionary
    :param (bool) use_amp: If True, add AMP to the algorithms' dictionary
    :param (bool) use_es: If True, add ES to the algorithms' dictionary
    :param (Optional[int]) n_iter: Maximal number of iteration the algorithm can do
    :param (Optional[Dict]) sea_params: Parameters to pass to the SEA algorithm
    :param (float) lip_factor: Lipschitz factor for the gradient step
    :return: A dictionary linking each algorithm to its name
    """
    if sea_params is None:
        sea_params = dict(return_both=True)
    iter_params = dict(n_iter=n_iter) if n_iter is not None else dict()  # For OMP-like algorithms
    grad_step_params = dict(lip_fact=lip_factor)
    algos_available = {}
    algo_inits = (None, omp, ompr, els, els_fast, omp_fast)
    if use_iht:
        for init in algo_inits:
            for algo, algo_name in ((iht, 'IHT'), (htp, 'HTP'), (niht, 'NIHT')):
                current_name = algo_name
                if init is not None:
                    current_name += f'-{init.__name__}'
                if lip_factor != 2 * 0.9:
                    current_name += f'-{lip_factor}'
                algos_available[current_name] = lambda *args, algo_init=init, algo_to_run=algo, **kwargs: algo_to_run(
                    *args, algo_init=algo_init, **kwargs, **params, **grad_step_params)
    if use_omp:
        algos_available.update({
            'OMP': lambda *args, **kwargs: omp(*args, **kwargs, **params, **iter_params),
            'OMPR': lambda *args, **kwargs: ompr(*args, **kwargs, **params, **iter_params),
            'ELS': lambda *args, **kwargs: els(*args, **kwargs, **params, **iter_params),
            'ELSFAST': lambda *args, **kwargs: els_fast(*args, return_history=True, **kwargs, **params,
                                                        **iter_params),
            'OMPFAST': lambda *args, **kwargs: omp_fast(*args, return_history=True, **kwargs, **params,
                                                        **iter_params),
            'OMPRFAST': lambda *args, **kwargs: ompr_fast(*args, return_history=True, **kwargs, **params,
                                                          **iter_params),
        })
    if use_amp:
        algos_available['AMP'] = lambda *args, **kwargs: amp(*args, alpha=1, return_both=True,
                                                             **kwargs, **params, **iter_params)
    if use_sea:  # Get all SEA versions
        for opti in ['all', None, 'last']:  # , 'half', -1, -2]:
            for init in algo_inits:
                for pas in PAS.keys():
                    for full_explo in (True, False):
                        current_name = 'SEA'
                        if full_explo:
                            current_name += '-full'
                        if init is not None:
                            current_name += f'-{init.__name__}'
                        if opti is not None:
                            current_name += f'-{opti}'
                        if pas is not None:
                            current_name += f'-{pas}'
                        algos_available[current_name] = \
                            (lambda *args, algo_init=init, optimize_sea=opti, pas_sea=pas, full_explo_sea=full_explo,
                                    **kwargs: sea(*args, algo_init=algo_init, optimize_sea=optimize_sea, pas=pas_sea,
                                                  full_explo_sea=full_explo, **kwargs, **sea_params, **params,
                                                  ))
    if use_seafast:
        for init in itertools.chain(algo_inits, ("rd",)):
            for algo, algo_name in ((sea_fast, 'SEAFAST'), (htp_fast, 'HTPFAST'),
                                    (lambda *args, **kwargs: sea_fast(*args, **kwargs, equal_to_random=True),
                                     "SEAFAST-rc")):
                current_name = algo_name
                if init == "rd":
                    current_name += f'-{init}'
                    init = None
                elif init is not None:
                    current_name += f'-{init.__name__}'
                if lip_factor != 2 * 0.9:
                    current_name += f'-{lip_factor}'
                algos_available[current_name] = lambda *args, algo_init=init, algo_to_run=algo, **kwargs: algo_to_run(
                    *args, algo_init=algo_init, return_history=True,
                    **kwargs, **sea_params, **params, **grad_step_params)
    if use_es:
        algos_available['ES'] = lambda *args, **kwargs: es(*args, **kwargs, **params)
        algos_available['REA'] = lambda *args, **kwargs: rea(*args, **kwargs, **params, return_history=True)
    return algos_available


def plot_results(datasets=DatasetOperator.BINARY_NAME + DatasetOperator.REGRESSIONS_NAME, presentation=False):
    """
    Plot all results by dataset like in the paper using the generated results by `solve_problem` function.

    :param (Iterable[str]) datasets: Datasets with results to plot
    :param (bool) presentation: If True, plot for presentation
    """
    PLOT_PATH.mkdir(exist_ok=True)
    datasets_sorted = list(datasets)
    datasets_sorted.sort(key=lambda elt: DatasetOperator.ORDER.index(elt))
    for data_name in datasets_sorted:
        logger.info(f"Plotting for {data_name}")
        dataset = DatasetOperator(data_name)
        n = dataset.K_MAX[dataset.name] + 1  # dataset.linop.A.shape[1] + 1
        res = defaultdict(lambda: np.zeros(n))
        res_best = defaultdict(lambda: np.zeros(n))
        time_spent = defaultdict(lambda: np.zeros(n))
        iterations = defaultdict(lambda: np.zeros(n))
        paths = list(RESULT_PATH.glob(f"*{data_name}*"))
        paths.sort()
        legend = dict(
            legend_title="Algorithms",
            xaxis_title="Sparsity",
            xaxis_range=(1, n - 1),
            title=data_name,
        )
        for path in paths:
            try:
                with np.load(str(path), allow_pickle=True) as file:
                    stem = path.stem
                    n_nonzeros = int(stem.rsplit('_', 1)[1])
                    data_idx = stem.find(data_name)
                    algo_name, str_idx = stem[:data_idx - 1].rsplit('_', 1)
                    idx = int(str_idx)
                    algo_name = algo_name + "_BEST" if idx == 1 else algo_name
                    res[algo_name][n_nonzeros] = dataset.f(file["x"])
                    iterations[algo_name][n_nonzeros] = file["res_norm"].shape[
                        0]  # 1 if len(file["res_norm"].shape) == 0 else
                    if presentation:
                        if algo_name not in ("ELS", "OMP", "OMPR", "IHTx1000", "SEA-init-els-opti-allx1000_BEST",
                                             "SEA-opti-allx1000_BEST"):
                            continue
                        algo_name = algo_name.split('x')[0]
                        if "SEA" in algo_name:
                            algo_name = algo_name[:-len("-opti-all")]
                        if algo_name == "SEA":
                            algo_name = "SEA-init-zero"
                    if (idx == 0 and 'SEA' not in algo_name) or idx == 1:
                        time_spent[algo_name][n_nonzeros] = file["time_spent"][0]
                        res_best[algo_name][n_nonzeros] = dataset.f(file["x"])
                    if 'SEAFAST' in algo_name:
                        pass
                        # file["history"][0].get_top()
            except Exception as e:
                logger.error(f"Couldn't open {path}")

        # fig = px.line(res)
        # fig.update_layout(yaxis_title="Loss", **legend)
        # fig.write_html(PLOT_PATH / f"loss_{data_name}.html")
        fig = px.line(res_best)
        fig.update_layout(yaxis_title="Loss", **legend)
        fig.write_html(PLOT_PATH / f"best_loss_{data_name}.html")
        fig = px.line(time_spent)
        fig.update_layout(yaxis_title="Time spent", **legend)
        fig.write_html(PLOT_PATH / f"time_spent_{data_name}.html")
        fig = px.line(iterations)
        fig.update_layout(yaxis_title="Iterations", **legend)
        fig.write_html(PLOT_PATH / f"iterations_{data_name}.html")


def plot_results_paper(datasets=DatasetOperator.BINARY_NAME + DatasetOperator.REGRESSIONS_NAME):
    PLOT_PATH_PAPER.mkdir(exist_ok=True)
    datasets_sorted = list(datasets)
    datasets_sorted.sort(key=lambda elt: DatasetOperator.ORDER.index(elt))
    for data_name in datasets_sorted:
        logger.info(f"Plotting for {data_name}")
        dataset = DatasetOperator(data_name)
        n = dataset.K_MAX[dataset.name] + 1  # dataset.linop.A.shape[1] + 1
        res = defaultdict(lambda: np.zeros(n))
        paths = list(RESULT_PATH.glob(f"*{data_name}*"))
        paths.sort()
        for path in paths:
            with np.load(str(path)) as file:
                stem = path.stem
                n_nonzeros = int(stem.rsplit('_', 1)[1])
                data_idx = stem.find(data_name)
                algo_name, str_idx = stem[:data_idx - 1].rsplit('_', 1)
                idx = int(str_idx)
                algo_name = algo_name + "_BEST" if idx == 1 else algo_name
                res[algo_name][n_nonzeros] = dataset.f(file["x"])
        fig = go.Figure()
        for algo, info in ALGOS_PAPER_TT.items():
            if algo in res.keys():
                legends_rank = res.keys()
                if dataset.name == "cal_housing":
                    legends_rank = ["IHT", "HTP", "OMP", "OMPR", "SEA_ELS", "SEA_0"]
                    if info["disp_name"] == "OMP":
                        display_name = "$\\text{OMP}, \\text{IHT}_{\\text{OMP}}, \\text{HTP}_{\\text{OMP}}$"
                    elif info["disp_name"] == "OMPR":
                        display_name = "$\\text{OMPR}, \\text{ELS}, \\text{IHT}_{\\text{ELS}}, \\text{HTP}_{\\text{ELS}}$"
                    elif info["disp_name"] == "SEA_ELS":
                        display_name = "$\\text{SEA}_{\\text{ELS}}, \\text{SEA}_\\text{OMP}$"
                    else:
                        display_name = info["name"]
                elif dataset.name == "slice" or dataset.name == "letter":
                    if dataset.name == "slice":
                        legends_rank = ["HTP", "IHT", "SEA_0", "OMP", "OMPR", "SEA_OMP", "ELS", "SEA_ELS", ]
                    elif dataset.name == "letter":
                        legends_rank = ["HTP", "IHT", "OMP", "OMPR", "SEA_OMP", "ELS", "SEA_0", "SEA_ELS", ]
                    if info["disp_name"] == "OMP":
                        display_name = "$\\text{OMP}, \\text{IHT}_{\\text{OMP}}, \\text{HTP}_{\\text{OMP}}$"
                    elif info["disp_name"] == "ELS":
                        display_name = "$\\text{ELS}, \\text{IHT}_{\\text{ELS}}, \\text{HTP}_{\\text{ELS}}$"
                    else:
                        display_name = info["name"]
                elif dataset.name == "ijcnn1":
                    legends_rank = ["HTP", "IHT", "OMP", "OMPR", "SEA_ELS", ]
                    if info["disp_name"] == "OMP":
                        display_name = "$\\text{OMP}, \\text{IHT}_{\\text{OMP}}, \\text{HTP}_{\\text{OMP}}$"
                    elif info["disp_name"] == "OMPR":
                        display_name = "$\\text{OMPR}, \\text{ELS}, \\text{IHT}_{\\text{ELS}}, \\text{HTP}_{\\text{ELS}}$"
                    elif info["disp_name"] == "SEA_ELS":
                        display_name = "$\\text{SEA}_{\\text{0}}, \\text{SEA}_{\\text{OMP}}, \\text{SEA}_{\\text{ELS}}$"
                    else:
                        display_name = info["name"]
                else:
                    display_name = info["name"]
                curve = res[algo]

                if dataset.name == 'letter' and (info["disp_name"] == "SEA_0" or info["disp_name"] == "ELS"):
                    info_line = deepcopy(info["line"])
                    info_line["width"] = info_line["width"] * 1.5
                else:
                    info_line = info["line"]

                try:
                    fig.add_trace(go.Scatter(y=curve, line=info_line, name=display_name, mode='lines',
                                             legendrank=legends_rank.index(info["disp_name"])))
                except Exception as e:
                    print(f"{algo} is not plotted in the paper version")
            fig.update_layout(
                xaxis_title="Sparsity",
                yaxis_title=r"$\text{log}\_\text{loss}$" if dataset.task == Task.BINARY else r"$\ell_2\_\text{loss}$",
                xaxis_range=(1, n - 1),
                **PAPER_LAYOUT,
            )
        fig.update_layout(showlegend=True, legend=dict(
            orientation="h",
            title=None,
            y=1,
            x=0.61,
            xanchor="left",
            yanchor="top",
            entrywidth=500,
            bgcolor='rgba(0,0,0,0)'
        ), margin=dict(l=70, b=60))
        if dataset.task == Task.BINARY:
            fig.update_layout(
                margin=dict(autoexpand=False, l=80, ))
            if dataset.name == "ijcnn1":
                fig.update_layout(legend=dict(x=0.3, xanchor="left", ))
        fig.write_html(PLOT_PATH_PAPER / f"paper_{data_name}.html", include_mathjax='cdn')


def select_algo(algos_type, algos_filter, sea_params=None, lip_factor=2 * 0.9, **params) -> Dict[str, Callable]:
    """
    Select algorithms to run in experiments

    :param (List[str]) algos_type: List of type of algorithm to look for. Must be a value of ALGOS_TYPE
    :param (List[str]) algos_filter: List of algorithm to run among those selected by algos_type parameter
    :param (Any) params: Parameters to pass to all algorithms
    :return: Dictionary with algorithms associated to their names
    """
    if ALL in algos_type:
        algos_type = ALGOS_TYPE
    algo_dict = get_algo_dict(**{f'use_{algo_name}': True for algo_name in algos_type}, sea_params=sea_params,
                              lip_factor=lip_factor, **params)
    if ALL in algos_filter:
        algo_selected = algo_dict
    else:
        algo_selected = {name: algo for name, algo in algo_dict.items()
                         if name + f"-{lip_factor}" in algos_filter or name in algos_filter}
    return algo_selected


@ray.remote
def run_main(cmd) -> None:
    """
    Run a command with the Click CLI

    :param (str) cmd: Command to send
    """
    main(cmd.split(" "), standalone_mode=False)


@click.command(context_settings={'show_default': True, 'help_option_names': ['-h', '--help']})
@click.option('--algos_type', '-at', multiple=True, default=[ALL],
              type=click.Choice([*ALGOS_TYPE, ALL], case_sensitive=False),
              help='Algorithms to run. If \'ALL\' is selected, run all algorithms.')
@click.option('--factors', '-f', multiple=True, default=[256],
              help='Sparsity factors to use for the iteration\'s number of SEA and IHT')
@click.option('--datasets', '-d', multiple=True,
              default=list(DatasetOperator.REGRESSIONS_NAME) + list(DatasetOperator.BINARY_NAME[:2]),
              type=click.Choice(list(DatasetOperator.REGRESSIONS_NAME) + list(DatasetOperator.BINARY_NAME) + [ALL],
                                case_sensitive=False),
              help='Datasets to evaluate. If \'ALL\' is selected, evaluate all datasets.')
@click.option('--algos_filter', '-af', multiple=True,
              default=["SEAFAST-els", "SEAFAST-omp", "SEAFAST", "ELS", "OMP", "IHT",
                       "HTP", "IHT-omp", "HTP-omp", "IHT-els", "HTP-els", "OMPR"],
              type=click.Choice(list(get_algo_dict(*[True] * len(ALGOS_TYPE)).keys()) + [ALL], case_sensitive=False),
              help='Algorithms to run. If \'ALL\' is selected, run all algorithms.')
@click.option('--resume/--no-resume', '-r/-nr', default=True, help='If don\'t overwrite previous results')
@click.option('--plot/--no-plot', '-pd/-npd', default=False,
              help='If True, only plot results for all selected datasets')
@click.option('--plot-paper/--no-plot-paper', '-pl/-npl', default=False,
              help='If True, only plot results for all selected datasets for the paper')
@click.option('--param_file', '-pf', default=None, help='If specified, run in parallel multiple instance'
                                                        ' of the function with the specified parameters')
@click.option('--num-cpus', '-cpu', default=None, type=int, help="Limit the number of CPU if needed")
@click.option('--reverse/--no-reverse', '-rev/-nrev', default=False, help='If True, compute by descending sparsity')
@click.option('--sparsities', '-s', multiple=True, default=[0], type=click.IntRange(0, DatasetOperator.MAX_K_MAX + 1),
              help='Sparsity values to use when solving problems')
def main(algos_type, datasets, factors, resume, algos_filter, plot, plot_paper, param_file, num_cpus, reverse,
         sparsities) -> None:
    """
    Solve problem from the selected datasets with the specified algorithms
    """
    logger.info(f"Parameters: \n{locals()}")
    if plot or plot_paper:
        if plot_paper:
            if ALL in datasets:
                plot_ml_paper(('cal_housing', 'comp-activ-harder', 'ijcnn1', 'letter', 'slice', 'year',),
                              factors)
            else:
                plot_ml_paper(datasets, factors)
        elif plot:
            if ALL in datasets:
                plot_results()
            else:
                plot_results(datasets)
    elif param_file is None:
        # Dict are already normalized and, we need to specify iteration limit for OMP-like algorithms
        algo_selected = select_algo(algos_type, algos_filter, normalize=False, n_iter=1000)
        datasets = DatasetOperator.REGRESSIONS_NAME + DatasetOperator.BINARY_NAME if ALL in datasets else datasets
        with np.errstate(divide='ignore', invalid='ignore'):
            run_experiments(algo_selected, datasets, factors, resume, reverse, sparsities)
        logger.info(f'End of Training for : \n{locals()}')
    else:
        # For multiprocessing
        if os.getcwd().startswith('/baie'):  # On sms cluster
            logger.info("Running on sms")
            ray.init(_temp_dir="/scratch/ray")
        else:  # On local or core5
            logger.info(f"Running on {socket.gethostname()}")
            ray.init(num_cpus=num_cpus)
        with open(param_file, 'r') as file:
            futures = [run_main.remote(line.strip()) for line in file.readlines()]
        ray.get(futures)


if __name__ == '__main__':
    main()
