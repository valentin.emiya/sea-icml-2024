# Python imports
import pickle
from pathlib import Path
from shutil import rmtree
import os

# Module imports
import click
from loguru import logger
import numpy as np
from numpy.random import RandomState
from plotly import graph_objects as go, express as px
from tqdm import tqdm

# Script imports
from sksea.algorithms import ExplorationHistory
from sksea.deconvolution import ConvolutionOperator, gen_u, gen_filter
from sksea.exp_phase_transition_diag import NoiseType, gen_noise
from sksea.plot_icml import plot_dcv_n_supports, plot_dcv_paper
from sksea.training_tasks import ALGOS_TYPE, select_algo, ALL, get_algo_dict
from sksea.utils import ALGOS_PAPER_DCV, compute_metrcs_from_file, PAPER_LAYOUT, RESULT_FOLDER, DATA_FILENAME

MODULE_PATH = Path(os.environ["WORK"]) / "sea_data" if "WORK" in os.environ else Path(__file__).parent
ROOT = MODULE_PATH / "temp" / "deconv"
TYPE_AVAILABLE = [True, True, True, True, False, True]


def solve_problem(algorithm, linop, n_nonzero, seed, range_max, temp_folder, noise_factor, noise_type, keep_temp=False,
                  n_iter=1000, min_u=1, max_u=10, is_light=False, k_ratio=1, noise_on_x=False):
    save_file = temp_folder / (str(seed) + ".npy")
    save_file_supp = temp_folder / (str(seed) + ".pkl")
    if keep_temp and save_file.is_file() and (algorithm is None or save_file_supp.is_file() or is_light):
        # logger.debug(f"{save_file} already solved")
        pass
    else:
        rand = RandomState(seed=seed)
        x = gen_u(linop.shape[0], n_nonzero, 4, rand, range_max, min_u=min_u, max_u=max_u)
        y = linop(x)
        x_not_noisy = x.copy()
        if noise_factor is not None:
            if noise_on_x:
                x += gen_noise(noise_type, noise_factor, x, rand)
            else:
                y += gen_noise(noise_type, noise_factor, y, rand)

        if algorithm is None:
            x_algo = x_not_noisy
        else:
            rd = {"seed": seed} if "SEAFAST-r" in str(temp_folder) else {}
            x_algo, *other_out = algorithm(linop=linop, y=y, n_nonzero=int(k_ratio * n_nonzero), n_iter=n_iter,
                                           f=lambda x, linop: np.linalg.norm(linop @ x - y),
                                           grad_f=lambda x, linop: linop.H @ (linop @ x - y), **rd)
            if not is_light:
                with open(save_file_supp, "wb") as f:
                    pickle.dump(other_out, f)
        np.save(save_file, x_algo, False)


def run_experiment(sigma, sigma_factor, n_runs, h_len, x_len, expe_name, noise_factor, algos, noise_type,
                   spars_max=None, sparsities=None, keep_temp=False, store_solution=True, reverse_seed=False,
                   seeds=tuple(), n_iter=1000, min_u=1, max_u=2, is_light=False, k_ratio=1, noise_on_x=False,
                   lip_factor=2*0.9):
    # Generate filter
    h = gen_filter(h_len, 3, sigma)
    linop = ConvolutionOperator(h, x_len)

    # Get algorithms we want to run
    algorithms_solve = select_algo(ALGOS_TYPE[:5], algos, sea_params=dict(return_best=True), lip_factor=lip_factor)
    algorithms = dict(**algorithms_solve)
    if store_solution:
        algorithms['solution'] = None
    range_max = 2 * sigma * sigma_factor if sigma_factor != 0 else x_len
    spars_max = min(range_max, x_len // 2 + 1) if spars_max is None else spars_max + 1
    temp_root = ROOT / expe_name
    temp_root.mkdir(parents=True, exist_ok=True)
    data = dict(h=h, x_len=x_len, sigma=sigma, sigma_factor=sigma_factor, n_runs=n_runs, range_max=range_max,
                h_len=h_len, noise_factor=noise_factor, spars_max=spars_max, sparsities=sparsities,
                noise_type=noise_type, n_iter=n_iter, min_u=min_u, max_u=max_u, is_light=is_light, k_ratio=k_ratio,
                noise_on_x=noise_on_x, lip_factor=lip_factor)
    data_path = temp_root / DATA_FILENAME
    if not data_path.is_file() or not keep_temp:
        np.savez(data_path, **data)
    for name, algorithm in algorithms.items():
        logger.info(name)
        if not keep_temp:
            rmtree(temp_root / name, ignore_errors=True)
        iterator = sparsities if len(sparsities) != 0 else range(1, spars_max)
        for n_nonzero in iterator:
            temp_folder = temp_root / name / str(n_nonzero)
            logger.debug(f"Solving k={n_nonzero} with {name}")
            temp_folder.mkdir(parents=True, exist_ok=True)
            if keep_temp and len(tuple(temp_folder.glob("*.npy"))) < n_runs or len(seeds) == 0:
                if len(seeds) == 0:
                    seed_iterator = range(n_runs, -1, -1) if reverse_seed else range(n_runs)
                else:
                    seed_iterator = seeds
                for seed in tqdm(seed_iterator, desc=f"{name} , k={n_nonzero}"):
                    solve_problem(algorithm, linop, n_nonzero, seed, range_max, temp_folder, noise_factor, noise_type,
                                  keep_temp, n_iter=n_iter, min_u=min_u, max_u=max_u, is_light=is_light,
                                  k_ratio=k_ratio, noise_on_x=noise_on_x)
            else:
                logger.debug(f"k={n_nonzero} Already done")


def get_n_supports_temp_from_start(history, best=None) -> int:
    """
    Return the number of supports visited by the algorithm including the ones visited by the previous algorithms
    """
    supports = set()
    for previous_it in history.old_it:
        supports.update(previous_it.keys())
    if best and history.best_it is None:
        raise ValueError("No best support found")
    elif best or (best is None and history.best_it is not None):
        for buffer, iterations in history.it.items():
            if iterations[0] <= history.best_it:
                supports.add(buffer)
    else:
        supports.update(history.it.keys())
    return len(supports)


def get_n_supports_temp_new(history, best=None) -> int:
    """
    Return the number of supports visited by the algorithm
    ONLY including the ones NOT visited by the previous algorithms
    """
    supports = set()
    new_supports = set()
    for previous_it in history.old_it:
        supports.update(previous_it.keys())
    if best and history.best_it is None:
        raise ValueError("No best support found")
    elif best or (best is None and history.best_it is not None):
        for buffer, iterations in history.it.items():
            if iterations[0] <= history.best_it and buffer not in supports:
                new_supports.add(buffer)
    else:
        new_supports.update(set(history.it.keys()) - supports)
    return len(new_supports)


def get_n_supports_temp(history, best=None) -> int:
    """
    Return the number of supports visited by the algorithm
    """
    n_supports = 0
    if best and history.best_it is None:
        raise ValueError("No best support found")
    elif best or (best is None and history.best_it is not None):
        for buffer, iterations in history.it.items():
            if iterations[0] <= history.best_it:
                n_supports += 1
    else:
        n_supports += len(history.it)
    return n_supports


def compile_results(expe_name, keep_temp=False, n_runs=None, spars_max=None, algos_filter=(ALL,), store_solution=True,
                    is_light=False):
    temp_root = ROOT / expe_name
    result_folder = RESULT_FOLDER / expe_name
    result_folder.mkdir(parents=True, exist_ok=True)
    with np.load(temp_root / DATA_FILENAME, allow_pickle=True) as data:  # With statement is needed because of .npz file
        n_runs = data["n_runs"] if n_runs is None else n_runs
        if spars_max is None:
            if "spars_max" in data:
                spars_max = data["spars_max"]
            else:
                spars_max = data["range_max"] - 1
        logger.info("Compiling....")
        algorithms_name = [f.name for f in temp_root.glob("*")] if ALL in algos_filter else list(algos_filter)
        if store_solution:
            algorithms_name.append("solution")
        for algo_name in algorithms_name:
            algo_folder = temp_root / algo_name
            results = np.zeros((n_runs, spars_max, data["x_len"]))
            n_supports = np.zeros((n_runs, spars_max))
            n_supports_new = np.zeros((n_runs, spars_max))
            n_supports_from_start = np.zeros((n_runs, spars_max))
            if algo_folder.is_dir():
                for n_nonzero in range(1, spars_max + 1):
                    temp_folder = algo_folder / str(n_nonzero)
                    if temp_folder.is_dir():
                        logger.debug(f"Compiling {temp_folder}")
                        # for file in temp_folder.glob("*"):
                        for run_id in range(n_runs):
                            file = temp_folder / f"{run_id}.npy"
                            try:
                                results[int(file.stem), n_nonzero - 1] = np.load(file)
                                # logger.info(f"Loaded {file}")
                            except FileNotFoundError:
                                logger.error(f"Can't open {file}")
                            # except Exception as e:
                            #     logger.error(f"Error in {file}")
                            #     os.remove(file)
                                # raise e
                            if algo_folder.name != "solution" and not is_light:
                                try:
                                    pkl_file = temp_folder / f"{run_id}.pkl"
                                    with open(pkl_file, 'rb') as f:
                                        content = pickle.load(f)
                                        if len(content) >= 2:
                                            history: ExplorationHistory = content[1]
                                            # TODO: Get back to history.get_n_supports() when fixed
                                            n_supports[int(file.stem), n_nonzero - 1] = get_n_supports_temp(
                                                history,
                                                best=(algo_folder.name.startswith("SEAFAST") or
                                                      algo_folder.name.startswith("HTPFAST"))
                                            )
                                            n_supports_new[int(file.stem), n_nonzero - 1] = get_n_supports_temp_new(
                                                history,
                                                best=(algo_folder.name.startswith("SEAFAST") or
                                                      algo_folder.name.startswith("HTPFAST"))
                                            )
                                            n_supports_from_start[
                                                int(file.stem), n_nonzero - 1] = get_n_supports_temp_from_start(
                                                history,
                                                best=(algo_folder.name.startswith("SEAFAST") or
                                                      algo_folder.name.startswith("HTPFAST"))
                                            )
                                except FileNotFoundError:
                                    logger.error(f"Can't open {pkl_file}")
                np.save(result_folder / algo_folder.name, results)
                np.savez(result_folder / algo_folder.name, n_supports=n_supports,
                         n_supports_new=n_supports_new, n_supports_from_start=n_supports_from_start)
                if not keep_temp:
                    rmtree(algo_folder)
        np.savez(result_folder / DATA_FILENAME, **data)
    if not any(result_folder.iterdir()):
        rmtree(temp_folder)


def add_curve(fig, sparcity, metric, color, name, paper=False, fig_type=None):
    mean = metric.mean(axis=0)
    legend_ranks = ["HTP", "IHT", "OMP", "OMPR", "ELS", "SEAFAST-els", "SEAFAST-omp", "SEAFAST", ]

    if paper:
        if name == "OMP":
            if fig_type in ("sup", "ws"):
                display_name = "$\\text{OMP}, \\text{IHT}_{\\text{OMP}}, \\text{HTP}_{\\text{OMP}}$"
            else:
                display_name = "$\\text{OMP}, \\text{IHT}_{\\text{OMP}}, \\text{HTP}_{\\text{OMP}}$"
        elif name == "ELS":
            display_name = "$\\text{ELS}, \\text{IHT}_{\\text{ELS}}, \\text{HTP}_{\\text{ELS}}$"
        else:
            display_name = ALGOS_PAPER_DCV[name]["name"]

        fig.add_trace(
            go.Scatter(x=sparcity, y=mean, name=display_name, line=ALGOS_PAPER_DCV[name]["line"],
                       legendrank=legend_ranks.index(name)))
    else:
        fig.add_trace(
            go.Scatter(x=sparcity, y=mean, name=name, line=dict(color=color, width=4)))


def plot_metrics_from_file(file, file_id, spars_max, linop, solution, paper, fig_mse, fig_sup, fig_y, fig_ws, fig_n_sup,
                           fig_n_sup_new, fig_n_sup_from_start, fig_sup_min, fig_sup_top, force_recompute=False):
    if file.is_file():
        sparcity = np.arange(1, spars_max + 1)
        colors = px.colors.qualitative.Plotly
        color = colors[file_id % len(colors)]
        temp_plot_file = file.parent / "temp_plot_data" / (file.stem + ".npz")
        *other_dim, last = solution.shape
        if temp_plot_file.is_file() and not force_recompute:
            logger.debug(f"Loading {temp_plot_file}")
        else:
            logger.debug(f"Computing {temp_plot_file}")
            compute_metrcs_from_file(file, spars_max, linop, solution, temp_plot_file)
            plot_metrics_from_file(file, file_id, spars_max, linop, solution, paper, fig_mse, fig_sup, fig_y, fig_ws,
                                   fig_n_sup, fig_n_sup_new, fig_n_sup_from_start, fig_sup_min, fig_sup_top,
                                   force_recompute=False)
            return None
        temp_plot_data = np.load(temp_plot_file)
        mse = temp_plot_data["mse"]
        sup_dist = temp_plot_data["sup_dist"]
        f_mse_y = temp_plot_data["f_mse_y"]
        ws = temp_plot_data.get("ws")
        n_supports = temp_plot_data.get("n_supports")
        n_supports_new = temp_plot_data.get("n_supports_new")
        n_supports_from_start = temp_plot_data.get("n_supports_from_start")
        sup_dist_min = temp_plot_data.get("sup_dist_min")
        sup_dist_top = temp_plot_data.get("sup_dist_top")

        if 0 in n_supports:
            logger.error(f"{file.stem} not fully computed")
        add_curve(fig_ws, sparcity, ws, color, file.stem, paper=paper, fig_type="ws")
        add_curve(fig_n_sup, sparcity, n_supports, color, file.stem, paper=paper, fig_type="n_supports")
        add_curve(fig_n_sup_new, sparcity, n_supports_new, color, file.stem, paper=paper, fig_type="n_supports")
        add_curve(fig_n_sup_from_start, sparcity, n_supports_from_start, color, file.stem, paper=paper,
                  fig_type="n_supports")
        add_curve(fig_mse, sparcity, mse, color, file.stem, paper=paper)
        add_curve(fig_sup, sparcity, sup_dist, color, file.stem, paper=paper, fig_type="sup")
        if sup_dist_min is not None:
            add_curve(fig_sup_min, sparcity, sup_dist, color, file.stem, paper=paper, fig_type="sup_min")
        else:
            logger.warning(f"{file.stem} has no minimum support distance")
        if sup_dist_top is not None:
            add_curve(fig_sup_top, sparcity, sup_dist, color, file.stem, paper=paper, fig_type="sup_top")
        else:
            logger.warning(f"{file.stem} has no top support distance")
        add_curve(fig_y, sparcity, f_mse_y.reshape(other_dim), color, file.stem, paper=paper, fig_type="mse_y")


def plot_figures(expe_name, paper=False, spars_max=None, force_recompute=False):
    result_folder = RESULT_FOLDER / expe_name
    with np.load(result_folder / DATA_FILENAME, allow_pickle=True) as data:
        fig_mse = go.Figure()
        fig_sup = go.Figure()
        fig_sup_min = go.Figure()
        fig_sup_top = go.Figure()
        fig_y = go.Figure()
        fig_ws = go.Figure()
        fig_n_sup = go.Figure()
        fig_n_sup_new = go.Figure()
        fig_n_sup_from_start = go.Figure()
        spars_max = min(data["range_max"], 32) if spars_max is None else spars_max
        solution = np.load(result_folder / "solution.npy")[:, :spars_max, :]
        linop = ConvolutionOperator(data["h"], data["x_len"])
        params = dict(**data)
        params.pop("h")
        subtitle = f"<br><sup>{params}</sup>"
    layout = dict(xaxis_title="Sparsity")
    if paper:
        layout.update(PAPER_LAYOUT)
        layout.update(
            legend_title=None,
            legend_orientation='h',
            legend_bgcolor='rgba(0,0,0,0)'
        )
    else:
        layout.update(dict(legend_title="Algorithms", ))

    if paper:
        paths = [result_folder / f"{name}.npy" for name in ALGOS_PAPER_DCV.keys()]
    else:
        paths = [path for path in result_folder.glob("*.npy") if path.name != "solution.npy"]
    for idx, file in enumerate(paths):
        plot_metrics_from_file(file, idx, spars_max, linop, solution, paper, fig_mse, fig_sup, fig_y, fig_ws, fig_n_sup,
                               fig_n_sup_new, fig_n_sup_from_start, fig_sup_min, fig_sup_top, force_recompute)
    if paper:
        fig_mse.update_layout(yaxis_title=r"$\text{Mean of } \ell_{2, \text{rel}}$", **layout,
                              legend=dict(y=1, x=0.05, xanchor="left", yanchor="top"))
        fig_ws.update_layout(yaxis_title=r"$\text{Average Wasserstein distance}$", **layout,
                             legend=dict(y=0, x=0.45, xanchor="left", yanchor="bottom", entrywidth=200))
        fig_ws.update_layout(margin=dict(l=80, ), yaxis=dict(showexponent='all', exponentformat='e'))
        fig_sup.update_layout(yaxis_title=r"$\text{Mean of } \text{supp}_{\text{dist}}$", **layout,
                              legend=dict(y=0, x=1.01, xanchor="right", yanchor="bottom",
                                          entrywidth=255))  # , font_size=25) )
        fig_sup_min.update_layout(yaxis_title=r"$\text{Mean of } \text{supp}_{\text{dist_min}}$", **layout,
                                    legend=dict(y=0, x=1.01, xanchor="right", yanchor="bottom",
                                                entrywidth=255))  # , font_size=25) )
        fig_y.update_layout(yaxis_title=r"$\text{Mean of } \ell_{2, \text{rel}\_\text{loss}}$", **layout,
                            legend=dict(y=0, x=1.01, xanchor="right", yanchor="bottom", entrywidth=255))
        fig_y.update_layout(margin=dict(l=70, b=55, ))

    else:
        fig_mse.update_layout(yaxis_title="MSE mean over x",
                              title=f"MSE mean and std over x by sparsity {subtitle}", **layout)
        fig_sup.update_layout(yaxis_title="Support distance mean",
                              title=f"Support distance mean by sparsity {subtitle}", **layout)
        fig_sup_min.update_layout(yaxis_title="Support distance_min mean",
                                    title=f"Support distance_min mean by sparsity {subtitle}", **layout)
        fig_sup_top.update_layout(yaxis_title="Support distance_top mean",
                                    title=f"Support distance_top mean by sparsity {subtitle}", **layout)
        fig_ws.update_layout(yaxis_title="Wasserstein distance mean over x",
                             title=f"Wasserstein distance mean by sparsity {subtitle}", **layout)
        fig_y.update_layout(yaxis_title="MSE mean and std over y",
                            title=f"MSE mean and std over y by sparsity {subtitle}", **layout)
    fig_n_sup.update_layout(yaxis_title="Number of support explored mean", yaxis_type="log",
                            title=f"Number of support explored mean by sparcity {subtitle}", **layout)
    fig_n_sup_new.update_layout(yaxis_title="Number of NEW support explored mean", yaxis_type="log",
                                title=f"Number of support NEW explored mean by sparcity {subtitle}", **layout)
    fig_n_sup_from_start.update_layout(yaxis_title="Number of support explored from start mean", yaxis_type="log",
                                       title=f"Number of support explored from start mean by sparcity {subtitle}",
                                       **layout)
    write_folder = result_folder / "paper" if paper else result_folder / "draft"
    write_folder.mkdir(parents=True, exist_ok=True)
    fig_mse.write_html(write_folder / "mse.html", include_mathjax='cdn')
    fig_ws.write_html(write_folder / "ws.html", include_mathjax='cdn')
    fig_sup.write_html(write_folder / "sup_dist.html", include_mathjax='cdn')
    fig_sup_min.write_html(write_folder / "sup_dist_min.html", include_mathjax='cdn')
    fig_sup_top.write_html(write_folder / "sup_dist_top.html", include_mathjax='cdn')
    fig_y.write_html(write_folder / "mse_y.html", include_mathjax='cdn')
    fig_n_sup.write_html(write_folder / "n_sup.html", include_mathjax='cdn')
    fig_n_sup_new.write_html(write_folder / "n_sup_new.html", include_mathjax='cdn')
    fig_n_sup_from_start.write_html(write_folder / "n_sup_from_start.html", include_mathjax='cdn')
    fig_n_sup.update_layout(yaxis_type="linear")
    fig_n_sup_new.update_layout(yaxis_type="linear")
    fig_n_sup_from_start.update_layout(yaxis_type="linear")
    fig_n_sup.write_html(write_folder / "n_sup_linear.html", include_mathjax='cdn')
    fig_n_sup_new.write_html(write_folder / "n_sup_new_linear.html", include_mathjax='cdn')
    fig_n_sup_from_start.write_html(write_folder / "n_sup_from_start_linear.html", include_mathjax='cdn')


def coherence(linop):
    matrix = linop.get_normalized_operator()[0].matrix
    return np.max(np.abs(np.conjugate(matrix.T) @ matrix) - np.eye(linop.shape[1]))


def display_coherences(sigmas, h_len, x_len):
    for sigma in sigmas:
        h = gen_filter(h_len, 3, sigma)
        linop = ConvolutionOperator(h, x_len)
        print(sigma, coherence(linop))


def debug_sigma(expe_names):
    fig = go.Figure()
    for expe_name in expe_names:
        result_folder = RESULT_FOLDER / expe_name
        data = np.load(result_folder / DATA_FILENAME)
        params = dict(**data)
        h = params.pop("h")
        fig.add_trace(go.Scatter(y=h, name=f"sigma = {params['sigma']}"))
    fig.write_html("debug.html")


def plot_exploration_size(expe_name, algo_names, sparsities, seeds, n_runs, reverse_seed):
    temp_root = ROOT / expe_name
    for name in algo_names:
        for n_nonzero in sparsities:
            temp_folder = temp_root / name / str(n_nonzero)
            if len(seeds) == 0:
                seed_iterator = range(n_runs, -1, -1) if reverse_seed else range(n_runs)
            else:
                seed_iterator = seeds
            len_data = []
            for seed in seed_iterator:
                with open(temp_folder / f"{seed}.pkl", 'rb') as f:
                    _, exploration_history = pickle.load(f)
                len_data.append(exploration_history.get_n_supports())
                if seed == 118:
                    print(exploration_history.get_n_supports())
            fig = go.Figure()
            fig.add_trace(go.Histogram(x=len_data, name="Number of supports"))
            layout = dict()
            layout.update(PAPER_LAYOUT)
            layout.update(
                legend_title=None,
                legend_orientation='h',
                legend_bgcolor='rgba(0,0,0,0)',
                margin=dict(
                    autoexpand=False,
                    l=55,
                    r=5,
                    t=30,
                    b=70,
                ),
            )
            display_name = ALGOS_PAPER_DCV[name]["name"]
            fig.update_layout(**layout, xaxis_title=rf"$\text{{Number of explored supports by }}{display_name[1:-1]}$",
                              yaxis_title=r"$\text{Number of runs}$")
            fig.write_html(temp_folder / "histogram.html", include_mathjax='cdn')


@click.command(context_settings={'show_default': True, 'help_option_names': ['-h', '--help']})
@click.option('--sigma', '-s', default=3, type=int, help='Variance of the gaussian filter')
@click.option('--sigma_factor', '-sf', default=0, type=int,
              help='If 0, the support of the signal can be spread over the whole signal, else it is spread over '
                   'sigma_factor*sigma')
@click.option('--n_runs', '-ru', default=200, type=int, help='Number of times each configuration is tested')
@click.option('--h_len', '-hl', default=500, help='Size of the gaussian filter')
@click.option('--x_len', '-xl', default=500, help='Size of the signal')
@click.option('--expe-name', '-en', default='', help="Name of the experiment")
@click.option('--noise-factor', '-nf', default=None, type=float, help="Variance of the gaussian noise")
@click.option('--noise_type', '-nt', default=NoiseType.NOISE_LEVEL.name,
              type=click.Choice(dir(NoiseType)[:len(NoiseType)], case_sensitive=False), help="How compute the noise")
@click.option('--algos_filter', '-af', multiple=True,
              default=[],
              type=click.Choice(list(get_algo_dict(*TYPE_AVAILABLE).keys()) + [ALL],
                                case_sensitive=False),
              help='Algorithms to run. If \'ALL\' is selected, run all algorithms.')
@click.option('--plot', '-pl', is_flag=True,
              help='If specified, plot results instead of running algorithmslike in the paper')
@click.option('--plot_draft', '-pd', is_flag=True, help='If specified, plot results instead of running algorithms')
@click.option('--plot_hist', '-plh', is_flag=True, help='If specified, plot histograms')
@click.option('--spars_max', '-sm', default=50, type=int, help='Sparsity maximum of the problems')
@click.option('--sparsities', '-sp', default=None, type=int, multiple=True, help='Sparsity to analyze')
@click.option('--compile', '-cp/-ncp', is_flag=True, default=True, help='If specified, compile results if not plotting')
@click.option('--run', '-r/-nr', is_flag=True, default=True, help='If specified, run experiment if not plotting')
@click.option('--keep_temp', '-kt/-nkt', is_flag=True, default=True, help='If specified, keep temporary files across runs')
@click.option('--store_solution', '-ss/-nss', is_flag=True, default=True, help='If disabled, does not store solution')
@click.option('--reverse_seed', '-rs/-nrs', is_flag=True, default=False,
              help='If enabled, use seeds in decreasing order')
@click.option('--seeds', '-sd', default=None, type=int, multiple=True, help='Seeds to use for computations')
@click.option('--n_iter', '-ni', default=1000, type=int, help='Seeds to use for computations')
@click.option('--force_data_plot', '-fdp/-nfdp', default=False, is_flag=True, help='If True, recompute data for plots')
@click.option('--min_u', '-mnu', default=1, type=int, help='Min of uniform distribution for |x|')
@click.option('--max_u', '-mxu', default=2, type=int, help='Max of uniform distribution for |x|')
@click.option('--is_light', '-l/-nl', is_flag=True, default=False,
              help='If true, do not store support history')
@click.option('--k_ratio', '-kr', default=1, type=float, help='Max of uniform distribution for |x|')
@click.option('--noise_on_x', '-nox', is_flag=True, default=False, help='If true, add noise on x instead of y')
@click.option('--lip_factor', '-lf', default=2*0.9, type=float, help='Lipschitz factor for the operator')
@click.option('--algos_filter_lip', '-afl', multiple=True, default=[],
              help='Algorithms to run with lipschitz constant')
def main(sigma, sigma_factor, n_runs, h_len, x_len, expe_name, noise_factor, noise_type, algos_filter, plot, spars_max,
         sparsities, compile, run, keep_temp, store_solution, reverse_seed, seeds, plot_hist, n_iter, force_data_plot,
         plot_draft, min_u, max_u, is_light, k_ratio, noise_on_x, lip_factor, algos_filter_lip):
    logger.info(f"Parameters: \n{locals()}")
    algos_filter = list(algos_filter) + list(algos_filter_lip)
    if plot:
        plot_dcv_paper(expe_name=expe_name, spars_max=spars_max)
        if "k" in expe_name:
            plot_dcv_paper(expe_name=expe_name, spars_max=spars_max, downscale=4)
        plot_dcv_n_supports(expe_name=expe_name, spars_max=spars_max)
    elif plot_draft:
        if plot_hist:
            plot_exploration_size(expe_name, algos_filter, sparsities, seeds, n_runs, reverse_seed)
        # plot_figures(expe_name, paper=True, spars_max=spars_max)  # Plot figures of the experiment like in the paper
        plot_figures(expe_name, paper=False, spars_max=spars_max, force_recompute=force_data_plot)
    else:
        if run:
            # Run the experiment with the parameters
            run_experiment(sigma, sigma_factor, n_runs, h_len, x_len, expe_name, noise_factor, algos_filter, noise_type,
                           spars_max, sparsities, keep_temp, store_solution, reverse_seed, seeds, n_iter, min_u, max_u,
                           is_light, k_ratio, noise_on_x, lip_factor)
        if compile:
            # Compile the raw results in code/sksea/results/deconv/{expe_name}
            compile_results(expe_name, keep_temp, n_runs, spars_max, algos_filter, store_solution, is_light)
    logger.info("End")


# Needed for click CLI
if __name__ == '__main__':
    main()
    # main("-sm 5 -s 3 -ru 10 -hl 100 -xl 100 -en tttttest -kt -l -af SEAFAST".split(" "))
    # main("-sm 50 -s 3 -ru 3 -hl 500 -xl 500 -en dcv_hist_test -r -cp -af SEAFAST -sp 1".split(" "))
    # main("-sm 50 -s 3 -ru 200 -hl 500 -xl 500 -en dcv_hist -kt -r -ncp -sd 4 -af HTPFAST-els_fast -sp 17".split(" "))
