# -*- coding: utf-8 -*-
from typing import Tuple
import numpy as np
from scipy.sparse.linalg.interface import MatrixLinearOperator
from scipy.linalg import cho_solve
from sklearn.preprocessing import normalize

from sksea.utils import AbstractLinearOperator
from sksea.cholesky import chol_1d_update, chol_1d_downdate


class MatrixOperator(MatrixLinearOperator, AbstractLinearOperator):
    def __init__(self, data_mat, seed=0):
        MatrixLinearOperator.__init__(self, A=data_mat)
        self.seed = seed

    def get_normalized_operator(self) -> Tuple['MatrixOperator', np.ndarray]:
        """
        Return a normalized version of the operator using normalize function of sklearn on the operator matrix
        """
        a, w_diag = normalize(self.A, axis=0, return_norm=True)
        return type(self)(a, self.seed), 1 / w_diag

    def get_operator_on_support(self, s) -> 'MatrixOperator':
        """
        Return the operator truncated on the provided support

        :param (np.ndarray) s: Support
        """
        return type(self)(self.A[:, s], self.seed)


class SparseSupportOperator(MatrixOperator):
    def __init__(self, data_mat, y, seed=0):
        MatrixOperator.__init__(self=self, data_mat=data_mat)
        self._support = []  # list of selected atom indices
        self._L = np.empty((0, 0))  # Cholesky
        self._y = y
        self._AT_y = np.empty((0,))  # Cholesky
        self.seed = seed
    
    def add_atom(self, i_atom):
        if i_atom not in self._support:
            self._support.append(i_atom)
            a_i = self.A[:, i_atom]
            v = np.array([np.vdot(a_i, self.A[:, j]) for j in self._support])
            self._L = chol_1d_update(self._L, v)
            AT_y = np.empty(self._AT_y.size+1)
            AT_y[:-1] = self._AT_y
            AT_y[-1] = np.vdot(a_i, self._y)
            self._AT_y = AT_y

    def remove_atom(self, i_atom):
        atom_indice = self._support.index(i_atom)
        self._L = chol_1d_downdate(self._L, atom_indice)
        self._AT_y = np.delete(self._AT_y, atom_indice)
        self._support.pop(atom_indice)

    def solve(self):
        return cho_solve((self._L, True), self._AT_y)

    def change_support(self, s):
        for i_atom in self._support[::-1]:
            if not s[i_atom]:
                self.remove_atom(i_atom)
        for (i_atom, ) in np.argwhere(s):
            if i_atom not in self._support:
                self.add_atom(i_atom)

    def get_normalized_operator(self) -> Tuple['SparseSupportOperator', np.ndarray]:
        """
        Return a normalized version of the operator using normalize function of sklearn on the operator matrix
        """
        a, w_diag = normalize(self.A, axis=0, return_norm=True)
        return type(self)(a, self._y, self.seed), 1 / w_diag

    @property
    def support(self):
        return self._support

    def reset(self):
        self._support = []  # list of selected atom indices
        self._L = np.empty((0, 0))  # Cholesky
        self._AT_y = np.empty((0,))  # Cholesky


if __name__ == '__main__':
    from scipy.spatial.distance import hamming
    from sklearn.linear_model import lasso_path

    from sksea.algorithms import iht, ista, sea, amp

    n_samples = 64
    n_atoms = 2 * n_samples
    n_nonzero = n_samples // 4
    D_mat = np.random.randn(n_samples, n_atoms)
    D_op = MatrixOperator(D_mat)
    x_ref = np.zeros(n_atoms)
    s_ref = np.random.permutation(n_atoms)[:n_nonzero]
    # x_ref[s_ref] = np.random.randn(n_nonzero)
    x_ref[s_ref] = np.random.rand(n_nonzero) + 1
    x_ref[s_ref] *= (-1) ** np.random.randint(0, 2, n_nonzero)
    y = D_op @ x_ref

    alphas, coefs, _ = lasso_path(X=D_mat, y=y)
    coefs_l0 = np.count_nonzero(coefs != 0, axis=0)
    i_alpha = np.nonzero(coefs_l0 >= n_nonzero)[0][0]
    alpha = alphas[i_alpha] * n_samples

    n_iter = 10 ** 4
    x_est = {}
    res_norm = {}
    f = lambda x, linop: np.linalg.norm(linop @ x - y)
    grad_f = lambda x, linop: linop.H @ (linop @ x - y)
    x_est['Lasso'], res_norm['Lasso'] = coefs[:, i_alpha], None
    x_est['ISTA'], res_norm['ISTA'] = \
        ista(linop=D_op, y=y, alpha=alpha, n_iter=n_iter)
    x_est['AMP'], res_norm['AMP'] = \
            amp(linop=D_op, y=y, alpha=1, n_iter=n_iter, normalize=False)
    x_est['IHT'], res_norm['IHT'] = \
        iht(linop=D_op, y=y, n_nonzero=n_nonzero, n_iter=n_iter, f=f, grad_f=grad_f, normalize=False)
    x_est['SEA'], res_norm['SEA'] = \
        sea(linop=D_op, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
            keep_nonzero_x=True, f=f, grad_f=grad_f, normalize=False)
    x_est['SEA best'], res_norm['SEA best'] = \
        sea(linop=D_op, y=y, n_nonzero=n_nonzero, n_iter=n_iter, return_best=True,
            keep_nonzero_x=True, f=f, grad_f=grad_f, normalize=False)
    x_est['SEA-0'], res_norm['SEA-0'] = \
        sea(linop=D_op, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
            keep_nonzero_x=False, f=f, grad_f=grad_f, normalize=False)
    x_est['SEA-0 best'], res_norm['SEA-0 best'] = \
        sea(linop=D_op, y=y, n_nonzero=n_nonzero, n_iter=n_iter, return_best=True,
            keep_nonzero_x=False, f=f, grad_f=grad_f, normalize=False)

    print('Sparsity (ref):', np.count_nonzero(x_ref))
    for k, x in x_est.items():
        x_l0 = np.count_nonzero(x != 0)
        rel_error = np.linalg.norm(x_ref - x)/np.linalg.norm(x_ref)
        hamming_dist = hamming(x != 0, x_ref != 0)
        if res_norm[k] is not None:
            print(f'{k}: '
                  f'sparsity {x_l0}, '
                  f'rel error {rel_error:.3f}, '
                  f'Hamming dist {hamming_dist:.3f}, '
                  f'last res_norm {res_norm[k][-1]:.3f},'
                  f'min res_norm {np.min(res_norm[k]):.3f}')
        else:
            print(f'{k}:'
                  f'sparsity {x_l0}, '
                  f'rel error {rel_error:.3f}, '
                  f'Hamming dist {hamming_dist:.3f}')

    import matplotlib.pyplot as plt

    plt.figure()
    for k in res_norm:
        if res_norm[k] is None:
            continue
        plt.loglog(res_norm[k], label=k)
    plt.xlabel('Iterations')
    plt.ylabel('Norm of residue')
    plt.legend()
    plt.savefig('figures/sparse_coding_res_norm.pdf')
    plt.show()
