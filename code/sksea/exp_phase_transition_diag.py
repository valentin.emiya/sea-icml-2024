# -*- coding: utf-8 -*-
# Basic python imports
import math
from enum import Enum
from pathlib import Path
from urllib import request
from time import time
from typing import Tuple, Dict, Callable, Iterable, Union

# Installed module imports
from loguru import logger
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
# from mat4py import loadmat
from scipy.spatial.distance import hamming
from scipy.interpolate import interp1d
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots


# Sksea imports
from sksea.sparse_coding import MatrixOperator, SparseSupportOperator
from sksea.deconvolution import gen_u, gen_filter, ConvolutionOperator
from sksea.utils import algos_paper_dt_from_factor, get_color, PAPER_LAYOUT, AbstractLinearOperator

CURVE_FOLDER = "DT_curve"
HM_CURVE_FOLDER = "HM_curve"
NM_CURVE_FOLDER = "NM_curve"
ALL_HM_CURVE_FOLDER = "ALL_HM_curve"
THRESH_FMT_LABEL = [(0.95, ':r', "95%"), (0.96, ':g', "96%"), (0.97, ':b', "97%"),
                    (0.98, ':m', "98%"), (0.99, ':y', "99%")]
# THRESH_FMT_LABEL = [(0.9, ':g', "90%"), (0.91, ':b', "91%"), (0.92, ':m', "92%"), (0.93, ':c', "93%"),
#                     (0.94, ':k', "94%"), (0.95, ':r', "95%"), (0.96, ':g', "96%"), (0.97, ':b', "97%"),
#                     (0.98, ':m', "98%"), (0.99, ':y', "99%")]



class NoiseType(Enum):
    NAIVE = 1
    SNR = 2
    NOISE_LEVEL = 3


def gen_noise(noise_type, noise_factor, y, rand):
    if isinstance(noise_type, str):
        noise_type = NoiseType[noise_type]
    if isinstance(noise_type, int):
        noise_type = NoiseType(noise_type)

    noise = rand.randn(y.shape[0])
    if noise_type == NoiseType.SNR:
        noise *= 10 ** (-noise_factor / 20) * np.linalg.norm(y) / np.linalg.norm(noise)
    elif noise_type == NoiseType.NOISE_LEVEL:
        noise *= noise_factor * np.linalg.norm(y) / np.linalg.norm(noise)
    elif noise_type == NoiseType.NAIVE:
        noise = noise_factor * rand.rand(y.shape[0])
    else:
        noise = np.zeros_like(y)
    return noise


def generate_problem(n_samples=None, rho=1, delta=1, distribution='gaussian', snr=None, seed=None, n_atoms=None,
                     deconv=False, noise_factor=None, noise_type=None, use_sparse_operator=False
                     ) -> Tuple[Dict[str, Union[np.ndarray, int, 'AbstractLinearOperator']], Dict[str, np.ndarray]]:
    """
    Generate random d, x and y matrix for problems like 'd * x = y'

    :param (int or None) n_samples: Size of y, number of lines of d
    :param (float) rho: Sparsity, number of non_zeros coefficients in x divided by n_samples.
        Must be between 0 and 1
    :param (float) delta: Under-sampling factor, number of lines of d divided by its number of columns.
        Must be between 0 and 1
    :param (str) distribution: Probability distribution to use for generating d coefficients.
        Must be 'gaussian' or '[1, 2]' if deconv is False. Else must be between 0 and 4.
    :param (float or None) snr: Optional, Signal-to-Noise ratio.
    :param (int) seed: Random seed to use for d and x generation
    :param (int or None) n_atoms: Size of x, number of columns of d. If specified, n_samples must be None.
        Must be specified if n_samples is None.
    :param (bool) deconv: If True, generate a deconvolution problem
    :return:
        | problem_data:
           'obs_vect': y,
           'dict_mat': Linear Operator,
           'n_nonzero': number of non_zero coefficient in x
        | solution_data:
            'sp_vec': x
    """
    if (n_atoms is None and n_samples is None) or (n_atoms is not None and n_samples is not None):
        print(n_atoms, n_samples)
        raise ValueError("n_atoms or n_samples must be specified, the other must be None")
    elif n_samples is not None:
        n_atoms = int(np.round(n_samples / delta))
    elif n_atoms is not None:
        n_samples = int(np.round(n_atoms * delta))
    rand = np.random.RandomState(seed=seed)

    if deconv:  # TODO: To remove
        n_nonzero = int(np.round(rho * n_atoms))
        try:
            distribution = int(distribution)
        except Exception:
            raise TypeError("Can't convert distribution to int")
        if distribution not in range(5):
            raise ValueError("distribution must be between 0 and 4")
        x_ref = gen_u(n_atoms, n_nonzero, rand=rand)
        d = ConvolutionOperator(gen_filter(n_samples, distribution), n_atoms)
        y = d @ x_ref

    else:
        n_nonzero = int(np.round(rho * n_samples))
        rand_mat = rand.randn(n_samples, n_atoms)
        x_ref = np.zeros(n_atoms)
        s_ref = rand.permutation(n_atoms)[:n_nonzero]
        if distribution == 'unif':
            x_ref[s_ref] = rand.rand(n_nonzero) + 1
            x_ref[s_ref] *= (-1) ** rand.randint(0, 2, n_nonzero)
        elif distribution == 'gaussian':
            x_ref[s_ref] = rand.randn(n_nonzero)
        else:
            raise ValueError("distribution must be 'gaussian' or 'unif'")
        y = rand_mat @ x_ref
        d = SparseSupportOperator(rand_mat, y, seed) if use_sparse_operator else MatrixOperator(rand_mat, seed)

    if noise_factor is not None:
        y += gen_noise(noise_type, noise_factor, y, rand)
    problem_data = {'obs_vec': y, 'dict_mat': d, 'n_nonzero': n_nonzero}
    solution_data = {'sp_vec': x_ref}
    return problem_data, solution_data


def plot_theorical_phase_diagram_curve(curve_type="crosspolytope") -> Callable[[float], float]:
    """
    Add the theoretical recovery DT phase diagram curves to the current figure
    For more details see https://people.maths.ox.ac.uk/tanner/polytopes.shtml

    :param (str) curve_type: Type of theoretical curves.
        Select 'crosspolytope' for classic l1 regularisation.
        Select 'simplex' for l1 regularisation with non-negativity.
        Select 'orthant' for uniqueness of non-negative vectors from mean-zero measurement matrices
    :return:
        A interpolated function of the weak curve.
    """
    # Retieve curves
    filepath = Path("downloads/polytope.mat")
    if not filepath.is_file():
        filepath.parent.mkdir(exist_ok=True)
        request.urlretrieve("https://people.maths.ox.ac.uk/tanner/data/polytope.mat", filepath)
    #matlab_data = loadmat(str(filepath))
    # Plot curves in the current figure
    #p1s = np.array(matlab_data[f"rhoS_{curve_type}"])
    #p1w = np.array(matlab_data[f"rhoW_{curve_type}"])
    #plt.plot(p1s[:, 0], p1s[:, 1], 'r', label="strong")
    #plt.plot(p1w[:, 0], p1w[:, 1], 'b', label="weak")
    #return interp1d(p1w[:, 0], p1w[:, 1], fill_value=(0, 1), bounds_error=False)
    return None


def plot_empirical_phase_diagram_curve(dt_diag, axis_range_rho, axis_range_delta, threshold, fmt, label, name,
                                       fig_nums=None, axis_range_rho_sparse=None, filepath=None) -> np.ndarray:
    """
    Plot empirical recovery DT phase diagram curve of the provided diagram for a given threshold

    :param (np.ndarray) dt_diag: DT diagram
    :param (np.ndarray) axis_range_rho: Array of rho values for the rho axis of the DT diagram
    :param (np.ndarray) axis_range_delta: Array of delta values for the delta axis of the DT diagram
    :param (float) threshold: All area under the curve have a recovery percentage above this threshold
    :param (str) fmt: Format argument of matplotlib.pyplot.plot
        See https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html for more details
    :param (str) label: Label to use for the curve
    :param (str) name: Name of the algorithm used in the experiment
    :param (List[str or int]) fig_nums: Identifier (label or num) of the pyplot figures above which
        we want to plot the curves. If not specified, the current pyplot figure is used.
    :param (str or Path) filepath: If specified, save the numpy array containing the curve at this location
    :return: Empirical DT curve
    """
    curve = np.ones((axis_range_delta.shape[0], 2))
    curve[:, 0] = axis_range_delta
    if fig_nums is None:
        fig_nums = [plt.gcf().number]
    # Compute the recovery curve
    for i_delta, delta in enumerate(axis_range_delta):
        if axis_range_rho_sparse is None:
            iterator = axis_range_rho
        else:
            iterator = axis_range_rho_sparse[i_delta]
        for i_rho in range(iterator.shape[0]):
            # Search for the first rho with a recovery percentage under the threshold for each delta
            if dt_diag[i_rho, i_delta] < threshold:
                curve[i_delta] = (delta, iterator[i_rho - 1])
                break

    if filepath is not None:
        filepath.parent.mkdir(exist_ok=True, parents=True)
        np.save(filepath, curve)

    # Plot curve in the selected figures
    for num in fig_nums:
        plt.figure(num=num)
        if num == label:  # For the plot with all algorithms
            if name in ['SEA_BEST', 'OMP']:
                name_fmt = ':'
            else:
                name_fmt = ''
            plt.plot(curve[:, 0], curve[:, 1], name_fmt, label=name)
        else:  # For the DT diagram of each algorithm
            plt.plot(curve[:, 0], curve[:, 1], fmt, label=label)
    return curve


def compute_phase_transition_diagram(n_samples, n_steps, n_runs, n_iter,
                                     algo, distribution, name, rel_tol=-np.inf, **kwargs) -> None:
    """
    Solve ||D * x - y||_2^2 with the provided algorithm for various configurations.
    Results are saved in the 'data_result' folder

    :param (int) n_samples: Size of y, number of lines of D
    :param (int or Tuple[int, int]) n_steps: Discretization of rho and delta.
        If an integer is provided, the same discretization is used for both rho and delta.
        Else, the first element is the discretization for rho, and the second the one for delta
    :param (int) n_runs: Number of times each configuration is tested
    :param (int) n_iter: Number max of iteration that the algorithm can do
    :param (Callable) algo: Solveur to use
    :param (str) distribution: Probability distribution to use for generating D coefficients.
        Must be 'gaussian' or '[1, 2]'
    :param (str) name: Name of the algorithm
    :param (float) rel_tol: The algorithm stops when the iterations relative difference is lower than rel_tol
    """
    # Creating output directory
    data_dir = Path('data_results')
    data_dir.mkdir(exist_ok=True)

    if isinstance(n_steps, int):
        n_steps_rho, n_steps_delta = n_steps, n_steps
    elif isinstance(n_steps, tuple):
        n_steps_rho, n_steps_delta = n_steps
    else:
        raise TypeError("n_step must be an int or a tuple")

    # Initialize arrays
    axis_range_rho = (np.arange(n_steps_rho) + 1) / n_steps_rho
    axis_range_delta = (np.arange(n_steps_delta) + 1) / n_steps_delta
    hamming_dist = np.empty((n_steps_rho, n_steps_delta, n_runs))
    recovery_results = np.empty((n_steps_rho, n_steps_delta, n_runs), dtype=bool)
    eucl_dist_rel = np.empty((n_steps_rho, n_steps_delta, n_runs))

    print(f'Algo {name}')
    start_time = time()
    for i_run in range(n_runs):
        print(f'Run {i_run}/{n_runs}')
        for i_rho, rho in enumerate(axis_range_rho):
            for i_delta, delta in enumerate(axis_range_delta):
                # print(f'(rho, delta)=({rho}, {delta})')
                problem_data, solution_data = \
                    generate_problem(n_samples=n_samples, rho=rho, delta=delta, distribution=distribution, snr=None,
                                     seed=i_run)
                y = problem_data['obs_vec']
                D = problem_data['dict_mat']
                n_nonzero = problem_data['n_nonzero']
                x_est, _ = algo(linop=MatrixOperator(D), y=y, n_nonzero=n_nonzero,
                                n_iter=n_iter, rel_tol=rel_tol)
                x_ref = solution_data['sp_vec']
                supp_ref = x_ref != 0
                supp_est = x_est != 0
                hamming_dist[i_rho, i_delta, i_run] = \
                    hamming(supp_est, supp_ref)
                recovery_results[i_rho, i_delta, i_run] = \
                    np.all(supp_est == supp_ref)
                # Compute euclidian distance for DT phase diagram
                dist_rel = np.linalg.norm(x_ref - x_est) / np.linalg.norm(x_ref)
                eucl_dist_rel[i_rho, i_delta, i_run] = dist_rel

        out_file = f'{name}_{n_samples}_{n_steps}_{n_runs}_{n_iter}_{rel_tol}.npz'
        np.savez(data_dir / out_file,
                 hamming_dist=hamming_dist,
                 recovery_results=recovery_results,
                 axis_range_rho=axis_range_rho,
                 axis_range_delta=axis_range_delta,
                 eucl_dist_rel=eucl_dist_rel,
                 name=name,
                 time_spent=time() - start_time)


def extend_data_on_y_axes(data:np.ndarray, y_axes:np.ndarray):
    new_data = np.ones_like(data) * np.nan
    ref_axis = y_axes[-1]
    for j, y_axis in enumerate(y_axes):
        for i, y_ref_value in enumerate(ref_axis):
            y_value = (np.abs(y_axis[y_axis != 0] - y_ref_value)).argmin()
            new_data[i, j] = data[y_value, j]
    return new_data


def create_and_save_diagram(data, title, extent, path=None, cmap=None, clim=True, norm=None) -> None:
    """
    Create a 2D figure with the specified parameters using imshow.
    Documentation: https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.imshow.html

    :param (np.ndarray) data: 2D array to plot
    :param (str) title: Title of the plot
    :param (Optional[Tuple[float, float, Union[float, np.ndarray], float]]) extent: Bounding box of data coordinates.
        See imshow doc for more details.
    :param (str or Path) path: Path of the output file
    :param (str or None) cmap: Colormap to use. See imshow doc for more details.
    :param (bool) clim: If true, set the colorbar between 0 and 1.
    :param (matplotlib.colors.Normalize or None) norm: Optional. Normalisation of the colorbar

    """
    if extent is not None:
        if extent[-1] is None:
            y_axes = extent[-2]
            data_to_show = extend_data_on_y_axes(data, y_axes)
            extent_to_show = (extent[0], extent[1], y_axes[-1, 0], y_axes[-1, -1])
        else:
            data_to_show = data
            extent_to_show = extent

        plt.figure()
        plt.imshow(data_to_show, cmap=cmap, origin='lower', extent=extent_to_show, norm=norm)
        if clim:
            if norm is None:
                plt.clim(0, 1)
        """    else:
                plt.clim(1e-5, 1)"""
        plt.colorbar()
        plt.xlabel(r'$\delta$')
        plt.ylabel(r'$\rho$')
        plt.title(title)
        if path is not None:
            plt.savefig(path)
            plt.close()


def sea_vs_sea_best(sea_data, sea_best_data, fig_dir, suffix, extent):
    """
    Plot figures for SEA VS SEA_BEST comparison

    :param (Dict[str, np.ndarray]) sea_data: Data from sea experiment
    :param (Dict[str, np.ndarray]) sea_best_data: Data from sea_best experiment
    :param (Path) fig_dir: Path of the folder containing the figures
    :param (str) suffix: Suffix of the output file name
    :param (Tuple[float, float, float, float] or None) extent: Bounding box of data coordinates.
        See imshow doc for more details.
    """
    # Load data
    sea_iter = sea_data.get("iterations")
    sea_best_iter = sea_best_data.get("iterations")
    sea_last_res = sea_data.get("last_res")
    sea_best_last_res = sea_best_data.get("last_res")
    sea_eucl = sea_data.get('eucl_dist_rel')
    sea_best_eucl = sea_best_data.get("eucl_dist_rel")

    # Iterations comparison
    create_and_save_diagram(np.mean(sea_iter - sea_best_iter, axis=2), "SEA/SEA_BEST iterations difference",
                            extent, fig_dir / f'iterations_SEA_diff{suffix}.pdf', clim=False)
    create_and_save_diagram(np.mean((sea_iter - sea_best_iter) / sea_iter, axis=2),
                            "SEA/SEA_BEST iterations relative difference", extent,
                            fig_dir / f'iterations_SEA_rel_diff{suffix}.pdf')

    # Last residual norm comparison
    denom = (sea_last_res == 0) + sea_last_res  # Remove division by 0
    create_and_save_diagram(np.mean((sea_last_res - sea_best_last_res) / denom, axis=2),
                            "SEA/SEA_BEST residual norm relative difference", extent,
                            fig_dir / f'res_norm_SEA_rel_diff{suffix}.pdf')

    # Relative distance comparison
    denom = (sea_eucl == 0) + sea_eucl  # Remove division by 0
    create_and_save_diagram(np.mean((sea_eucl - sea_best_eucl) / denom, axis=2),
                            "SEA/SEA_BEST relative difference of relative euclidian distance wrt x", extent,
                            fig_dir / f'eucl_SEA_rel_diff{suffix}.pdf')
    create_and_save_diagram(np.mean(np.abs(sea_eucl - sea_best_eucl), axis=2),
                            "SEA/SEA_BEST difference of relative euclidian distance wrt x", extent,
                            fig_dir / f'eucl_SEA_diff{suffix}.pdf', clim=True, norm=LogNorm())
    create_and_save_diagram(np.mean(sea_eucl, axis=2),
                            "SEA relative euclidian distance wrt x", extent,
                            fig_dir / f'eucl_SEA_{suffix}.pdf', clim=False)
    create_and_save_diagram(np.mean(sea_best_eucl, axis=2),
                            "SEA_BEST relative euclidian distance wrt x", extent,
                            fig_dir / f'eucl_SEA_BEST_{suffix}.pdf', clim=False)

    # DT comparison by threshold value
    thresholds = [f"{i}e-{p}" for p in range(1, 8) for i in range(9, 0, -1)]
    maxs = np.empty(len(thresholds))
    mins = np.empty(len(thresholds))
    means = np.empty(len(thresholds))
    plt.figure()
    for idx, epsilon in enumerate(thresholds):
        eps = float(epsilon)
        diff = np.mean(sea_best_eucl < eps, axis=2) - np.mean(sea_eucl < eps, axis=2)
        maxs[idx] = diff.max()
        mins[idx] = diff.min()
        means[idx] = diff.mean()
    x = [float(e) for e in thresholds]
    plt.semilogx(x, mins, label='min')
    plt.semilogx(x, means, label='mean')
    plt.semilogx(x, maxs, label='max')
    plt.title("Difference between DT diagrams by threshold value for SEA VS SEA_BEST")
    plt.legend()
    plt.savefig(fig_dir / 'epsilon_selection.pdf')
    plt.close()


def plot_all_hm_curves(diag, axis_range_rho, axis_range_delta, axis_range_rho_sparse, filepath, name, success, n_samples):
    """
    Plot all recovery curves in the same figure

    :param (np.ndarray) diag: DT diagram
    :param (np.ndarray) axis_range_rho: Array of rho values for the rho axis of the DT diagram
    :param (np.ndarray) axis_range_delta: Array of delta values for the delta axis of the DT diagram
    :param (np.ndarray) axis_range_rho_sparse: Array of rho values for the rho axis of the DT diagram by delta
    :param (str or Path) filepath: Save the numpy array containing the curve at this location
    :param (str) name: Name of the algorithm used in the experiment
    """
    curve = np.ones((axis_range_delta.shape[0], 101))
    curve[:, 0] = axis_range_delta

    # Compute all recovery curve
    for i_delta, delta in enumerate(axis_range_delta):
        if axis_range_rho_sparse is None:
            iterator = axis_range_rho
        else:
            iterator = axis_range_rho_sparse[i_delta]
        last = 100
        for i_rho in range(iterator.shape[0]):
            current = int(diag[i_rho, i_delta] * 100)
            if current < last:
                for j in range(current + 1, last+1):
                    if j != 0:
                        curve[i_delta, j] = iterator[i_rho - 1]
                last = current
            if current == 0:
                break

    # Display all recovery curve
    cmin = int(success * 100)
    cmax = 99
    fig = go.Figure()
    colorscale = 'Rainbow'
    # add invisible markers to display the colorbar without displaying the markers
    # fig.add_trace(go.Scatter(
    #     x=[0.5,],
    #     y=[0,],
    #     mode='markers',
    #     marker=dict(
    #         size=0,
    #         color="rgba(0,0,0,0)",
    #         colorscale=colorscale,
    #         cmin=cmin,
    #         cmax=cmax,
    #         colorbar=dict(thickness=40, orientation="h")
    #     ),
    #     showlegend=False
    # ))
    for i in range(cmin, cmax + 1):
        fig.add_trace(go.Scatter(x=curve[:, 0], y=curve[:, i], mode='lines', name=f"{i}%",
                                 # fill='tonexty',
                                 line=dict(width=1, color=get_color(colorscale, (i - cmin) / (cmax - cmin))),
                                 # showlegend=False
                                 ))
    # fig.add_trace(go.Scatter(x=curve[:, 0], y=np.zeros(curve.shape[0]), mode='lines', fill='tonexty', showlegend=False,
    #                          line=dict(width=0.1, color=get_color(colorscale, 1)), name="bottom"))
    fig.update_layout(title=f'All HM curves for {name}', xaxis_title=r'$\delta$', yaxis_title=r'$\rho$', plot_bgcolor='white',)
    fig.write_html(filepath, include_mathjax='cdn')

    # Display heatmap
    if n_samples is not None:
        fig = go.Figure()
        fig.add_trace(go.Heatmap(z=diag, x=axis_range_delta, y=axis_range_rho_sparse[0], zmin=success, zmax=1))

        fig.update_layout(title=f'Heatmap for {name}', xaxis_title=r'$\delta$', yaxis_title=r'$\rho$', plot_bgcolor='white',)
        fig.write_html(filepath.parent / f"heatmap_{name}.html", include_mathjax='cdn')


def one_column_analysis(hamming_dist, axis_range_rho, axis_range_delta, axis_range_rho_sparse, name):
    """
    Function for debugging ELS
    Plot hamming distance for a given delta and rho

    :param (np.ndarray) hamming_dist: Hamming distance results
    :param (np.ndarray) axis_range_rho: Array of rho values for the rho axis of the DT diagram
    :param (np.ndarray) axis_range_delta: Array of delta values for the delta axis of the DT diagram
    :param (np.ndarray) axis_range_rho_sparse: Array of rho values for the rho axis of the DT diagram by delta
    :param (str) name: Name of the algorithm used in the experiment
    """
    delta = 0.7
    rho = 0.1457143
    i_delta = np.argmin(np.abs(axis_range_delta - delta))
    rho_axis = axis_range_rho_sparse[i_delta]
    i_rho = np.argmin(np.abs(rho_axis - rho)) + 1
    fig = px.line(y=hamming_dist[i_rho, i_delta, :], title=f"Hamming distance for {name}")
    fig.show()


def plot_results(n_samples, n_steps, n_runs, n_iter, epsilon, rel_tol=-np.inf, n_atoms=None, epsilon_x=1e-4,
                 expe_name='', success=0.01) -> None:
    """
    Plot results stored in the 'data_results' folder.
    See compute_phase_transition_diagram documentation for details

    :param (int or None) n_samples: Size of y, number of lines of D
    :param (int or Tuple[int, int]) n_steps: Discretization of rho and delta.
        If an integer is provided, the same discretization is used for both rho and delta.
        Else, the first element is the discretization for rho, and the second the one for delta
    :param (int) n_runs: Number of times each configuration is tested
    :param (int) n_iter: Number max of iteration that the algorithm has done
    :param (float) epsilon: Relative error criteria for DT phase diagram
    :param (float) rel_tol: The algorithm stopped when the iterations relative difference was lower than rel_tol
    :param (int or None) n_atoms: Size of x, number of columns of D. If specified, n_samples must be None.
    :param (float) epsilon_x: Threshold on the relative distance between x_approx and x_true
        for the definition of a successful recovery
    :param (str) expe_name: Name of the current experiment
    :param (float) success: Minimal success rate used to continue DT computation
    """
    suffix = f'_{n_samples}_{n_steps}_{n_runs}_{n_iter}_{rel_tol}_{epsilon}_{n_atoms}_{expe_name}'
    plot_suffix = suffix + f"_{epsilon_x}"
    fig_dir = Path('figures') / plot_suffix[1:]
    (fig_dir / CURVE_FOLDER).mkdir(exist_ok=True, parents=True)
    (fig_dir / HM_CURVE_FOLDER).mkdir(exist_ok=True, parents=True)
    data_dir = Path('data_results')
    # Variable instantiation
    sea_data, sea_best_data, hamming_dist_iht, sea_els_best_data, els_data = None, None, None, None, None
    for f in Path(data_dir).glob(f'*{suffix}_*.npz'):
        # Loading results
        data = np.load(f)
        hamming_dist = data['hamming_dist']  # [:, :, :200]
        recovery_results = data.get('recovery_results')
        eucl_dist_rel = data.get('eucl_dist_rel')
        axis_range_delta = data.get('axis_range_delta')
        axis_range_rho = data.get('axis_range_rho')
        axis_range_rho_sparse = data.get('axis_range_rho_per_sparcity')
        supp_est_in_sup_ref = data.get('supp_est_in_sup_ref')
        eucl_dist_rel_y = data.get('eucl_dist_rel_y')

        stem, index = f.stem.rsplit('_', 1)
        algo_name = stem.split('_')[0]
        name = algo_name if index == '0' else algo_name + '_BEST'
        time_algo = data.get('time')
        iterations = data.get('iterations')
        stem = stem.replace(algo_name, name)

        if axis_range_rho is None and axis_range_delta is None:
            extent = None
        elif axis_range_rho is None:
            extent = (axis_range_delta[0], axis_range_delta[-1], axis_range_rho_sparse, None)
        else:
            extent = (axis_range_delta[0], axis_range_delta[-1], axis_range_rho[0], axis_range_rho[-1])

        # Recovery diagram
        if recovery_results is not None:  # For retro-compatibility
            create_and_save_diagram(np.mean(recovery_results, axis=2), name, extent, fig_dir / f'phase_diag_{stem}.pdf')

        # Euclidian distance diagram
        create_and_save_diagram(np.mean(eucl_dist_rel, axis=2), name, extent, fig_dir / f'eucl_dist_{stem}.pdf')

        # DT phase diagram
        if eucl_dist_rel is not None:  # For retro-compatibility
            create_and_save_diagram(np.mean(eucl_dist_rel < epsilon_x, axis=2), name, extent, cmap='Greys_r')
            num = plt.gcf().number  # Store principal (active) figure number

            # Add DT phase diagram curves
            # plot_theorical_phase_diagram_curve()
            dt_diag = np.mean(eucl_dist_rel < epsilon_x, axis=2)
            for threshold, fmt, label in THRESH_FMT_LABEL:
                plot_empirical_phase_diagram_curve(dt_diag, axis_range_rho, axis_range_delta,
                                                   threshold, fmt, label, name, (num, label), axis_range_rho_sparse,
                                                   filepath=fig_dir / CURVE_FOLDER / f"{name}_{threshold}.npy")
            plt.figure(num=num)  # Restore principal figure
            plt.legend()

            plt.savefig(fig_dir / f'DT_phase_diag_{stem}.pdf')
            plt.close()

        # Time spent diagram
        if time_algo is not None:  # For retro-compatibility
            create_and_save_diagram(np.mean(time_algo, axis=2), name, extent,
                                    fig_dir / f'time_{stem}.pdf', clim=False)

        # Hamming distance diagram
        create_and_save_diagram(1 - np.mean(hamming_dist, axis=2), name, extent, fig_dir / f'hamming_dist_{stem}.pdf')

        # Hamming phase diagram
        # if "els" in name.lower() and "sea" not in name.lower():
        #     one_column_analysis(hamming_dist, axis_range_rho, axis_range_delta, axis_range_rho_sparse, name)
        hm_diag = np.mean(hamming_dist == 0, axis=2)
        diags = [(hm_diag, "HM", HM_CURVE_FOLDER)]
        if supp_est_in_sup_ref is not None and eucl_dist_rel_y is not None:
            nm_diag = np.mean((hamming_dist == 0) | (supp_est_in_sup_ref & (eucl_dist_rel_y < 1e-2)), axis=2)
            diags.append((nm_diag, "NM", NM_CURVE_FOLDER))
        for diag, diag_name, curve_folder in diags:
            create_and_save_diagram(hm_diag, name, extent, cmap='Greys_r')
            num = plt.gcf().number  # Store principal (active) figure number
            for threshold, fmt, label in THRESH_FMT_LABEL:
                plot_empirical_phase_diagram_curve(diag, axis_range_rho, axis_range_delta,
                                                   threshold, fmt, label, name, (num, label),
                                                   axis_range_rho_sparse,
                                                   filepath=fig_dir / curve_folder / f"{name}_{threshold}.npy")
            plt.figure(num=num)  # Restore principal figure
            plt.legend()
            plt.savefig(fig_dir / f'{diag_name}_phase_diag_{stem}.pdf')
            plt.close()

        plot_all_hm_curves(hm_diag, axis_range_rho, axis_range_delta,
                           axis_range_rho_sparse, fig_dir / f'ALL_HM_phase_diag_{stem}.html', name, success, n_samples)

        # Iterations diagram
        create_and_save_diagram(np.mean(iterations, axis=2), name, extent,
                                fig_dir / f'iterations_{stem}.pdf', clim=False)

        logger.debug(f"Plotting {f.stem} done")

        if f.stem.startswith('ELS') and f.stem.endswith('0'):
            els_data = data
        elif f.stem.startswith('SEAFAST-els') and f.stem.endswith('1'):
            sea_els_best_data = data
        # elif f.stem.startswith('IHT'):
        #     hamming_dist_iht = 1 - np.mean(hamming_dist, axis=2)
        #     recovery_results_iht = np.mean(recovery_results, axis=2)
        # elif f.stem.startswith('SEA') and f.stem.endswith('1'):
        #     sea_best_data = data
        # elif f.stem.startswith('SEA') and f.stem.endswith('0'):
        #     sea_data = data

    # Save DT comparison
    for _, _, label in THRESH_FMT_LABEL:
        plt.figure(num=label)
        plt.title(label)
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.legend()
        plt.savefig(fig_dir / f'DT_{label}{suffix}.pdf')

    # For debugging SEA_ELS
    if sea_els_best_data is not None and els_data is not None:
        debug_sea_els(sea_els_best_data, els_data, fig_dir, suffix)

    # Save sea/sea_best comparison
    # if sea_data is not None and sea_best_data is not None:
    #     sea_vs_sea_best(sea_data, sea_best_data, fig_dir, suffix, extent)

    # Comparison of all algorithm with IHT
    # if hamming_dist_iht is not None:
    #     for f in Path(data_dir).glob(f'*{suffix}.npz'):
    #         if f.stem.startswith('IHT'):
    #             continue
    #         data = np.load(f)
    #         hamming_dist = data['hamming_dist']
    #         recovery_results = data['recovery_results']
    #         axis_range = data.get('axis_range')  # For retro-compatibility
    #         if axis_range is None:
    #             axis_range_delta = data.get('axis_range_delta')
    #             axis_range_rho = data.get('axis_range_rho')
    #         else:  # For retro-compatibility
    #             axis_range_delta, axis_range_rho = axis_range, axis_range
    #         stem, index = f.stem.rsplit('_', 1)
    #         name = str(data['name']) if index == 0 else str(data['name']) + '_BEST'
    #
    #         extent = (axis_range_delta[0], axis_range_delta[-1], axis_range_rho[0], axis_range_rho[-1])
    #
    #         # Hamming distance difference
    #         d_results = 1 - np.mean(hamming_dist, axis=2) - hamming_dist_iht
    #         create_and_save_diagram(d_results, f'{name} - IHT $\\approx{np.mean(d_results[~np.isnan(d_results)]):.3g}$',
    #                                 extent, fig_dir / f'diff_hamming_dist_{stem}.pdf', clim=False)
    #
    #         # Recovery result difference
    #         d_results = np.mean(recovery_results, axis=2) - recovery_results_iht
    #         create_and_save_diagram(d_results, f'{name} - IHT $\\approx{np.mean(d_results[~np.isnan(d_results)]):.3g}$',
    #                                 extent, fig_dir / f'diff_phase_diag_{stem}.pdf', clim=False)


def plot_threshold_curves_comparison(expe_names, filename, epsilon_x=1e-4, nm=False) -> None:
    """
    Create a figures for using all algorithms available for each threshold value

    :param Iterable[str] expe_names: Name of the experiments to compile together
    :param (str) filename: Name of the html file for saving the plot
    :return:
    """
    fig_dir = Path("figures")
    plot_dir = fig_dir / "comparison"
    plot_dir.mkdir(exist_ok=True)
    colors = ['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c',
              '#fdbf6f', '#ff7f00', '#cab2d6', '#6a3d9a', '#ffff99', '#b15928']

    if nm:
        curve_folder = NM_CURVE_FOLDER
        file_suffix = "nm"
    else:
        curve_folder = HM_CURVE_FOLDER
        file_suffix = "hm"

    # Get curves
    paths = []
    for suff in expe_names:
        paths.extend(list(fig_dir.glob(f'*_{suff}_*{epsilon_x}')))

    # Plot curves
    fig = go.Figure()
    for threshold, _, label in THRESH_FMT_LABEL:
        idx = 0
        for path in paths:
            npy_files = list(Path(path / curve_folder).glob(f"*_{threshold}.npy"))
            npy_files.sort()  # Keep the color order
            for curve_path in npy_files:
                curve = np.load(str(curve_path))
                name = curve_path.name.rsplit('_', 1)[0]
                for suff in expe_names:  # Add experiement name
                    if f'_{suff}_' in str(path) and suff != '':
                        name += '_' + suff
                fig.add_trace(go.Scatter(x=curve[:, 0], y=curve[:, 1], line=dict(color=colors[idx % len(colors)]),
                                         name=name, visible=False))
                idx += 1

    # Slider (start) --------------------------
    steps = []
    n_data = len(fig.data)
    n = n_data / len(THRESH_FMT_LABEL)
    for idx, (threshold, _, label) in enumerate(THRESH_FMT_LABEL):
        step = dict(
            # Update method allows us to update both trace and layout properties
            method='update',
            args=[
                # Make the ith trace visible
                {'visible': [idx * n <= i < (idx + 1) * n for i in range(n_data)]},

                # Set the title for the ith trace
                {'title.text': f"{file_suffix.upper()} threshold comparison at {label}"}],
        )
        steps.append(step)
    sliders = [go.layout.Slider(
        active=1,
        len=0.5,
        currentvalue={"prefix": "Threshold: "},
        steps=steps
    )]
    # Slider (stop) --------------------------

    fig.update_layout(
        xaxis_title=r"$\delta$",
        yaxis_title=r"$\rho$",
        legend_title="Algorithms",
        sliders=sliders,
        xaxis_range=[0, 1],
        yaxis_range=[0, 1],
        xaxis_autorange=False,
        yaxis_autorange=False,
    )
    filename = f'{filename}_{epsilon_x}.html' if filename.endswith('.html') else f'{filename}_{epsilon_x}'
    fig.write_html(plot_dir / f'{filename}_{file_suffix}.html', include_mathjax='cdn')


def plot_paper_curves(expe_name, epsilon_x=1e-4, factors=(256,), dt=False, threshold_position=-5) -> None:
    threshold, _, label = THRESH_FMT_LABEL[threshold_position]
    fig_dir = Path("figures")
    plot_dir = fig_dir / "comparison"
    plot_dir.mkdir(exist_ok=True)

    if dt:
        curve_folder = CURVE_FOLDER
        file_suffix = "dt"
    else:
        curve_folder = HM_CURVE_FOLDER
        file_suffix = "hm"

    algos_paper_dt = algos_paper_dt_from_factor(factors[0])

    # Get curves
    path = list(fig_dir.glob(f'*_{expe_name}_*{epsilon_x}'))[0]
    npy_files = {np_path.name.rsplit('_', 1)[0]: np_path
                 for np_path in Path(path / curve_folder).glob(f"*_{threshold}.npy")
                 if np_path.name.rsplit('_', 1)[0] in algos_paper_dt.keys()}

    legend_ranks = ["SEA_ELS", "SEA_OMP", "ELS", "OMPR", "IHT_OMP", "HTP_OMP", "SEA_0", "OMP", "IHT", "HTP"]

    fig = go.Figure()
    for algo, info in algos_paper_dt.items():
        if algo in npy_files.keys() and npy_files[algo].is_file:
            curve = np.load(str(npy_files[algo]))
            if info["disp_name"] == "ELS":
                display_name = "$\\text{ELS}, \\text{IHT}_{\\text{ELS}}, \\text{HTP}_{\\text{ELS}}$"
            else:
                display_name = info["name"]
            fig.add_trace(go.Scatter(x=curve[:, 0], y=curve[:, 1], line=info["line"],
                                     name=display_name, legendrank=legend_ranks.index(info["disp_name"])))

    fig.update_layout(
        xaxis_title=r"$\zeta = m/n$",
        yaxis_title=r"$\rho = k/m$",
        **PAPER_LAYOUT,
        legend=dict(
            orientation='h',
            y=1,
            x=0, #0.05,
            xanchor="left",
            yanchor="top",
            bgcolor= 'rgba(0,0,0,0)',
            entrywidth=160
        )
    )
    fig.update_layout(legend=dict(title=None), font_size=17)
    fig.write_html(plot_dir / f"paper_{expe_name}_{file_suffix}_{threshold}.html", include_mathjax='cdn')
    # fig.update_layout(showlegend=False)
    # fig.write_html(plot_dir / f"paper_{expe_name}_{file_suffix}_{threshold}_no_legend.html", include_mathjax='cdn')


def debug_sea_els(sea_els_best_data, els_data, fig_dir, suffix):
    """
    Plot figures for debugging SEA_ELS

    :param (Dict[str, np.ndarray]) sea_els_best_data: Data from sea_els_best
    :param (Dict[str, np.ndarray]) els_data: Data from els
    :param (Path) fig_dir: Path of the folder containing the figures
    :param (str) suffix: Suffix of the output file name
    """
    # Load data

    axis_range_delta = sea_els_best_data.get('axis_range_delta')
    axis_range_rho_sparse = sea_els_best_data.get('axis_range_rho_per_sparcity')

    sea_els_best_iter = sea_els_best_data.get("iterations")
    sea_els_best_last_res = sea_els_best_data.get("last_res")
    sea_els_best_eucl = sea_els_best_data.get("eucl_dist_rel")
    sea_els_best_time = sea_els_best_data.get("time_spent")
    sea_els_best_recovery_results = sea_els_best_data.get("recovery_results")
    sea_els_best_hamming_dist = sea_els_best_data.get("hamming_dist")
    sea_els_best_supp = sea_els_best_data.get("supp_est_in_sup_ref")


    els_iter = els_data.get("iterations")
    els_last_res = els_data.get("last_res")
    els_eucl = els_data.get("eucl_dist_rel")
    els_eucl_y = els_data.get("eucl_dist_rel_y")
    els_time = els_data.get("time_spent")
    els_recovery_results = els_data.get("recovery_results")
    els_hamming_dist = els_data.get("hamming_dist")
    els_supp = els_data.get("supp_est_in_sup_ref")

    # Hamming distance comparison
    hm_diff = els_hamming_dist - sea_els_best_hamming_dist
    # logger.debug(f"Hamming distance difference negative positions: {list(zip(*np.where(hm_diff < 0)))}")

    # metric comparison
    hm = np.mean(els_hamming_dist == 0, axis=-1)
    y_err_max = np.max(np.abs(els_eucl_y), axis=-1)
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    for i, delta in enumerate(axis_range_delta):
        fig.add_trace(go.Scatter(x=axis_range_rho_sparse[i], y=hm[:,i], name=f"Hamming distance for delta={delta}",
                                 ), secondary_y=False)
        fig.add_trace(go.Scatter(x=axis_range_rho_sparse[i], y=y_err_max[:,i], name=f"Euclidian distance for delta={delta}",
                                 line=dict(dash='dash')), secondary_y=True)
    fig.update_layout(title="Hamming distance and Euclidian distance for SEA_ELS")
    fig.update_xaxes(title_text=r"$\rho$")
    fig.update_yaxes(title_text="Hamming distance", secondary_y=False)
    fig.update_yaxes(title_text="Euclidian distance", secondary_y=True)
    fig.write_html(fig_dir / f"ELS.html", include_mathjax='cdn')
