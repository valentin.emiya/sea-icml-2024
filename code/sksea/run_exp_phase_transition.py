"""
File for running phase transition experiment on the cluster (via command line).
You can display the help by using the command `python run_exp_compute_one_phase_transistion_run.py --help`
The CLI was made with the click library.
This file contains also a parallel version of the phase transition experiment
"""
import pickle
# Basic python imports
from itertools import chain
from pathlib import Path
from time import time
from typing import Tuple, Dict, Union, List
import shutil
import socket
import os

# Installed module imports
import click
import pandas as pd
from hyperopt import hp, fmin, tpe, Trials
from loguru import logger
import numpy as np
import ray
from scipy.spatial.distance import hamming

# Sksea imports
from sksea.exp_phase_transition_diag import generate_problem, plot_results, \
    plot_paper_curves, NoiseType, plot_threshold_curves_comparison
from sksea.plot_icml import plot_dt_paper, plot_dt_paper_zoom
from sksea.training_tasks import ALL, select_algo, get_algo_dict, ALGOS_TYPE

DATA_DIR = Path('data_results')
ONE_POINT_DIR = DATA_DIR / 'one_point' / "raw_files"


def get_filename(rho, delta, index, ext='npz') -> str:
    """
    Return the temporary file name for a given (rho, delta)

    :param (float) rho: Current value of rho
    :param (float) delta: Current value of delta
    :param (int or None) index: Index of the result
    """
    if index is None:
        return f"{int(rho * 100000)}_{int(delta * 100000)}.{ext}"
    else:
        return f"{int(rho * 100000)}_{int(delta * 100000)}_{index}.{ext}"


# @ray.remote
def solve_problem(run_number, n_samples, rho, delta, n_iter, algo, distribution, rel_tol, n_atoms, factor, deconv,
                  noise_factor, noise_type, **algo_params) -> List[Dict[str, Union[float, int]]]:
    """
    Solve a problem like 'D @ x = y' with the following parameters

    :param (int) run_number: Used as a seed for the random generation of the problem
    :param (int or None) n_samples: Size of y, number of lines of D
    :param (float) rho: Sparsity, number of non_zeros coefficients in x divided by n_samples.
        Must be between 0 and 1
    :param (float) delta: Under-sampling factor, number of lines of D divided by its number of columns.
        Must be between 0 and 1
    :param (int or None) n_iter: Number max of iteration that the algorithm can do
    :param (Callable) algo: Solver to use
    :param (str) distribution: Probability distribution to use for generating D coefficients.
        Must be 'gaussian' or '[1, 2]' if deconv is False. Else must be between 0 and 4.
    :param (float) rel_tol: The algorithm stops when the iterations relative difference is lower than rel_tol
    :param (int or None) n_atoms: Size of x, number of columns of D. If specified, n_samples must be None.
    :param (int or None) factor: The number of iterations of the algorithm will be factor * sparsity
        Used if n_iter is None.
    TODO :return: Dictionaries with resolution metrics
    """
    results = []
    problem_data, solution_data = \
        generate_problem(n_samples=n_samples, rho=rho, delta=delta, distribution=distribution, noise_type=noise_type,
                         seed=run_number, n_atoms=n_atoms, deconv=deconv, noise_factor=noise_factor,
                         use_sparse_operator=True)
    y = problem_data['obs_vec']
    d = problem_data['dict_mat']
    n_nonzero = problem_data['n_nonzero']
    if n_nonzero > 0 and y.shape[0] > 1:  # For removing zero-sparse vector and one line matrices
        start_time = time()
        params = dict(n_iter=factor * n_nonzero) if n_iter is None else dict(n_iter=n_iter)
        params.update(**algo_params)
        out = algo(linop=d, y=y, n_nonzero=n_nonzero, rel_tol=rel_tol,
                   f=lambda x, linop: np.linalg.norm(linop @ x - y),
                   grad_f=lambda x, linop: linop.H @ (linop @ x - y),
                   is_mse=True, **params)

        outputs = out if isinstance(out[0], tuple) else (out,)
        for idx, (x_est, res_norm, *sea_supp) in enumerate(outputs):
            results.append({})
            results[idx]["time"] = time() - start_time
            # Results from first experiments: Hamming distance and support recovery
            x_ref: np.ndarray = solution_data['sp_vec']
            supp_ref: np.ndarray = x_ref != 0
            supp_est = x_est != 0
            results[idx]["hamming_dist"] = hamming(supp_est, supp_ref)
            # Compute euclidian distance for DT phase diagram
            results[idx]["eucl_dist_rel"] = np.linalg.norm(x_ref - x_est) / np.linalg.norm(x_ref)
            # Compute euclidian distance on the observation for DT phase diagram
            results[idx]["eucl_dist_rel_y"] = np.linalg.norm(d @ x_ref - d @ x_est) / np.linalg.norm(d @ x_ref)
            results[idx]["supp_est_in_sup_ref"] = np.all(supp_ref[supp_est])
            # Keep information about the optimization process
            results[idx]["last_res"] = res_norm[-1]
            results[idx]["iterations"] = len(res_norm)
            # if len(sea_supp) > 0:
            #     results[idx]["support_history"] = sea_supp[0]
    else:
        results.append({})
    return results


@ray.remote
def solve_batch_problem(run_numbers, n_samples, rho, delta, n_iter, algo, distribution, rel_tol, n_atoms, factor,
                        deconv, noise_factor, noise_type, **algo_params):
    return [solve_problem(i_run, n_samples, rho, delta, n_iter, algo,
                          distribution, rel_tol, n_atoms, factor, deconv, noise_factor, noise_type, **algo_params)
            for i_run in range(*run_numbers)]


def compute_dt_point(n_runs, n_samples, rho, delta, n_iter, algo, distribution, rel_tol, temp_dir, epsilon, n_atoms,
                     factor, deconv, noise_factor, batch_size, noise_type, **algo_params
                     ) -> Tuple[float, float, float]:
    """
    Solve a problem like 'D * x = y' with the following parameters, n_runs times

    :param (int) n_runs: Number of run to do.
    :param (int or None) n_samples: Size of y, number of lines of D
    :param (float) rho: Sparsity, number of non_zeros coefficients in x divided by n_samples.
        Must be between 0 and 1
    :param (float) delta: Under-sampling factor, number of lines of D divided by its number of columns.
        Must be between 0 and 1
    :param (int) n_iter: Number max of iteration that the algorithm can do
    :param (Callable) algo: Solveur to use
    :param (str) distribution: Probability distribution to use for generating D coefficients.
        Must be 'gaussian' or '[1, 2]'
    :param (float) rel_tol: The algorithm stops when the iterations relative difference is lower than rel_tol
    :param (Path) temp_dir: Folder to use for temporary files
    :param (float) epsilon: Threshold for euclidian distance relative difference in DT recovery diagram.
        If less than 1% of problems are solved above this threshold twice in a row,
        the computation stops for the current value of delta. If 0, compute the entire diagram.
    :param (int or None) n_atoms: Size of x, number of columns of D. If specified, n_samples must be None.
    :param (int or None) factor: The number of iterations of the algorithm will be factor * sparsity
        Used if n_iter is None.
    TODO :return: Mean of the relative euclidian distances between `x real` and `x predicted by the algorithm`
    """
    # Check if the run is not already done
    if temp_dir is None:
        out_file = None
    else:
        if (temp_dir / get_filename(rho, delta, 1)).is_file():
            out_file = temp_dir / get_filename(rho, delta, 1)
        else:
            out_file = temp_dir / get_filename(rho, delta, 0)
    if out_file is not None and out_file.is_file():
        logger.debug(f"(rho, delta) = {(rho, delta)} is already computed")
        res = np.load(str(out_file))
    else:
        # futures = [solve_problem.remote(i_run, n_samples, rho, delta, n_iter, algo,
        #                                 distribution, rel_tol, n_atoms, factor, deconv, noise_factor)
        #            for i_run in range(n_runs)]
        # futures_completed = ray.get(futures)
        # n_results = len(futures_completed[0])

        futures = [solve_batch_problem.remote((batch_id * batch_size, min((batch_id + 1) * batch_size, n_runs)),  # noqa
                                              n_samples, rho, delta, n_iter, algo, distribution, rel_tol,  # noqa
                                              n_atoms, factor, deconv, noise_factor, noise_type, **algo_params)  # noqa
                   for batch_id in range(n_runs // batch_size + 1)]
        futures_batchs_completed = ray.get(futures)
        futures_completed = list(chain(*futures_batchs_completed))
        n_results = len(futures_completed[0])

        # Initialize arrays
        results = [
            dict(
                hamming_dist=np.ones(n_runs) * np.nan,
                supp_est_in_sup_ref=np.zeros(n_runs, dtype=bool),
                eucl_dist_rel=np.ones(n_runs) * np.nan,
                eucl_dist_rel_y=np.ones(n_runs) * np.nan,
                last_res=np.ones(n_runs) * np.nan,
                iterations=np.ones(n_runs) * np.nan,
                time=np.ones(n_runs) * np.nan,
                # support_history=[],
            ) for _ in range(n_results)]

        for i_run, results_list in enumerate(futures_completed):  # Get results for each run in parallel
            for idx, result_dict in enumerate(results_list):
                for key, value in result_dict.items():
                    # if key == "support_history":
                    #     results[idx][key].append(value)
                    # else:
                    results[idx][key][i_run] = value
        if out_file is not None:
            for idx, result in enumerate(results):  # Save files
                np.savez(out_file.with_name(get_filename(rho, delta, idx)), **result)

        res = results[-1]
    if epsilon is not None:
        return float(np.mean(res["eucl_dist_rel"] < epsilon)), float(np.mean(res["hamming_dist"] == 0)), float(
            np.max(res["eucl_dist_rel_y"]))
    else:
        return float(np.mean(res["eucl_dist_rel"])), float(np.mean(res["hamming_dist"] == 0)), float(
            np.max(res["eucl_dist_rel_y"]))


@ray.remote
def compute_dt_column(n_runs, n_samples, delta, n_steps_rho, n_iter, algo,
                      distribution, rel_tol, temp_dir, weak_curve, epsilon, n_atoms, factor, deconv, success,
                      noise_factor, batch_size, noise_type) -> None:
    """
    Solve a problem like 'D * x = y' with the following parameters, n_runs times for all possible values of rho

    :param (int) n_runs: Number of run to do.
    :param (int or None) n_samples: Size of y, number of lines of D
    :param (float) delta: Under-sampling factor, number of lines of D divided by its number of columns.
        Must be between 0 and 1
    :param n_steps_rho:
    :param (int) n_iter: Number max of iteration that the algorithm can do
    :param (Callable) algo: Solveur to use
    :param (str) distribution: Probability distribution to use for generating D coefficients.
        Must be 'gaussian' or '[1, 2]'
    :param (float) rel_tol: The algorithm stops when the iterations relative difference is lower than rel_tol
    :param (Path) temp_dir: Folder to use for temporary files
    :param (Callable[[float], float]) weak_curve: Function interpolating the asymptotic recovery weak curve.
        The algorithm will not solve problems with a rho value above this curve and the 1% recovery curve
    :param (float) epsilon: Threshold for euclidian distance relative difference in DT recovery diagram.
        If less than 1% of problems are solved above this threshold twice in a row,
        the computation stops for the current value of delta. If 0, compute the entire diagram.
    :param (int or None) n_atoms: Size of x, number of columns of D. If specified, n_samples must be None.
    :param (int or None) factor: The number of iterations of the algorithm will be factor * sparsity
        Used if n_iter is None.
    """
    if n_steps_rho == 0:
        n_samples_tmp = int(np.round(n_atoms * delta)) if n_samples is None else n_samples
        axis_range_rho = (np.arange(n_samples_tmp) + 1) / n_samples_tmp
    else:
        axis_range_rho = (np.arange(n_steps_rho) + 1) / n_steps_rho
    last_eucl_dist = 1
    logger.debug(f"Begin delta={delta}")
    last_non_recovery = -1
    last_hamming = -1
    max_y_rel_err = 1
    rho = None
    for idx, rho in enumerate(axis_range_rho):
        if last_eucl_dist < success and last_hamming < success and max_y_rel_err > 0.1 and epsilon is not None:  # rho > weak_curve(delta) and
            if idx == last_non_recovery + 1:
                break  # We stop only if the recovery fail twice in a row above the asymptotic curve
            else:
                last_non_recovery = idx
        last_eucl_dist, last_hamming, max_y_rel_err = compute_dt_point(
            n_runs, n_samples, rho, delta, n_iter, algo, distribution, rel_tol, temp_dir, epsilon, n_atoms, factor,
            deconv, noise_factor, batch_size, noise_type)
        logger.debug(f"delta={delta:2f}, rho={rho:2f}, rel_y={max_y_rel_err:2f}, hamming={last_hamming:2f}")
    logger.debug(f"End delta={delta}, rho={rho}")


def compute_dt_complete(n_samples, n_steps, n_runs, n_iter, algo, distribution, name, epsilon, *args,
                        rel_tol=-np.inf, resume=False, n_atoms=None, expe_name='', factor=None, deconv=False,
                        success=0.01, noise_factor=None, batch_size=200, noise_type=None,
                        remove_first=False, remove_last=False, keep_temp=False, force_resume=False,
                        delta_values=(), compile_=None, **kwargs) -> None:
    """
    Solve ||D * x - y||_2^2 with the provided algorithm for various configurations. Runs are parallelized.
    Results are saved in the 'data_result' folder

    :param (int or None) n_samples: Size of y, number of lines of D
    :param (int or Tuple[int, int]) n_steps: Discretization of rho and delta.
        If an integer is provided, the same discretization is used for both rho and delta.
        Else, the first element is the discretization for rho, and the second the one for delta
    :param (int) n_runs: Number of times each configuration is tested
    :param (int) n_iter: Number max of iteration that the algorithm can do
    :param (Callable) algo: Solver to use
    :param (str) distribution: Probability distribution to use for generating D coefficients.
        Must be 'gaussian' or '[1, 2]'
    :param (str) name: Name of the algorithm
    :param (float) rel_tol: The algorithm stops when the iterations relative difference is lower than rel_tol
    :param (bool) resume: If True, don't redo already done runs
    :param (float) epsilon: Threshold for euclidian distance relative difference in DT recovery diagram.
        If less than 1% of problems are solved above this threshold twice in a row,
        the computation stops for the current value of delta. If 0, compute the entire diagram.
    :param (int or None) n_atoms: Size of x, number of columns of D. If specified, n_samples must be None.
    :param (str) expe_name: Name of the current experiment
    :param (int or None) factor: The number of iterations of the algorithm will be factor * sparsity
        Used if n_iter is None.
    :param (Optional[bool]) compile_: If True, does only the compilation of the results.
        If False, does only the computations of DT
    """
    # Creating directory
    first_part = f'{name}' if factor is None else f'{name}x{factor}'
    prefix = f'{first_part}_{n_samples}_{n_steps}_{n_runs}_{n_iter}_{rel_tol}_{epsilon}_{n_atoms}_{expe_name}'

    if isinstance(n_steps, int):
        n_steps_rho, n_steps_delta = n_steps, n_steps
    elif isinstance(n_steps, tuple):
        n_steps_rho, n_steps_delta = n_steps
    else:
        raise TypeError("n_steps must be an int or a tuple")

    axis_range_delta = (np.arange(n_steps_delta) + 1) / n_steps_delta
    if remove_first:
        axis_range_delta = axis_range_delta[1:]
    if remove_last:
        axis_range_delta = axis_range_delta[:-1]
    if len(delta_values) != 0:
        axis_range_delta = np.concatenate((axis_range_delta, delta_values))
        axis_range_delta.sort()

    temp_dir = Path('temp') / prefix

    if compile_ is None or not compile_:
        temp_dir.mkdir(exist_ok=True, parents=True)

        logger.info(f'Algo {first_part}')

        if not resume:  # Clean old experiments if needed
            shutil.rmtree(temp_dir)
            temp_dir.mkdir(exist_ok=True)
        elif not force_resume:
            logger.info(f"Resuming...")
            if (DATA_DIR / f"{prefix}_0.npz").is_file():
                logger.info(f"Results for {first_part} have already been computed")
                return None

        weak_curve = None  # plot_theorical_phase_diagram_curve()
        futures = [compute_dt_column.remote(n_runs, n_samples, delta, n_steps_rho, n_iter, algo,  # noqa
                                            distribution, rel_tol, temp_dir, weak_curve, epsilon, n_atoms, factor,
                                            deconv,  # noqa
                                            success, noise_factor, batch_size, noise_type)  # noqa
                   for delta in axis_range_delta]
        ray.get(futures)

    if compile_ is None or compile_:
        logger.info("Compiling results")
        compile_results(n_steps_rho, axis_range_delta, n_runs, temp_dir, name, n_atoms, n_samples)
    if not keep_temp:
        shutil.rmtree(temp_dir)


def compile_results(n_steps_rho, axis_range_delta, n_runs, temp_dir, name, n_atoms, n_samples) -> None:
    """
    Compile the results stored in the temporary folder, and store them.

    :param (int) n_steps_rho: Discretization of rho
    :param (np.ndarray) axis_range_delta: Array of delta
    :param (int) n_runs: Number of times each configuration is tested
    :param (Path) temp_dir: Folder to use for temporary files
    :param (str) name: Name of the algorithm
    """
    # Creating directory
    DATA_DIR.mkdir(exist_ok=True)

    n_steps_delta = axis_range_delta.shape[0]
    idx = 0
    while len(list(temp_dir.glob(f'*_{idx}.npz'))) > 0:
        logger.debug(f"Compiling {len(list(temp_dir.glob(f'*_{idx}.npz')))} files for idx={idx}")
        results = {"axis_range_delta": axis_range_delta}
        if n_steps_rho == 0:
            n_samples_tmp = int(
                np.round(n_atoms * np.max(results["axis_range_delta"]))) if n_samples is None else n_samples
            results["axis_range_rho_per_sparcity"] = np.zeros((n_steps_delta, n_samples_tmp))
            for i_delta, delta in enumerate(results["axis_range_delta"]):
                n_samples_delta = int(np.round(n_atoms * delta)) if n_samples is None else n_samples
                results["axis_range_rho_per_sparcity"][i_delta] = np.pad(
                    (np.arange(n_samples_delta) + 1) / n_samples_delta, (0, n_samples_tmp - n_samples_delta))
            n_rho = n_samples_tmp
        else:
            results["axis_range_rho"] = (np.arange(n_steps_rho) + 1) / n_steps_rho
            n_rho = n_steps_rho

        # Initialize arrays
        results.update(dict(
            hamming_dist=np.ones((n_rho, n_steps_delta, n_runs)) * np.nan,
            eucl_dist_rel=np.ones((n_rho, n_steps_delta, n_runs)) * np.nan,
            eucl_dist_rel_y=np.ones((n_rho, n_steps_delta, n_runs)) * np.nan,
            supp_est_in_sup_ref=np.zeros((n_rho, n_steps_delta, n_runs), dtype=bool),
            last_res=np.ones((n_rho, n_steps_delta, n_runs)) * np.nan,
            iterations=np.ones((n_rho, n_steps_delta, n_runs)) * np.nan,
            time=np.ones((n_rho, n_steps_delta, n_runs)) * np.nan,
            # support_history={},
        ))

        # Retrieve results from parallel computation
        for i_delta, delta in enumerate(results["axis_range_delta"]):
            delta: float
            if n_steps_rho == 0:
                iterator = results["axis_range_rho_per_sparcity"][i_delta]
            else:
                iterator = results["axis_range_rho"]
            for i_rho, rho in enumerate(iterator):
                filepath = temp_dir / get_filename(rho, delta, idx)
                if filepath.is_file():
                    result_dict = np.load(str(filepath))
                    for key, value in result_dict.items():
                        results[key][i_rho, i_delta] = value

        # Store results
        out_file = f'{temp_dir.name}_{idx}.npz'
        print(out_file)
        np.savez(DATA_DIR / out_file, name=name, **results)
        idx += 1


def compute_one_point_from_dt(n_samples, n_runs, n_iter, algo, distribution, name, epsilon, *args, rel_tol=-np.inf,
                              n_atoms=None, expe_name='', factor=None, deconv=False, noise_factor=None, batch_size=200,
                              noise_type=None, rho=0, delta=0, seed=0, max_evals=1, lip_fact=0, **kwargs):
    if rho == 0 or delta == 0:
        raise ValueError("rho and delta must be specified")
    first_part = f'{name}' if factor is None else f'{name}x{factor}'
    logger.info(f"Computing point {n_samples}x{n_atoms} (rho={rho}, delta={delta}) for {first_part} and lf={lip_fact}")

    space = {'lip_fact': hp.loguniform('lip_fact', np.log(2e-3), np.log(2000))}
    fn = lambda space_in, temp_dir=None: -compute_dt_point(n_runs, n_samples, rho, delta, n_iter, algo, distribution,
                                                           rel_tol, temp_dir, epsilon, n_atoms, factor, deconv,
                                                           noise_factor, batch_size, noise_type,
                                                           lip_fact=space_in['lip_fact'])[1]

    out_dir = ONE_POINT_DIR / expe_name / f'{first_part}_{n_samples}x{n_atoms}_{lip_fact}'
    out_dir.mkdir(parents=True, exist_ok=True)

    if lip_fact == 0:
        trial = Trials()
        best = fmin(fn=fn, space=space, algo=tpe.suggest, max_evals=max_evals, rstate=np.random.default_rng(seed),
                    loss_threshold=-(1 - np.finfo(float).eps), trials=trial,
                    trials_save_file=out_dir / get_filename(rho, delta, None, ext='pkl'))
        print(fn(best, out_dir))
        logger.debug(f"Results: {best}")
    else:
        fn({'lip_fact': lip_fact}, out_dir)


def display_one_point_from_dt(n_samples, *args, n_atoms=None, expe_name='', rho=0, delta=0, lip_fact=0., **kwargs):
    df = pd.DataFrame(columns=['algorithm', 'best_lip_fact', 'success_rate_mean', 'success_rate_std', 'time_mean',
                               'time_std'])
    for temp_dir in (ONE_POINT_DIR / expe_name).glob(f"*_{n_samples}x{n_atoms}_{lip_fact}"):
        if (temp_dir / get_filename(rho, delta, 1)).is_file():
            out_file = temp_dir / get_filename(rho, delta, 1)
        else:
            out_file = temp_dir / get_filename(rho, delta, 0)
        if out_file.is_file():
            if lip_fact == 0:
                trials: Trials = pickle.load(open(temp_dir / get_filename(rho, delta, None, ext='pkl'), 'rb'))
                best_trial = trials.best_trial
                lip_fact_iter = best_trial['misc']['vals']['lip_fact'][0]
            else:
                lip_fact_iter = lip_fact
            first_part, *_ = temp_dir.name.split('_')
            name = first_part.split('x')[0]
            res = np.load(str(out_file))
            df.loc[len(df.index)] = [name, lip_fact_iter,
                                     float(np.mean(res["hamming_dist"] == 0)),
                                     float(np.std(res["hamming_dist"] == 0)), float(np.mean(res["time"])),
                                     float(np.std(res["time"]))]
    # print(df)
    return df


def fuse_one_point_from_dt(n_samples, *args, n_atoms=None, expe_name='', rho=0, delta=0, **kwargs):
    df = display_one_point_from_dt(n_samples, n_atoms=n_atoms, expe_name=expe_name, rho=rho, delta=delta,
                                   lip_fact=0.)
    temp_dirs = (ONE_POINT_DIR / expe_name).glob(f"*_{n_samples}x{n_atoms}_*")
    lip_factors = set(float(temp_dir.name.split('_')[-1]) for temp_dir in temp_dirs)
    for lip_fact in lip_factors:
        print(lip_fact)
        if lip_fact != 0.:
            df_lip = display_one_point_from_dt(n_samples, n_atoms=n_atoms, expe_name=expe_name, rho=rho, delta=delta,
                                               lip_fact=lip_fact)
            df = df.merge(df_lip, how='outer', on=['algorithm'], suffixes=(None, str(lip_fact)))
    df.round(4).to_csv(ONE_POINT_DIR / expe_name / f'one_point_{n_samples}x{n_atoms}.csv', index=False)


@click.command(context_settings={'show_default': True, 'help_option_names': ['-h', '--help']})
@click.option('--n_samples', '-ns', default=None, type=int, help='Size of y, number of lines of D')
@click.option('--n_atoms', '-na', default=None, type=int,
              help='Size of x, number of columns of D. '
                   'If specified, n_samples must be None. Must be specified if n_samples is None.')
@click.option('--n_steps', '-s', default=(0, 1), nargs=2, help='Discretization of rho and delta')
@click.option('--n_runs', '-ru', default=1000, help='Number of times each configuration is tested')
@click.option('--n_iter', '-i', default=None, type=int, help='Number max of iteration that the algorithm can do')
@click.option('--distribution', '-d', default='unif',
              help="Probability distribution to use for generating x* coefficients. Must be 'gaussian' or 'unif'")
@click.option('--rel_tol', '-re', default=-np.inf,
              help='Algorithms will stop if the relative difference between '
                   'two following residuals are below this threshold')
@click.option('--normalize/--no-normalize', '-n/-nn', default=True,
              help='If True, normalize linear operators')
@click.option('--plot', '-pl', is_flag=True, help='If specified, plot results instead of running algorithms')
@click.option('--algos_type', '-at', multiple=True, default=[ALL],
              type=click.Choice([*ALGOS_TYPE, ALL], case_sensitive=False),
              help='Algorithms to run. If \'ALL\' is selected, run all algorithms.')
@click.option('--algos_filter', '-af', multiple=True,
              default=["SEAFAST-els", "SEAFAST-omp", "SEAFAST", "ELS", "OMP", "IHT",
                       "HTP", "IHT-omp", "HTP-omp", "IHT-els", "HTP-els", "OMPR"],
              type=click.Choice(list(get_algo_dict(*[True] * len(ALGOS_TYPE)).keys()) + [ALL], case_sensitive=False),
              help='Algorithms to run. If \'ALL\' is selected, run all algorithms.')
@click.option('--resume', '-r', multiple=True, default=[ALL],
              type=click.Choice(list(get_algo_dict(*[True] * len(ALGOS_TYPE)).keys()) + ["None", ALL],
                                case_sensitive=False),
              help='Algorithms to resume instead of running them again from scratch. '
                   'They must also be specified in algorithm filter. Use None compute everything from scratch '
                   'Only work with the parallel version of the algorithm')
@click.option('--factors', '-f', multiple=True, default=[256],
              help='Sparsity factors to use for the iteration\'s number of SEA and IHT')
@click.option('--epsilon', '-e', default=1e-4,
              help='Threshold for euclidian distance relative difference in DT recovery diagram. '
                   'If less than 1% of problems are solved under this threshold twice in a row, '
                   'the computation stops for the current value of delta. If 0, compute the entire diagram')
@click.option('--epsilon_x', '-e_x', default=1e-4,
              help='Threshold on the relative distance between x_approx and x_true '
                   'for the definition of a successful recovery')
@click.option('--expe-name', '-en', default='', help="Name of the experiment")
@click.option('--num-cpus', '-cpu', default=None, type=int, help="Limit the number of CPU if needed")
@click.option('--deconv', '-dcv', is_flag=True, help="If specified, solve deconvolution problem")
@click.option('--success', '-su', default=0.95, help="Minimal success rate to continue DT computation")
@click.option('--noise-factor', '-nf', default=None, type=float, help="Variance of the gaussian noise")
@click.option('--noise_type', '-nt', default=NoiseType.NOISE_LEVEL.name,
              type=click.Choice(dir(NoiseType)[:len(NoiseType)], case_sensitive=False), help="How compute the noise")
@click.option('--batch-size', '-bs', default=200, type=int,
              help="Size of a batch of problems to send for parallelization")
@click.option('--remove_first', '-rmf', is_flag=True, default=False,
              help="If True, doesn't compute the first column of the phase diagram")
@click.option('--remove_last', '-rml', is_flag=True, default=True,
              help="If True, doesn't compute the last column of the phase diagram")
@click.option('--keep_temp', '-kt', is_flag=True, default=True,
              help="If True, keep temporary fil instead of deleting them")
@click.option('--force_resume', '-fr', is_flag=True, default=True,
              help="Set to True for continuing computations made with the `keep_temp` flag")
@click.option('--delta_values', '-dv', multiple=True,
              default=[round(0.025*k, 4) for k in range(1, 17)] + [round(0.1*k, 4) for k in range(5, 10)],
              type=click.FloatRange(0, 1),
              help='Columns to add to the diagram based on its delta value (between 0 and 1)')
@click.option('--rho', '-rho', default=0., help="Rho value for DT with only one point")
@click.option('--delta', '-delta', default=0., help="Delta value for DT with only one point")
@click.option('--seed', '-sd', default=0,
              help="Seed value for the bayesian search of DT with only one point")
@click.option('--max_evals', '-me', default=100,
              help="Seed value for the bayesian search of DT with only one point")
@click.option('--lip_fact', '-lf', default=0.,
              help="Factor of the Lipschitz constant of the gradient of the loss function")
@click.option('--compile_/--no-compile_', '-cp/-ncp', default=None, type=bool,
              help="Factor of the Lipschitz constant of the gradient of the loss function")
def run_experiment(n_samples, n_steps, n_runs, n_iter, distribution, rel_tol, plot, resume, factors,
                   normalize, epsilon, n_atoms, epsilon_x, expe_name, algos_type, algos_filter, num_cpus, deconv,
                   success, noise_factor, noise_type, batch_size, remove_first, remove_last, keep_temp, force_resume,
                   delta_values, rho, delta, seed, max_evals, lip_fact, compile_
                   ) -> None:
    """
    Solve ||D * x - y||_2^2 with the provided algorithm for various configurations.
    Results are saved in the 'data_result' folder
    """
    logger.info(f"Parameters: \n{locals()}")
    if n_steps[0] >= 100000 or n_steps[1] >= 100000:
        raise ValueError('n_steps values can\'t be above 100000')
    if (n_atoms is None and n_samples is None) or (n_atoms is not None and n_samples is not None):
        raise ValueError("n_atoms or n_samples must be specified, the other must be None")
    if plot:
        if rho != 0:
            display_one_point_from_dt(n_samples, n_atoms=n_atoms, expe_name=expe_name, rho=rho, delta=delta,
                                      lip_fact=lip_fact)
            fuse_one_point_from_dt(n_samples, n_atoms=n_atoms, expe_name=expe_name, rho=rho, delta=delta,
                                   lip_fact=lip_fact)
        else:
            plot_results(n_samples=n_samples, n_steps=n_steps, n_runs=n_runs, n_iter=n_iter, success=success,
                         rel_tol=rel_tol, epsilon=epsilon, n_atoms=n_atoms, epsilon_x=epsilon_x, expe_name=expe_name)
            for nm in (False, ): #  True):
                plot_threshold_curves_comparison([expe_name], expe_name, epsilon_x, nm=nm)
            # for threshold_position in (-1, -2):
            # plot_paper_curves(expe_name, epsilon_x, factors, False, -1)
            plot_dt_paper(expe_name=expe_name, threshold=success, factors=factors)
            plot_dt_paper_zoom(expe_name=expe_name, threshold=success, factors=factors)
    else:
        # For multiprocessing
        if compile_ is None or not compile_:
            if os.getcwd().startswith('/baie'):  # On sms cluster
                logger.info("Running on sms")
                ray.init(_temp_dir="/scratch/enroot-mimoun.mohamed")
                logger.info("Ray initialized")
            else:  # On local or core5
                logger.info(f"Running on {socket.gethostname()}")
                ray.init(num_cpus=num_cpus)
        algos_selected = select_algo(algos_type, algos_filter, normalize=normalize, optimizer='cg')
        algo_to_resume = select_algo(algos_type, resume).keys()
        if rho != 0:
            compute_func = compute_one_point_from_dt
        else:
            compute_func = compute_dt_complete
        algo_to_run = algos_selected.keys()
        logger.info(f"Running {tuple(algo_to_run)}")
        for algo_name in algo_to_run:
            for factor in factors:
                compute_func(n_samples=n_samples, n_steps=n_steps, n_runs=n_runs, n_iter=n_iter,
                             algo=algos_selected[algo_name], distribution=distribution, name=algo_name,
                             rel_tol=rel_tol, resume=algo_name in algo_to_resume, epsilon=epsilon, n_atoms=n_atoms,
                             expe_name=expe_name, factor=factor, deconv=deconv, success=success,
                             noise_factor=noise_factor, batch_size=batch_size, noise_type=NoiseType[noise_type],
                             remove_first=remove_first, remove_last=remove_last, keep_temp=keep_temp,
                             force_resume=force_resume, delta_values=delta_values, rho=rho, delta=delta, seed=seed,
                             max_evals=max_evals, lip_fact=lip_fact, compile_=compile_)
        logger.info("End")


# Needed for click CLI
if __name__ == '__main__':
    run_experiment()
