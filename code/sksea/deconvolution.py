# -*- coding: utf-8 -*-
# Python imports
from typing import Tuple

# Module imports
import numpy as np
from numpy.random import RandomState
from scipy.signal import convolve2d

# Script imports
from sksea.utils import find_support, AbstractLinearOperator
from sksea.algorithms import iht, ista, sea


class ConvolutionOperator(AbstractLinearOperator):
    """
    Convolution operator able to be used on restricted supports
    Here, the 1D convolution is done by using the 2D convolution of scipy
    """

    def __init__(self, filter_conv, input_len, support=None, seed=0):
        """
        Linear Operator for convolutions

        :param (np.ndarray) filter_conv: Filter of the convolution
        :param (int) input_len: Size of the wanted input of the filter
        :param (np.ndarray or None) support: If provided, upscale the provided vectors to R^{input_len}
            and restrict the output vector of the adjoint operator to R^{support_size}
        """
        self._filter = filter_conv[None, :]
        # Mapping for computing the convolution only on the provided support
        self.support = np.ones(input_len, dtype=bool) if support is None else support
        if support is not None and support.shape[0] != input_len:
            raise AssertionError(f"The support size and input_len must have the same value,"
                                 f" {support.shape[0]} != {input_len}")
        self.upscaled_x = np.empty_like(self.support, dtype=float)
        self.seed = seed

        super().__init__(dtype=filter_conv.dtype, shape=[input_len, self.support.sum()])

    def _matvec(self, x) -> np.ndarray:
        """
        Compute self @ x

        :param (np.ndarray) x: Input vector of support size
        :return: Output vector of input_len (size of the whole space) size
        """
        # Upscale x to the full space size
        if x.ndim == 2:
            x = x.flatten()
        self.upscaled_x[self.support] = x.view()  # Tentative infructueuse d'optimisation de performances
        self.upscaled_x[~self.support] = 0

        # Use the upscaled version of x for all computations
        return convolve2d(self.upscaled_x[None, :], self._filter, 'same', 'wrap')[0]

    def _rmatvec(self, x) -> np.ndarray:
        """
        Compute self.H @ x

        :param (np.ndarray) x: Input vector of input_len (size of the whole space) size
        :return: Output vector of support size
        """
        out = convolve2d(x[None, :], self._filter[:, ::-1].conj(), 'same', 'wrap')[0]
        return out[self.support]  # Restrict the output of the adjoint in the support

    @property
    def filter(self) -> np.ndarray:
        """

        :return:
        """
        return np.copy(self._filter[0])

    def get_operator_on_support(self, s) -> 'ConvolutionOperator':
        """
        Return the operator truncated on the provided support

        :param (np.ndarray) s: Support
        """
        return ConvolutionOperator(self.filter, self.shape[0], s, self.seed)

    def get_normalized_operator(self) -> Tuple['ConvolutionOperator', np.ndarray]:
        """
        Return a normalized version of the operator using its kernel
        """
        norms = np.linalg.norm(self.filter)
        return ConvolutionOperator(self.filter / norms, self.shape[0], self.support, self.seed), np.ones(self.shape[0]) / norms


def gen_u(N, n, u_type=2, rand=RandomState(), max_size=None, min_u=1, max_u=2) -> np.ndarray:
    """
    Generate a signal

    :param (int) N: Size of the signal
    :param (int) n: Behaviour depends of u_type.
        2: Sparsity of the signal
        Else: Position of the dirac
    :param (int) u_type: Type of the signal.
        1: Dirac of size 1 in position n.
        2: n-sparse random signal from N(0,1).
        3: Dirac of random size from U(0.1,10) in position n.
    :param (RandomState) rand: Numpy random state
    :return: Signal of size N
    """
    u = np.zeros(N)  # par defaut des 0

    if max_size is None:
        max_size = N

    if u_type == 1:  # un dirac en n
        if 0 <= n < N:
            u[n] = 1
    elif u_type == 2:  # un signal n-sparse
        u = rand.normal(0, 1, N)
        x = rand.normal(0, 1, N)
        s = find_support(x, n)
        u = u * s
    elif u_type == 3:  # Dirac of random size in position n
        u[n] = rand.uniform(0.1, 10)
    elif u_type == 4:  # Uniform on [1, 2]U[-2, -1] with support space constrained by max_size
        s = rand.permutation(max_size)[:n]
        u[s] = (max_u - min_u) * rand.rand(n) + min_u
        u[s] *= (-1) ** rand.randint(0, 2, n)
    return u


def gen_filter(N, h_type, sigma=1) -> np.ndarray:
    """
    Generate a convolution filter

    :param (int) N: Size of the filter
    :param (int) h_type: Type of the filter.
        0: Random from N(0, 1).
        1: One everywhere.
        2: Dirac of size 1 in the middle.
        3: Gaussian filter of variance sigma.
        4: Ricker filter with sigma as parameter.
        Else: Zero filter.
    :param (float) sigma: Parameter for Gaussian and Ricker filters
    :return: Filter of size N
    """
    half_N = N // 2
    if h_type == 0:  # aleatoire
        h = np.random.normal(0, 1, N)
    elif h_type == 1:  # 1
        h = np.ones(N)
    elif h_type == 2:  # un dirac
        h = gen_u(N, half_N, 1)
    elif h_type == 3:  # gaussien
        t = np.arange(-half_N, half_N + 1, 1)
        h = np.exp(-(t / sigma) ** 2 / 2)
    elif h_type == 4:  # Ricker
        t = np.arange(-half_N, half_N + 1, 1)
        h = (1 - (t / sigma) ** 2) * np.exp(-(t / sigma) ** 2 / 2)
    else:
        h = np.zeros(N)  # defaut
    return h


if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt

    from sksea.algorithms import sea, iht as palm

    # palm vs algo avec x_bar
    x_len = 64
    h_len = 10
    p = 0.30  # pourcentage de données manquantes
    n_nonzero = int(np.ceil(p * x_len))
    sigma = 0.5
    n_iter = 1000

    # generation des donnees
    x_true = 10 * gen_u(x_len, n_nonzero, 2)
    # h = gen_filter(Nh,2)     #Pour debugger, on prend h = un dirac
    h = gen_filter(h_len, 4)  # Pour trouver des difficiles, on prend h = 1,
    # nh=6, avec Nx = 10
    h_op = ConvolutionOperator(filter_conv=h, input_len=x_len)
    y = h_op @ x_true + sigma * np.random.normal(0, 1, x_len)
    f = lambda x, linop: np.linalg.norm(linop @ x - y)
    grad_f = lambda x, linop: linop.H @ (linop @ x - y)
    print(h)

    # reconstruction
    x = {}
    res_norm = {}
    x['IHT'], res_norm['IHT'] = iht(linop=h_op, y=y, n_nonzero=n_nonzero, n_iter=n_iter, f=f, grad_f=grad_f)
    x['sea'], res_norm['sea'] = sea(h_op, y, n_nonzero, n_iter, return_best=True, f=f, grad_f=grad_f,
                                    optimize_sea="all")

    print("erreur IHT = ", np.linalg.norm(x['IHT'] - x_true))
    print("erreur sea = ", np.linalg.norm(x['sea'] - x_true))

    plt.figure()
    plt.plot(y, "r", label="y")
    plt.plot(h_op @ x_true, "g:", label="h*x_true")
    plt.plot(h_op @ x['IHT'], "b:o", label="h*x_iht")
    plt.plot(h_op @ x['sea'], "r:o", label="h*x_sea")
    plt.legend()
    plt.savefig('figures/deconvolution_signals.pdf')
    #######################

    plt.figure()
    plt.plot(x_true, "g-", label="x_true")
    plt.plot(x['IHT'], "b:o", label="x_iht")
    plt.plot(x['sea'], "r:o", label="x_sea")
    plt.legend()
    plt.savefig('figures/deconvolution_sparse_vectors.pdf')

    plt.figure()
    for k in res_norm:
        if res_norm[k] is None:
            continue
        plt.loglog(res_norm[k], label=k)
    plt.xlabel('Iterations')
    plt.ylabel('Norm of residue')
    plt.legend()
    plt.savefig('figures/deconvolution_res_norm.pdf')

    plt.show()
