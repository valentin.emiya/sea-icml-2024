# Python imports
import pickle
from collections import defaultdict
from copy import deepcopy
from itertools import chain
from pathlib import Path

# Module imports
from loguru import logger
import matplotlib.pyplot as plt
from matplotlib.markers import MarkerStyle
from matplotlib.transforms import Affine2D
import numpy as np
from plotly import graph_objects as go
from typing import Dict

# Script imports
from sksea.dataset_operator import DatasetOperator, RESULT_PATH
from sksea.deconvolution import ConvolutionOperator
from sksea.exp_phase_transition_diag import HM_CURVE_FOLDER
from sksea.utils import compute_metrcs_from_file, DATA_FILENAME, RESULT_FOLDER

# https://matplotlib.org/stable/gallery/text_labels_and_annotations/tex_demo.html
els_color = "#e41a1c"
omp_color = "#377eb8"
zero_color = "#4daf4a"
ompr_color = "#37b8a9"
base_linestyle = (0, (1, 2, 0, 0))  # (0, (0, 0, 1, 4)
sea_linestyle = "solid"
htp_linestyle = (0, (1, 1, 0, 0))
iht_linestyle = (0, (3, 3, 0, 0))
alpha_dcv_iter = 0.6
algos_base = {
    "IHT": {"disp_name": "IHT",
            'plot': {"label": r"IHT", "linestyle": None},
            'marker': {"marker": r"$\unboldmath\bowtie$", "transform": Affine2D().rotate_deg(90), },
            'scatter': {"color": "#AB63FA"},
            'marker_label': r"IHT",
            'plot_dcv_iter': {},
            },
    "HTP": {"disp_name": "HTP",
            'plot': {"label": r"HTP", "linestyle": None},
            'marker': {"marker": "d", },
            'scatter': {"color": "#F39221"},
            'marker_label': r"HTP",
            'plot_dcv_iter': {},
            },
    "HTP-omp": {"disp_name": "HTP-omp",
                'plot': {"label": r"HTP$_{{OMP}}$", "linestyle": None}},
    "IHT-omp": {"disp_name": "IHT-omp",
                'plot': {"label": r"IHT$_{{OMP}}$", "linestyle": None}},
    "OMP": {"disp_name": "OMP",
            'plot': {"label": r"OMP", "linestyle": None}},
    "OMPR": {"disp_name": "OMPR",
             'plot': {"label": r"OMPR", "linestyle": None}},
    "ELS": {"disp_name": "ELS",
            'plot': {"label": r"ELS", "linestyle": None},
            'marker': {"marker": "d", "transform": Affine2D().rotate_deg(90), },
            'scatter': {"color": "#005FFA"},
            'marker_label': r"ELS, OMP, OMPR, IHT$_{{OMP}}$, HTP$_{{OMP}}$, IHT$_{{ELS}}$, HTP$_{{ELS}}$",
            'marker_label_light': r"ELS, OMP, OMPR",
            'plot_dcv_iter': {}, },
    "IHT-els": {"disp_name": "IHT-els",
                'plot': {"label": r"IHT$_{{ELS}}$", "linestyle": None}},
    "HTP-els": {"disp_name": "HTP-els",
                'plot': {"label": r"HTP$_{{ELS}}$", "linestyle": None}},
    "SEA-0": {"disp_name": "SEA-0",
              'plot': {"label": r"SEA$_0$", "linestyle": None},
              'marker': {"marker": "s", },
              'scatter': {"color": "#e41a1c"},
              'marker_label': r"SEA$_0$, SEA$_{{OMP}}$, SEA$_{{ELS}}$",
              'marker_noisy_label': r"SEA$_0$",
              'marker_label_light': r"SEA$_0$, SEA$_{{OMP}}$, SEA$_{{ELS}}$",
              'plot_dcv_iter': {"alpha": alpha_dcv_iter},
              'plot_dcv_iter_best': {"color": "darkred"}},
    "SEA-els": {"disp_name": "SEA-els",
                'plot': {"label": r"SEA$_{{ELS}}$", "linestyle": None},
                'marker_noisy_label': r"SEA$_{{OMP}}$, SEA$_{{ELS}}$",
                'scatter': {"color": "black"},
                'marker': {"marker": "s", "transform": Affine2D().rotate_deg(45)},
                'plot_dcv_iter': {"alpha": alpha_dcv_iter},
                'plot_dcv_iter_best': {"color": "grey"}},
    "SEA-omp": {"disp_name": "SEA-omp",
                'plot': {"label": r"SEA$_{{OMP}}$", "linestyle": None},
                'plot_dcv_iter': {"color": "#19D3F3", "alpha": alpha_dcv_iter},
                'plot_dcv_iter_best': {"color": "indigo"}},
    "NIHT": {"disp_name": "NIHT",
             'plot': {"label": r"NIHT", "linestyle": None}},
    "NIHT-omp": {"disp_name": "NIHT-omp",
             'plot': {"label": r"NIHT$_{{OMP}}$", "linestyle": None}},
    "NIHT-els": {"disp_name": "NIHT-els",
             'plot': {"label": r"NIHT$_{{ELS}}$", "linestyle": None}},
    "remove": {"disp_name": "remove",
               'plot': {"label": r"remove", "linestyle": None}},
    "SOTA": {"disp_name": "SOTA", 'plot': {"label": r"SOTA", "linestyle": None}},
    "x": {"disp_name": "x",
          'plot': {"label": r"x", "linestyle": None},
          'marker': {"marker": "o", },
          'scatter': {"color": "#4daf4a"},
          'marker_label': r"$x^*$",
          'marker_label_light': r"$x^*$"
          },
    "y": {"disp_name": "y",
          'plot': {"label": r"$y$", "linestyle": None}, }
}

legend_order = ["remove", "SOTA", "x", "y",
                "SEA-els", "SEA-omp", "SEA-0", "IHT-els", "IHT-omp", "IHT", "HTP-els", "HTP-omp", "HTP", "ELS", "OMPR",
                "OMP", "NIHT-els", "NIHT-omp", "NIHT"]
ZOOM_X_LIM = (383, 465)
ZOOM_Y_ABS = 3.15


def get_legend_order(label, markers=False, algo_dict=algos_base):
    for idx, algo in enumerate(legend_order):
        if algo in algo_dict:
            if label == algo_dict[algo]["plot"]["label"] or markers and (
                    label == algo_dict[algo].get("marker_label_light")
                    or label == algo_dict[algo].get("marker_label")
                    or label == algo_dict[algo].get("marker_noisy_label")):
                return idx


def algos_from_factor(factor):
    map = {
        f"IHTx{factor}": "IHT",
        f"IHT-ompx{factor}": "IHT-omp",
        f"IHT-elsx{factor}": "IHT-els",
        f"HTPFASTx{factor}_BEST": "HTP",
        f"HTPFAST-ompx{factor}_BEST": "HTP-omp",
        f"HTPFAST-elsx{factor}_BEST": "HTP-els",
        f"OMPx{factor}": "OMP",
        f"OMPRx{factor}": "OMPR",
        f"ELSx{factor}": "ELS",
        f"SEAFASTx{factor}_BEST": "SEA-0",
        f"SEAFAST-ompx{factor}_BEST": "SEA-omp",
        f"SEAFAST-elsx{factor}_BEST": "SEA-els",

    }
    return {algo_surname: deepcopy(algos_base[algo_name]) for algo_surname, algo_name in map.items()}


def algos_from_factor_ml(factor):
    map = {
        f"IHTx{factor}": "IHT",
        f"IHT-ompx{factor}": "IHT-omp",
        f"IHT-elsx{factor}": "IHT-els",
        f"HTPx{factor}": "HTP",
        f"HTP-ompx{factor}": "HTP-omp",
        f"HTP-elsx{factor}": "HTP-els",
        f"OMP": "OMP",
        f"OMPR": "OMPR",
        f"ELS": "ELS",
        f"SEAFASTx{factor}": "SEA-0",
        f"SEAFAST-ompx{factor}": "SEA-omp",
        f"SEAFAST-elsx{factor}": "SEA-els",

    }
    return {algo_surname: deepcopy(algos_base[algo_name]) for algo_surname, algo_name in map.items()}


def algos_dcv():
    map = {
        f"IHT": "IHT",
        f"IHT-omp": "IHT-omp",
        f"IHT-els": "IHT-els",
        f"HTPFAST": "HTP",
        f"HTPFAST-omp": "HTP-omp",
        f"HTPFAST-omp_fast": "HTP-omp",
        f"HTPFAST-els": "HTP-els",
        f"HTPFAST-els_fast": "HTP-els",
        f"OMPFAST": "OMP",
        f"OMPRFAST": "OMPR",
        f"ELSFAST": "ELS",
        f"SEAFAST": "SEA-0",
        f"SEAFAST-omp": "SEA-omp",
        f"SEAFAST-omp_fast": "SEA-omp",
        f"SEAFAST-els": "SEA-els",
        f"SEAFAST-els_fast": "SEA-els",

    }
    return {algo_surname: deepcopy(algos_base[algo_name]) for algo_surname, algo_name in map.items()}

def algos_dcv_step_size():
    map = {
        f"IHT": "IHT",
        f"IHT-omp": "IHT-omp",
        f"IHT-omp_fast": "IHT-omp",
        f"IHT-els": "IHT-els",
        f"IHT-els_fast": "IHT-els",
        f"HTPFAST": "HTP",
        f"HTPFAST-omp": "HTP-omp",
        f"HTPFAST-omp_fast": "HTP-omp",
        f"HTPFAST-els": "HTP-els",
        f"HTPFAST-els_fast": "HTP-els",
        f"NIHT": "NIHT",
        f"NIHT-omp_fast": "NIHT-omp",
        f"NIHT-els_fast": "NIHT-els",
        f"SEAFAST": "SEA-0",
        f"SEAFAST-omp": "SEA-omp",
        f"SEAFAST-omp_fast": "SEA-omp",
        f"SEAFAST-els": "SEA-els",
        f"SEAFAST-els_fast": "SEA-els",

    }
    return {algo_surname: deepcopy(algos_base[algo_name]) for algo_surname, algo_name in map.items()}


def algos_dcv_signal():
    map = {
        f"IHT": "IHT",
        f"IHT-omp": "IHT-omp",
        f"IHT-els": "IHT-els",
        f"HTPFAST": "HTP",
        f"HTPFAST-omp": "HTP-omp",
        f"HTPFAST-els": "HTP-els",
        f"OMPFAST": "OMP",
        f"OMPRFAST": "OMPR",
        f"ELSFAST": "ELS",
        f"SEAFAST": "SEA-0",
        f"SEAFAST-omp": "SEA-omp",
        f"SEAFAST-els": "SEA-els",
        "x": "x",
    }
    return {algo_surname: deepcopy(algos_base[algo_name]) for algo_surname, algo_name in map.items()}


def get_best_hist(loss_array):
    best_loss = np.empty_like(loss_array)
    best = np.inf
    for current_idx, current in enumerate(loss_array):
        best = min(best, current)
        best_loss[current_idx] = best
    return best_loss


# Matplotlib
# Plot https://randalolson.com/2014/06/28/how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/
plt.rcParams.update(
    {
        'axes.edgecolor': '#cccccc',
        'axes.linewidth': 1,
        'axes.spines.top': False,  # Remove the plot frame lines
        'axes.spines.right': False,  # Remove the plot frame lines
        'font.size': 25,
        'figure.dpi': 600,
        'figure.figsize': (10, 6),
        'legend.frameon': False,
        'legend.borderaxespad': 0,
        'legend.borderpad': 0,
        'legend.columnspacing': 1,
        'legend.labelspacing': 0.5,
        'legend.handlelength': 1.25,
        'legend.handletextpad': 0.7,
        'lines.linewidth': 3.2,
        'lines.markersize': 25,
        'mathtext.fontset': 'cm',
        'text.usetex': True,
        'text.latex.preamble': r'\usepackage[cm]{sfmath}',
        'xtick.bottom': False,  # Remove the tick marks
        'ytick.left': False,  # Remove the tick marks

    }
)

# Add style
for algo_selected, algo_infos in algos_base.items():
    algo_infos["legend_order"] = legend_order.index(algo_selected)  # Add curve order
    if "OMPR" in algo_selected or ("niht" in algo_selected.lower() and "omp" not in algo_selected.lower()
                                   and "els" not in algo_selected.lower()):
        algo_infos["plot"]["color"] = ompr_color
    elif "OMP".lower() in algo_selected.lower():
        algo_infos["plot"]["color"] = omp_color
    elif "ELS".lower() in algo_selected.lower():
        algo_infos["plot"]["color"] = els_color
    else:
        algo_infos["plot"]["color"] = zero_color
    if "SEA" in algo_selected:
        algo_infos["plot"]["linestyle"] = sea_linestyle
    elif "HTP" in algo_selected:
        algo_infos["plot"]["linestyle"] = htp_linestyle
    elif "IHT" in algo_selected:
        algo_infos["plot"]["linestyle"] = iht_linestyle
    else:
        algo_infos["plot"]["linestyle"] = base_linestyle
        algo_infos["plot"]["linewidth"] = plt.rcParams['lines.linewidth'] * 2
    if algo_infos.get("marker") is not None:
        algo_infos["marker"]["fillstyle"] = "none"
    algo_infos["vlines"] = {"color": "#cccccc", "linestyle": "solid"}


def plot_dt_paper(expe_name="unifr1000", threshold=0.95, factors=(256,)):
    fig_dir = Path("figures")
    plot_dir = fig_dir / "icml"
    plot_dir.mkdir(exist_ok=True)

    curve_folder = HM_CURVE_FOLDER

    algos = algos_from_factor(factors[0])

    # Get curves
    path = list(fig_dir.glob(f'*_{expe_name}_0.0*'))[0]
    npy_files = {np_path.name.rsplit('_', 1)[0]: np_path
                 for np_path in Path(path / curve_folder).glob(f"*_{threshold}.npy")
                 if np_path.name.rsplit('_', 1)[0] in algos.keys()}

    # Add curve order
    curve_order = ["remove",
                   "SEAFAST-omp", "SEAFAST-els", "SEAFAST", "IHT-els", "IHT", "IHT-omp", "HTP", "HTP-els",
                   "HTP-omp", "OMPR", "ELS", "OMP", ]

    for algo, infos in algos.items():
        algo_name = algo.split(f"x{factors[0]}")[0]
        if algo_name in curve_order:
            infos["plot"]["zorder"] = 100 * curve_order.index(algo_name)

    fig = plt.figure()
    # Put data in the plot
    for algo, np_path in npy_files.items():
        curve = np.load(np_path)
        plt.plot(curve[:, 0], curve[:, 1], **algos[algo]["plot"])
        if "SEA" in algo and "omp" in algo:
            style = dict(**algos[algo]["plot"])
            style["linewidth"] = plt.rcParams['lines.linewidth'] * 2
            style["label"] = None
            nb_pts_bold = 7 if "noisy" in expe_name else 8
            plt.plot(curve[:nb_pts_bold, 0], curve[:nb_pts_bold, 1], **style)

    # Limit the range of the plot to only where the data is.
    plt.ylim(0.01, 0.36)
    plt.xlim(0, 0.9)

    plt.xlabel(r'$\zeta = m / n$', labelpad=6)
    plt.ylabel(r'$\rho = k / m$', labelpad=10)

    # Reorder legend
    handles, labels = plt.gca().get_legend_handles_labels()
    order = np.argsort([get_legend_order(label) for label in labels])
    ax = plt.gca()
    first_legend = ax.legend([handles[idx] for idx in order[:9]], [labels[idx] for idx in order[:9]], ncol=3,
                             loc='upper left', bbox_to_anchor=(0.02, 1))
    ax.add_artist(first_legend)  # Add the legend manually to the Axes.
    plt.legend([handles[idx] for idx in order[9:]], [labels[idx] for idx in order[9:]], ncol=1,
               loc='upper left', bbox_to_anchor=(0.02, 0.725))

    fig.tight_layout(pad=0, rect=(0.015, 0, 1, 0.995))
    plt.savefig(plot_dir / f"{expe_name}.svg")
    plt.close("all")


def plot_dt_paper_zoom(expe_name="unifr1000", threshold=0.95, factors=(256,), legend_order=legend_order):
    legend_order = legend_order.copy()
    old_linewidth = plt.rcParams['lines.linewidth']
    plt.rcParams['lines.linewidth'] *= 1.5
    old_font_size = plt.rcParams['font.size']
    plt.rcParams['font.size'] = 33
    # legend_order.insert(0, "ELS")

    fig_dir = Path("figures")
    plot_dir = fig_dir / "icml"
    plot_dir.mkdir(exist_ok=True)

    curve_folder = HM_CURVE_FOLDER

    algos = algos_from_factor(factors[0])

    # Get curves
    path = list(fig_dir.glob(f'*_{expe_name}_0.0*'))[0]
    npy_files = {np_path.name.rsplit('_', 1)[0]: np_path
                 for np_path in Path(path / curve_folder).glob(f"*_{threshold}.npy")
                 if np_path.name.rsplit('_', 1)[0] in algos.keys()
                 and np_path.name.rsplit('_', 1)[0] in
                 (f"OMPx{factors[0]}", f"SEAFAST-ompx{factors[0]}_BEST",
                  f"SEAFAST-elsx{factors[0]}_BEST", f"ELSx{factors[0]}")}

    # Add curve order
    curve_order = ["remove", "SEAFAST-omp", "SEAFAST-els", "ELS", "OMP", ]

    for algo, infos in algos.items():
        algo_name = algo.split(f"x{factors[0]}")[0]
        if algo_name in curve_order:
            infos["plot"]["zorder"] = 100 * curve_order.index(algo_name)

    fig = plt.figure(figsize=(7, 4.5))
    # Put data in the plot
    for algo, np_path in npy_files.items():
        curve = np.load(np_path)
        plt.plot(curve[:, 0], curve[:, 1], **algos[algo]["plot"])
        if "SEA" in algo and "omp" in algo:
            style = dict(**algos[algo]["plot"])
            style["linewidth"] = plt.rcParams['lines.linewidth'] * 2
            style["label"] = None
            nb_pts_bold = 7 if "noisy" in expe_name else 8
            plt.plot(curve[:nb_pts_bold, 0], curve[:nb_pts_bold, 1], **style)

    # Limit the range of the plot to only where the data is.
    plt.ylim(0.075, 0.22)
    plt.xlim(0.02, 0.4)

    plt.xlabel(r'$\zeta=m/n$', labelpad=6)
    plt.ylabel(r'$\rho=k/m$', labelpad=10)

    # Reorder legend
    handles, labels = plt.gca().get_legend_handles_labels()
    order = np.argsort([get_legend_order(label) for label in labels])
    plt.legend([handles[idx] for idx in order], [labels[idx] for idx in order], ncol=4,
               loc='lower right', bbox_to_anchor=(1.05, 1.05),
               **{'labelspacing': 0.2, 'handletextpad': 0.2, 'columnspacing': 0.5, 'borderpad': 0.2,
                  'handlelength': 0.58
                  })

    fig.tight_layout(pad=0, rect=(0, 0, 1, 1))
    plt.savefig(plot_dir / f"{expe_name}_zoom.svg")
    plt.rcParams['lines.linewidth'] = old_linewidth
    plt.rcParams['font.size'] = old_font_size
    legend_order: list
    legend_order.remove("ELS")
    plt.close("all")


def plot_dcv_paper(expe_name, spars_max, downscale=None):
    result_folder = RESULT_FOLDER / expe_name
    plot_dir = result_folder / "icml"
    plot_dir.mkdir(exist_ok=True)

    solution = np.load(result_folder / "solution.npy")[:, :spars_max, :]
    sparsity = np.arange(1, spars_max + 1)
    with np.load(result_folder / DATA_FILENAME, allow_pickle=True) as data:
        linop = ConvolutionOperator(data["h"], data["x_len"])

    algos = algos_dcv()
    npy_files = {np_path.stem: np_path
                 for np_path in result_folder.glob("*.npy")
                 if np_path.stem in algos.keys()}

    sota_linewidth = plt.rcParams['lines.linewidth'] * 1.5
    sota_font = 33
    plots = {
        "sup_dist": {
            "ylabel": r"dist$_{supp}$", "ylim": (0, 0.75),
            "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
            "rect": (0.015, 0, 1, 0.995)},
        "sup_dist_min": {
            "ylabel": r"dist$_{supp,k'}$", "ylim": (0, 0.75),
            "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
            "rect": (0.015, 0, 1, 0.995)},
        "sup_dist_top": {
            "ylabel": r"dist$_{supp,largest}$", "ylim": (0, 0.75),
            "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
            "rect": (0.015, 0, 1, 0.995)},
        "ws": {
            "ylabel": r"Wasserstein distance", "ylim": (0, 7.5e-4),
            "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
            "rect": (0.015, 0, 1, 0.995)},
        # "ws_bin": {
        #     "ylabel": r"Unnorm Support Wasserstein distance", "ylim": (None, ),
        #     "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
        #     "rect": (0.015, 0, 1, 0.995)},
        # "ws_bin_norm": {
        #     "ylabel": r"Support Wasserstein distance", "ylim": (None, ),
        #     "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
        #     "rect": (0.015, 0, 1, 0.995)},
        # "n_supports": {
        #     "ylabel": r"Number of supports explored", "ylim": (1, 4e3),
        #     "legend1": {"ncol": 1, "loc": "upper left", "bbox_to_anchor": (1.04, 1)},
        #     "rect": (0, 0, 0.8, 1)},
        # "n_supports_new": {
        #     "ylabel": r"Number of NEW supports explored", "ylim": (1, 4e3),
        #     "legend1": {"ncol": 1, "loc": "upper left", "bbox_to_anchor": (1.04, 1)},
        #     "rect": (0, 0, 0.8, 1)},
        # "n_supports_from_start": {
        #     "ylabel": r"Number of supports explored from start", "ylim": (1, 4e3),
        #     "legend1": {"ncol": 1, "loc": "upper left", "bbox_to_anchor": (1.04, 1)},
        #     "rect": (0, 0, 0.8, 1)},
        # "mse": {
        #     "ylabel": r"MSE", "ylim": (None,),
        #     "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
        #     "rect": (0.015, 0, 1, 0.995)},
        "f_mse_y": {
            "ylabel": r"Mean of $\ell_{2, rel\_loss}$", "ylim": (0, 0.125),
            "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
            "rect": (0.015, 0, 1, 0.995)},
        "sota": {
            "ylabel": r"Support distance", "ylim": (0, 0.66),
            "legend1": {
                "ncol": 4, "loc": "lower right", "bbox_to_anchor": (1.05, 1.05), "fontsize": sota_font,
                'labelspacing': 0.2, 'handletextpad': 0.2, 'columnspacing': 0.5, 'borderpad': 0.2, 'handlelength': 0.6
            },
            "rect": (0, 0, 1, 0.85)},  # (left, bottom, right, top)
    }
    algos_sup_dist = []
    # For each algo
    for algo, file in npy_files.items():
        metrics_file = file.parent / "temp_plot_data" / (file.stem + ".npz")

        # Load or compute metrics
        if metrics_file.is_file():
            logger.debug(f"Loading {metrics_file}")
            metrics = np.load(metrics_file)
        else:
            logger.debug(f"Computing {metrics_file}")
            compute_metrcs_from_file(file, spars_max, linop, solution, metrics_file)
            metrics = np.load(metrics_file)

        # Plot metrics
        for plot_name in plots.keys():
            if "sota" == plot_name or "supports" in plot_name and "IHT" in algo:
                continue
            plt.figure(plot_name)
            style = dict(**algos[algo]["plot"])
            if "OMPR" in algo and "sup_dist" in plot_name:
                plt.plot(0, -1, **style)
                style["linewidth"] *= 1.2
                style.pop("label")
            elif "OMP" in "algo" and "sup_dist" in plot_name:
                plt.plot(0, -1, **style)
                style["linestyle"] = (0, (0, 1, 1, 1))
                style.pop("label")
            if plot_name == "f_mse_y":
                metric = metrics[plot_name].reshape(*solution.shape[:-1]).mean(axis=0)
                if "OMPR" in algo:
                    plt.plot(0, -1, **style)
                    style["linewidth"] *= 1.2
                    style.pop("label")
            else:
                metric = metrics[plot_name].mean(axis=0)
            if downscale is not None:
                spa = sparsity[::downscale]
                keep = spa >= downscale - 1
                plt.plot(spa[keep], metric[::downscale][keep], **style)
            else:
                plt.plot(sparsity, metric, **style)

        # For plot of the introduction
        if "SEA" not in algo:
            algos_sup_dist.append(metrics["sup_dist"].mean(axis=0))
        else:
            old_font_size = plt.rcParams['font.size']
            plt.rcParams.update({'font.size': sota_font})
            plt.figure("sota", figsize=(7, 4.5))
            plt.plot(sparsity, metrics["sup_dist"].mean(axis=0), **algos[algo]["plot"],
                     linewidth=sota_linewidth * 1.75 if "omp" in algo else sota_linewidth,
                     zorder=0 if "omp" in algo else 10)
            plt.rcParams.update({'font.size': old_font_size})

    plt.figure("sota")
    plt.plot(sparsity, np.array(algos_sup_dist).min(axis=0), color="black", linewidth=sota_linewidth,
             label="SOTA", zorder=100)

    # Design
    for plot_name, infos in plots.items():
        fig = plt.figure(plot_name)
        # Reorder legend
        if infos.get("legend1") is not None:
            handles, labels = plt.gca().get_legend_handles_labels()
            order = np.argsort([get_legend_order(label) for label in labels])
            ax = plt.gca()
            first_legend = ax.legend([handles[idx] for idx in order[:9]], [labels[idx] for idx in order[:9]],
                                     **infos["legend1"]
                                     )
            ax.add_artist(first_legend)  # Add the legend manually to the Axes.
            plt.legend([handles[idx] for idx in order[9:]], [labels[idx] for idx in order[9:]], ncol=1,
                       loc='lower right', bbox_to_anchor=(0.975, 0.28))

        plt.ylim(*infos["ylim"])
        plt.xlim(1, 50)
        plt.xlabel(r'Sparsity')
        plt.ylabel(infos["ylabel"], loc="top" if plot_name == "sota" else None)
        if plot_name == "sota":
            plt.yticks((0, 0.5))

        fig.tight_layout(pad=0, rect=infos["rect"])
        plt.savefig(plot_dir / f"{plot_name}_{downscale}.svg")
        if "n_supports" in plot_name:
            plt.yscale("log")
            plt.savefig(plot_dir / f"{plot_name}_{downscale}_log.svg")
    plt.close("all")


def plot_dcv_n_supports(expe_name, spars_max):
    result_folder = RESULT_FOLDER / expe_name
    plot_dir = result_folder / "icml"
    plot_dir.mkdir(exist_ok=True)

    solution = np.load(result_folder / "solution.npy")[:, :spars_max, :]
    sparsity = np.arange(1, spars_max + 1)
    with np.load(result_folder / DATA_FILENAME, allow_pickle=True) as data:
        linop = ConvolutionOperator(data["h"], data["x_len"])

    algos = algos_dcv()
    npy_files = {np_path.stem: np_path
                 for np_path in result_folder.glob("*.npy")
                 if np_path.stem in algos.keys() and "IHT" not in np_path.stem}

    plots = ("n_supports_from_start", "n_supports_new",)
    global_plot_name = "multi_n_supports"
    fig, axs = plt.subplots(1, 2, sharey=True, sharex=True, figsize=(20, 7))

    # For each algo
    for algo, file in npy_files.items():
        metrics_file = file.parent / "temp_plot_data" / (file.stem + ".npz")

        # Load or compute metrics
        if metrics_file.is_file():
            logger.debug(f"Loading {metrics_file}")
            metrics = np.load(metrics_file)
        else:
            logger.debug(f"Computing {metrics_file}")
            compute_metrcs_from_file(file, spars_max, linop, solution, metrics_file)
            metrics = np.load(metrics_file)

        # Plot metrics
        for ax, plot_name in zip(axs, plots):
            style = dict(**algos[algo]["plot"])
            ax.plot(sparsity, metrics[plot_name].mean(axis=0), **style, zorder=100 if "OMPR" in algo else None)

    # Design
    handles, labels = plt.gca().get_legend_handles_labels()
    order = np.argsort([get_legend_order(label) for label in labels])
    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order], ncol=9, loc='upper center',
               bbox_to_anchor=(0.5, 1))

    plt.ylim(1, 4e3)
    plt.xlim(1, 50)
    axs[0].set(ylabel="Number of explored supports")
    axs[0].set(xlabel="Sparsity")
    axs[1].set(xlabel="Sparsity")

    fig.tight_layout(pad=1.5, rect=(-.03, -0.07, 1.015, 1.025))  # (left, bottom, right, top))
    plt.savefig(plot_dir / f"{global_plot_name}.svg")

    plt.yscale("log")
    for ax in axs:
        ax.grid(True, which="both", axis="y")
    plt.savefig(plot_dir / f"{global_plot_name}_log.svg")
    plt.close("all")


def plot_signal_paper(expe_name: str):
    old_rcParams = plt.rcParams.copy()
    plt.rcParams.update({
        'lines.linewidth': plt.rcParams['lines.linewidth'] * 0.75,
        'lines.markersize': 30,
    })
    fig_dir = Path(f"figures/exp_deconv/{expe_name}/icml")
    fig_dir.mkdir(exist_ok=True, parents=True)
    refs = {}
    solutions = {}
    with open(fig_dir.parent / "refs.pkl", "rb") as f:
        refs_in = pickle.load(f)
    with open(fig_dir.parent / "solutions.pkl", "rb") as f:
        solutions_in = pickle.load(f)
    for dict_in, dict_new in zip((refs_in, solutions_in), (refs, solutions)):
        for key, value in dict_in.items():
            dict_new[key.rsplit('_', 1)[0]] = value

    algos = algos_dcv_signal()
    curve_order = ["x", "SEAFAST-els", "SEAFAST", "IHT", "ELSFAST", "HTPFAST", ]
    for algo, infos in algos.items():
        if infos.get("marker") is not None:
            infos["scatter"].update({"zorder": 100 * curve_order.index(algo)})
            if algo == "x":
                infos["scatter"]["s"] = (plt.rcParams['lines.markersize'] * np.sqrt(2.4)) ** 2
            infos["vlines"]["zorder"] = 10 * curve_order.index(algo)
            infos["vlines"]["linewidth"] = plt.rcParams['lines.linewidth'] * 0.75

    # Plot
    fig = plt.figure(figsize=(20, 5))
    plt.axhline(color=plt.rcParams['axes.edgecolor'])
    plt.plot(refs["y"], color="black", label="$y$")

    for algo, infos in algos.items():
        if (infos.get("marker_label") is not None or
                infos.get("marker_noisy_label") is not None and "noisy" in expe_name):
            data = refs[algo] if algo == "x" else solutions[algo]
            add_spikes(data, infos["vlines"])
            add_markers(data, infos["scatter"], infos["marker"])
            if algo == "x":
                infos["scatter"]["s"] = plt.rcParams['lines.markersize'] ** 2
            add_markers([100], infos["scatter"], infos["marker"],
                        legend=infos["marker_noisy_label"]
                        if infos.get("marker_noisy_label") is not None and "noisy" in expe_name
                        else infos["marker_label"])
    rect = plt.Rectangle((ZOOM_X_LIM[0], -ZOOM_Y_ABS), ZOOM_X_LIM[1] - ZOOM_X_LIM[0], 2 * ZOOM_Y_ABS,
                         color="black", fill=False, linewidth=plt.rcParams['lines.linewidth'] * 1.2,
                         linestyle="dashed")
    plt.xlim(*ZOOM_X_LIM)
    yabs = ZOOM_Y_ABS
    plt.ylim(-yabs, yabs)
    ax = plt.gca()
    ax.spines["bottom"].set_visible(False)
    ax.spines['bottom'].set_position('center')
    ax.set_xticks([400])
    ax.xaxis.zorder = 10000
    ax.add_patch(rect)

    ax2 = ax.twiny()
    ax2.set_xlim(*ZOOM_X_LIM)
    ax2.plot(refs["y"], alpha=0)
    ax2.spines["bottom"].set_visible(False)
    ax2.spines['top'].set_position('center')
    ax2.set_xticks([450])
    ax2.tick_params(axis='x', labeltop=True)

    handles, labels = ax.get_legend_handles_labels()
    order = np.argsort([get_legend_order(label, markers=True) for label in labels])
    first_legend = ax.legend([handles[idx] for idx in order[:-1]], [labels[idx] for idx in order[:-1]], ncol=6,
                             loc='upper left', bbox_to_anchor=(0.45, 0.97))
    ax.add_artist(first_legend)  # Add the legend manually to the Axes.
    plt.legend([handles[idx] for idx in order[-1:]], [labels[idx] for idx in order[-1:]],
               loc='upper left', bbox_to_anchor=(0.45, 0.85))

    fig.tight_layout(pad=0, rect=(0, 0, 1, 1))  # (left, bottom, right, top))
    plt.savefig(fig_dir / "zoom_signal_complete.svg")
    plt.rcParams.update(old_rcParams)
    plt.close("all")


def plot_signal_paper_light(expe_name: str):
    old_linewidth = plt.rcParams['lines.linewidth']
    plt.rcParams['lines.linewidth'] *= 0.75
    fig_dir = Path(f"figures/exp_deconv/{expe_name}/icml")
    fig_dir.mkdir(exist_ok=True, parents=True)
    refs = {}
    solutions = {}
    with open(fig_dir.parent / "refs.pkl", "rb") as f:
        refs_in = pickle.load(f)
    with open(fig_dir.parent / "solutions.pkl", "rb") as f:
        solutions_in = pickle.load(f)
    for dict_in, dict_new in zip((refs_in, solutions_in), (refs, solutions)):
        for key, value in dict_in.items():
            dict_new[key.rsplit('_', 1)[0]] = value

    algos = algos_dcv()
    algos["x"] = deepcopy(algos_base["x"])
    curve_order = ["HTPFAST", "x", "SEAFAST", "IHT", "ELSFAST", ]
    for algo, infos in algos.items():
        if infos.get("marker_label_light") is not None:
            infos["scatter"].update({"zorder": 100 * curve_order.index(algo)})
            if algo == "x":
                infos["scatter"]["s"] = (plt.rcParams['lines.markersize'] * np.sqrt(2.4)) ** 2
            infos["vlines"]["zorder"] = 10 * curve_order.index(algo)
            infos["vlines"]["linewidth"] = plt.rcParams['lines.linewidth'] * 0.75

    # Plot
    fig = plt.figure(figsize=(10, 4))
    plt.axhline(color=plt.rcParams['axes.edgecolor'])
    plt.plot(refs["y"], color="black", label="$y$")

    for algo, infos in algos.items():
        if infos.get("marker_label_light") is not None:
            data = refs[algo] if algo == "x" else solutions[algo]
            add_spikes(data, infos["vlines"])
            add_markers(data, infos["scatter"], infos["marker"])
            if algo == "x":
                infos["scatter"]["s"] = plt.rcParams['lines.markersize'] ** 2
            add_markers([100], infos["scatter"], infos["marker"], legend=infos["marker_label_light"])

    plt.xlim(*ZOOM_X_LIM)
    yabs = ZOOM_Y_ABS
    plt.ylim(-yabs, yabs)
    ax = plt.gca()
    ax.spines["bottom"].set_visible(False)
    ax.spines['bottom'].set_position('center')
    ax.set_xticks([400])
    ax.xaxis.zorder = 10000

    ax2 = ax.twiny()
    ax2.set_xlim(*ZOOM_X_LIM)
    ax2.plot(refs["y"], alpha=0)
    ax2.spines["bottom"].set_visible(False)
    ax2.spines['top'].set_position('center')
    ax2.set_xticks([450])
    ax2.tick_params(axis='x', labeltop=True)

    handles, labels = ax.get_legend_handles_labels()
    order = np.argsort([get_legend_order(label, markers=True) for label in labels])
    first_legend = ax.legend([handles[idx] for idx in order], [labels[idx] for idx in order], ncol=5,
                             loc='lower left', bbox_to_anchor=(-.05, -.15), columnspacing=0.9,
                             )
    ax.add_artist(first_legend)  # Add the legend manually to the Axes.

    fig.tight_layout(pad=0, rect=(0, 0.15, 1, 1))  # (left, bottom, right, top))
    plt.savefig(fig_dir / "zoom_signal_light.svg")
    plt.rcParams['lines.linewidth'] = old_linewidth
    plt.close("all")


def plot_signal_paper_full(expe_name: str):
    old_rcParams = plt.rcParams.copy()
    plt.rcParams.update({
        'lines.linewidth': plt.rcParams['lines.linewidth'] * 0.75,
        'lines.markersize': 30,
    })
    fig_dir = Path(f"figures/exp_deconv/{expe_name}/icml")
    fig_dir.mkdir(exist_ok=True, parents=True)
    refs = {}
    solutions = {}
    with open(fig_dir.parent / "refs.pkl", "rb") as f:
        refs_in = pickle.load(f)
    with open(fig_dir.parent / "solutions.pkl", "rb") as f:
        solutions_in = pickle.load(f)
    for dict_in, dict_new in zip((refs_in, solutions_in), (refs, solutions)):
        for key, value in dict_in.items():
            dict_new[key.rsplit('_', 1)[0]] = value

    algos = algos_dcv_signal()
    curve_order = ["x", "SEAFAST-els", "SEAFAST", "IHT", "ELSFAST", "HTPFAST", ]
    for algo, infos in algos.items():
        if infos.get("marker") is not None:
            infos["scatter"].update({"zorder": 100 * curve_order.index(algo)})
            if algo == "x":
                infos["scatter"]["s"] = (plt.rcParams['lines.markersize'] * np.sqrt(2.4)) ** 2
            if algo == "IHT":
                infos["scatter"]["s"] = 1.1 * (plt.rcParams['lines.markersize']) ** 2
            infos["vlines"]["zorder"] = 10 * curve_order.index(algo)
            infos["vlines"]["linewidth"] = plt.rcParams['lines.linewidth'] * 0.75

    # Plot
    fig = plt.figure(figsize=(20, 8))
    plt.axhline(color=plt.rcParams['axes.edgecolor'])
    plt.plot(refs["y"], color="black", label="$y$")

    for algo, infos in algos.items():
        if (infos.get("marker_label") is not None or
                infos.get("marker_noisy_label") is not None and "noisy" in expe_name):
            data = refs[algo] if algo == "x" else solutions[algo]
            add_spikes(data, infos["vlines"])
            add_markers(data, infos["scatter"], infos["marker"])
            if algo == "x":
                infos["scatter"]["s"] = plt.rcParams['lines.markersize'] ** 2
            add_markers([100], infos["scatter"], infos["marker"],
                        legend=infos["marker_noisy_label"]
                        if infos.get("marker_noisy_label") is not None and "noisy" in expe_name
                        else infos["marker_label"])
    rect = plt.Rectangle((ZOOM_X_LIM[0], -ZOOM_Y_ABS), ZOOM_X_LIM[1] - ZOOM_X_LIM[0], 2 * ZOOM_Y_ABS,
                         color="black", fill=False, linewidth=plt.rcParams['lines.linewidth'] * 1.2,
                         linestyle="dashed")

    plt.xlim(-2, 500)
    yabs = 6
    plt.ylim(-yabs, yabs)
    ax = plt.gca()
    ax.add_patch(rect)
    ax.spines["bottom"].set_visible(False)
    ax.spines['bottom'].set_position('center')
    ax.set_xticks([100, 200, 300, 400])
    ax.xaxis.zorder = 10000
    handles, labels = plt.gca().get_legend_handles_labels()
    order = np.argsort([get_legend_order(label, markers=True) for label in labels])
    first_legend = ax.legend([handles[idx] for idx in order[:-1]], [labels[idx] for idx in order[:-1]], ncol=6,
                             loc='upper left', bbox_to_anchor=(0.01, 1)
                             )
    ax.add_artist(first_legend)  # Add the legend manually to the Axes.
    plt.legend([handles[idx] for idx in order[-1:]], [labels[idx] for idx in order[-1:]],
               loc='upper left', bbox_to_anchor=(0.01, 0.925))

    fig.tight_layout(pad=0, rect=(0, 0, 1, 1))  # (left, bottom, right, top))
    plt.savefig(fig_dir / "full_signal.svg")
    plt.rcParams.update(old_rcParams)
    plt.close("all")


def add_spikes(array, vlines_args: Dict):
    for x, y in enumerate(array):
        if y != 0:
            plt.vlines(x, 0, y, **vlines_args)


def add_markers(array, scatter_args: Dict, marker_args: Dict, legend=None):
    for x, y in enumerate(array):
        if y != 0:
            plt.scatter(x, y, marker=MarkerStyle(**marker_args), **scatter_args, label=legend)


def iterations_dcv(expe_name):
    old_rcParams = plt.rcParams.copy()
    plt.rcParams.update({
        'lines.linewidth': plt.rcParams['lines.linewidth'] * 0.75,
        'lines.markersize': 30,
        'legend.handlelength': 1.7,
    })
    fig_dir = Path(f"figures/exp_deconv/{expe_name}/icml")
    fig_dir.mkdir(exist_ok=True, parents=True)
    res_norms = {}
    with open(fig_dir.parent / "res_norms.pkl", "rb") as f:
        res_norms_in = pickle.load(f)
    for key, value in res_norms_in.items():
        res_norms[key.rsplit('_', 1)[0]] = value

    algos = algos_dcv_signal()
    plt.figure(figsize=(20, 8))
    curve_order = ["SEAFAST-els", "SEAFAST", "SEAFAST-omp", "IHT", "HTPFAST", "OMPFAST", "ELSFAST", ]
    if "noisy" not in expe_name:
        sea_omp_infos = algos.pop("SEAFAST-omp")
    for algo, infos in algos.items():
        if infos.get("plot_dcv_iter") is not None:
            if "SEAFAST-omp" in algo:
                infos["scatter"] = {"color": infos["plot_dcv_iter"]["color"]}
            if "SEAFAST-els" in algo and "noisy" not in expe_name:
                infos["plot"]["label"] += "," + sea_omp_infos["plot"]["label"]
            infos["plot_dcv_iter"].update({"label": fr'$x^t_{{{infos["plot"]["label"].replace("$", "")}}}$',
                                           "zorder": 100 * curve_order.index(algo), "color": infos["scatter"]["color"],
                                           "linewidth": plt.rcParams['lines.linewidth'] * 0.75})
            if "HTPFAST" in algo:
                infos["plot_dcv_iter"]["linewidth"] = plt.rcParams['lines.linewidth'] * 2.5
                infos["plot_dcv_iter"]["zorder"] = 10000
            if "ELS" in algo:
                infos["plot_dcv_iter"]["linewidth"] = plt.rcParams['lines.linewidth'] * 3.5
                infos["plot_dcv_iter"]["zorder"] = 10000
            if len(res_norms[algo]) == 1:
                add_markers([0] * 4 + [res_norms[algo]],
                            {"color": infos["scatter"]["color"], "s": 700, "zorder": 10000},
                            {"marker": "."})
                add_markers([100], {"color": infos["scatter"]["color"], "s": 700},
                            {"marker": "."}, legend=fr'$x^t_{{{infos["plot"]["label"].replace("$", "")}}}$')
            else:
                plt.plot(res_norms[algo], **infos["plot_dcv_iter"])
            if "SEAFAST" in algo:
                infos["plot_dcv_iter_best"].update(
                    {"label": fr'$x^{{t_{{BEST}}}}_{{{infos["plot"]["label"].replace("$", "")}}}$',
                     "zorder": 1000 + 100 * curve_order.index(algo),
                     "linewidth": plt.rcParams['lines.linewidth'] * 2})
                plt.plot(get_best_hist(res_norms[algo]), **infos["plot_dcv_iter_best"], linestyle=(0, (3, 3)))
    plt.ylim(-0.004, 1)
    plt.xlim(-2, 1000)
    plt.xlabel(r'Iterations')
    plt.ylabel(r'$\ell_{2, rel\_{loss}}$')
    plt.legend(ncols=9, loc="upper right", bbox_to_anchor=(1, 1))
    plt.tight_layout(pad=0)
    plt.savefig(fig_dir / "iter.svg")
    plt.rcParams.update(old_rcParams)
    plt.close("all")


def iterations_sup_dcv(expe_name):
    old_rcParams = plt.rcParams.copy()
    plt.rcParams.update({
        'lines.linewidth': plt.rcParams['lines.linewidth'] * 0.75,
        'lines.markersize': 30,
        'legend.handlelength': 1.7,
    })
    fig_dir = Path(f"figures/exp_deconv/{expe_name}/icml")
    fig_dir.mkdir(exist_ok=True, parents=True)
    refs = {}
    histories = {}
    with open(fig_dir.parent / "refs.pkl", "rb") as f:
        refs_in = pickle.load(f)
    with open(fig_dir.parent / "histories.pkl", "rb") as f:
        histories_in = pickle.load(f)
    for dict_in, dict_new in zip((refs_in, histories_in), (refs, histories)):
        for key, value in dict_in.items():
            dict_new[key.rsplit('_', 1)[0]] = value

    algos = algos_dcv_signal()
    fig = plt.figure(figsize=(20, 8))
    curve_order = ["SEAFAST-els", "SEAFAST", "SEAFAST-omp", "IHT", "HTPFAST", "ELSFAST", ]
    if "noisy" not in expe_name:
        sea_omp_infos = algos.pop("SEAFAST-omp")
    for algo, infos in algos.items():
        if infos.get("plot_dcv_iter") is not None and histories.get(algo) is not None:
            if "SEAFAST-omp" in algo:
                infos["scatter"] = {"color": infos["plot_dcv_iter"]["color"]}
            if "SEAFAST-els" in algo and "noisy" not in expe_name:
                infos["plot"]["label"] += "," + sea_omp_infos["plot"]["label"]
            infos["plot_dcv_iter"].update({"label": fr'$x^{{t(s)}}_{{{infos["plot"]["label"].replace("$", "")}}}$',
                                           "zorder": 100 * curve_order.index(algo), "color": infos["scatter"]["color"],
                                           "linewidth": plt.rcParams['lines.linewidth'] * 0.75})
            data = histories[algo].get_loss_by_explored_support() / np.linalg.norm(refs["y"])
            if "ELSFAST" in algo:
                infos["plot_dcv_iter"]["linewidth"] = plt.rcParams['lines.linewidth']
            elif "HTPFAST" in algo:
                infos["plot_dcv_iter"]["linewidth"] = plt.rcParams['lines.linewidth'] * 2.5
                infos["plot_dcv_iter"]["zorder"] = 10000
            if len(data) == 1:
                add_markers([0] * 4 + [data], {"color": infos["scatter"]["color"], "s": 700, "zorder": 10000},
                            {"marker": "."})
                add_markers([100], {"color": infos["scatter"]["color"], "s": 700},
                            {"marker": "."}, legend=fr'$x^t_{{{infos["plot"]["label"].replace("$", "")}}}$')
            else:
                plt.plot(data, **infos["plot_dcv_iter"])
            if "SEAFAST" in algo:
                infos["plot_dcv_iter_best"].update(
                    {"label": fr'$x^{{t_{{BEST}}}}_{{{infos["plot"]["label"].replace("$", "")}}}$',
                     "zorder": 1000 + 100 * curve_order.index(algo),
                     "linewidth": plt.rcParams['lines.linewidth'] * 2})
                plt.plot(get_best_hist(data), **infos["plot_dcv_iter_best"], linestyle=(0, (3, 3)))

    plt.ylim(-0.004, 1)
    plt.xlim(-2, 1000)
    plt.xlabel(r'Number $s$ of explored supports')
    plt.ylabel(r'$\ell_{2, rel\_{loss}}$')
    plt.legend(ncols=9, loc="upper right", bbox_to_anchor=(1, 1))
    plt.tight_layout(pad=0)
    plt.savefig(fig_dir / "hist.svg")
    plt.rcParams.update(old_rcParams)
    plt.close("all")


def plot_ml_paper(datasets=("cal_housing", "comp-activ-harder", "letter", "ijcnn1", "year", "slice"), factors=(256,)):
    global legend_order
    fig_dir = Path("results/training_tasks_plot_icml")
    fig_dir.mkdir(exist_ok=True, parents=True)
    datasets_sorted = list(datasets)
    datasets_sorted.sort(key=lambda elt: DatasetOperator.ORDER.index(elt))
    algos_raw = algos_from_factor_ml(factors[0])
    algos = {}
    # Add curve order
    curve_order = ["remove",
                   "OMPR", "ELS", "OMP", "SEAFAST-omp", "SEAFAST-els", "SEAFAST", "IHT-els", "IHT", "IHT-omp", "HTP",
                   "HTP-els", "HTP-omp", ]

    data_data = {
        "cal_housing": {"ylim": (199, 306),
                        "legend": {"ncol": 1, "loc": "upper right", "bbox_to_anchor": (1, 1)},
                        "labels": {
                            "ELS": ["HTP-els", "IHT-els", "OMPR"],
                            "OMP": ["HTP-omp", "IHT-omp", ],
                            "HTP": [],
                            "IHT": [],
                            "SEA-els": ["SEA-omp"],
                            "SEA-0": []
                        }
                        },
        "comp-activ-harder": {"ylim": (39, 141),
                              "legend": {"ncol": 1, "loc": "upper right", "bbox_to_anchor": (1, 1)},
                              "labels": {
                                  "IHT": [],
                                  "OMPR": [],
                                  "HTP": [],
                                  "OMP": ["HTP-omp", "IHT-omp", ],
                                  "ELS": ["SEA-0", "SEA-omp", "SEA-els", "IHT-els", "HTP-els", ],
                              }
                              },
        "letter": {"ylim": (9400, 10670),
                   "legend": {"ncol": 1, "loc": "upper right", "bbox_to_anchor": (1, 1)},
                   "labels": {
                       "ELS": ["SEA-els", "HTP-els", "IHT-els", ],
                       "OMP": ["HTP-omp", "IHT-omp", ],
                       "OMPR": [],
                       "HTP": [],
                       "IHT": [],
                       "SEA-omp": [],
                       "SEA-0": [],
                   }
                   },
        "ijcnn1": {"ylim": (4750, 8050),
                   "legend": {"ncol": 1, "loc": "upper left", "bbox_to_anchor": (0.35, 1)},
                   "labels": {
                       "ELS": ["HTP-els", "IHT-els", "OMPR"],
                       "OMP": ["HTP-omp", "IHT-omp", ],
                       "HTP": [],
                       "IHT": [],
                       "SEA-0": ["SEA-omp", "SEA-els"],
                   }
                   },
        "year": {"ylim": (220, 13400),
                 # "legend": {"ncol": 4, "loc": "upper left", "bbox_to_anchor": (0.5, 1)}},
                 "legend": {"ncol": 1, "loc": "upper right", "bbox_to_anchor": (1, 1)},
                 "labels": {
                     "ELS": ["HTP-els", "IHT-els", ],
                     "OMP": ["HTP-omp", "IHT-omp", ],
                     "OMPR": ["SEA-omp"],
                     "HTP": [],
                     "IHT": [],
                     "SEA-0": [],
                     "SEA-els": [],
                 }
                 },
        "slice": {"ylim": (228, 1020),
                  # "legend": {"ncol": 4, "loc": "upper left", "bbox_to_anchor": (0.5, 1)}},
                  "legend": {"ncol": 1, "loc": "upper right", "bbox_to_anchor": (1, 1)},
                  "labels": {
                      "ELS": ["SEA-els", "HTP-els", "IHT-els", ],
                      "OMP": ["HTP-omp", "IHT-omp", ],
                      "OMPR": [],
                      "HTP": [],
                      "IHT": [],
                      "SEA-omp": [],
                      "SEA-0": [],
                  }
                  },
    }

    for algo, infos in algos_raw.items():
        algo_name = algo.replace(f"x{factors[0]}", "").replace("FAST", "")
        if algo_name == "SEA":
            algo_name = "SEA-0"
        if algo_name in curve_order:
            infos["plot"]["zorder"] = 100 * curve_order.index(algo_name)  # Add curve order
        infos["old_name"] = algo
        algos[algo_name] = infos

    algos_backup = deepcopy(algos)
    for data_name in datasets_sorted:
        legend_backup = legend_order.copy()
        if data_name in ("cal_housing",):
            legend_order.insert(legend_order.index("HTP"), legend_order.pop(legend_order.index("SEA-els")))
        if data_name in ("ijcnn1",):
            legend_order.insert(legend_order.index("HTP"), legend_order.pop(legend_order.index("SEA-0")))
        legend_order.append(legend_order.pop(legend_order.index("ELS")))
        algos = deepcopy(algos_backup)
        logger.info(f"Plotting for {data_name}")
        dataset = DatasetOperator(data_name)
        plt.figure(figsize=(20, 8))
        for algo, infos in algos.items():
            if data_data.get(data_name, {}).get("labels").get(algo) is None:
                continue
            res_by_k = []
            for k in range(1, dataset.k_max + 1):
                if (RESULT_PATH / f"{infos['old_name']}_1_{dataset.name}_{k}.npz").is_file():
                    path = RESULT_PATH / f"{infos['old_name']}_1_{dataset.name}_{k}.npz"
                else:
                    path = RESULT_PATH / f"{infos['old_name']}_0_{dataset.name}_{k}.npz"
                with np.load(path) as file:
                    res_by_k.append(dataset.f(file["x"]))
            if algo in data_data[data_name]["labels"]:
                infos["plot"]["label"] = ", ".join([algos[algo_key]["plot"]["label"] for algo_key in
                                                    chain((algo,), data_data[data_name]["labels"][algo])])
            label = infos["plot"].get("label")
            if "OMP" == algo:
                plt.plot(0, 1, **infos["plot"])
                infos["plot"]["linestyle"] = (0, (0, 2, 1, 0))
                infos["plot"].pop("label")
            if "ELS" in algo and data_name != "cal_housing":
                plt.plot(0, 1, **infos["plot"])
                infos["plot"]["linestyle"] = (0, (0, 2, 1, 0))
                infos["plot"].pop("label")
            plt.plot(range(1, dataset.k_max + 1), res_by_k, **infos["plot"])
            infos["plot"]["label"] = label
        handles, labels = plt.gca().get_legend_handles_labels()
        order = np.argsort([get_legend_order(label, markers=True, algo_dict=algos) for label in labels])
        plt.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
                   **data_data.get(data_name, {}).get("legend", {}))
        plt.ylim(*data_data.get(data_name, {}).get("ylim", (None, None)))
        plt.xlim(1, dataset.k_max)
        plt.xlabel("Sparsity")
        plt.ylabel(r"$\ell_{2}$\_loss")
        plt.tight_layout(pad=0)
        plt.savefig(fig_dir / f"{data_name}.svg")
        legend_order = legend_backup.copy()
    plt.close("all")


def plot_dcv_step_size_paper(expe_name, spars_max, downscale=None):
    result_folder = RESULT_FOLDER / expe_name
    plot_dir = result_folder / "icml"
    plot_dir.mkdir(exist_ok=True)

    solution = np.load(result_folder / "solution.npy")[:, :spars_max, :]
    sparsity = np.arange(1, spars_max + 1)
    with np.load(result_folder / DATA_FILENAME, allow_pickle=True) as data:
        linop = ConvolutionOperator(data["h"], data["x_len"])

    algos = algos_dcv_step_size()
    npy_files = {np_path.stem: np_path
                 for np_path in result_folder.glob("*.npy")
                 if "solution" not in np_path.stem}

    plots = {
        "sup_dist": {
            "ylabel": r"dist$_{supp}$", "ylim": (0, 0.75),
            "legend1": {"ncol": 3, "loc": "lower right", "bbox_to_anchor": (1, 0.01)},
            "rect": (0.015, 0, 1, 0.995)},
    }
    sup_dist_stacked = defaultdict(list)
    sup_dist_algo = {}
    fig = plt.figure("sup_dist")

    # For each algo
    for algo, file in npy_files.items():
        metrics_file = file.parent / "temp_plot_data" / (file.stem + ".npz")

        # Load or compute metrics
        if metrics_file.is_file():
            logger.debug(f"Loading {metrics_file}")
            metrics = np.load(metrics_file)
        else:
            logger.debug(f"Computing {metrics_file}")
            compute_metrcs_from_file(file, spars_max, linop, solution, metrics_file)
            metrics = np.load(metrics_file)
        split = algo.rsplit("-", 1)
        if len(split) == 2 and split[1].replace(".", "").isdigit():
            sup_dist_stacked[split[0]].append(metrics["sup_dist"])
        else:
            style = dict(**algos[algo]["plot"])
            plt.plot(sparsity, metrics["sup_dist"].mean(axis=0), **style)

    fill_style = {
        "IHT": {"color": "#636EFA", "alpha": 0.3},
        "HTPFAST": {"color": "#EF553B", "alpha": 0.3},
    }
    for algo, metric_stacked in sup_dist_stacked.items():
        array_stacked = np.array(metric_stacked)
        minn = array_stacked.mean(axis=1).min(axis=0)
        maxx = array_stacked.mean(axis=1).max(axis=0)
        style = dict(**algos[algo]["plot"])
        if "omp" in algo or "els" in algo:
            # sup_dist_algo[algo] = minn
            plt.plot(sparsity, minn, **style)
        else:
            # sup_dist_algo[algo + "__min"] = minn
            # sup_dist_algo[algo + "__max"] = maxx
            plt.fill_between(sparsity, minn, maxx, label=style["label"], **fill_style.get(algo, {}))

    # Plot metrics
    infos = plots["sup_dist"]
    # fig_plt = go.Figure()
    for algo, metric in sup_dist_algo.items():
        if "max" in algo:
            style.pop("label")
        # fig_plt.add_trace(
        #     go.Scatter(x=sparsity, y=metric, name=algo)
        # )

    # Reorder legend
    if infos.get("legend1") is not None:
        handles, labels = plt.gca().get_legend_handles_labels()
        order = np.argsort([get_legend_order(label) for label in labels])
        ax = plt.gca()
        first_legend = ax.legend([handles[idx] for idx in order[:9]], [labels[idx] for idx in order[:9]],
                                 **infos["legend1"]
                                 )
        ax.add_artist(first_legend)  # Add the legend manually to the Axes.
        plt.legend([handles[idx] for idx in order[9:]], [labels[idx] for idx in order[9:]], ncol=1,
                   loc='lower right', bbox_to_anchor=(1.01, 0.28))

    plt.ylim(*infos["ylim"])
    plt.xlim(1, 50)
    plt.xlabel(r'Sparsity')
    plt.ylabel(infos["ylabel"])
    fig.tight_layout(pad=0, rect=infos["rect"])
    plt.savefig(plot_dir / f"sup_dist_{downscale}_ss.svg")
    # fig_plt.write_html(plot_dir / f"sup_dist_{downscale}_ss.html")


    #     # Plot metrics
    #     for plot_name in plots.keys():
    #         plt.figure(plot_name)
    #         style = dict(**algos[algo]["plot"])
    #         if algo == "OMP" and plot_name == "sup_dist":
    #             plt.plot(0, -1, **style)
    #             style["linestyle"] = (0, (0, 1, 1, 1))
    #             style.pop("label")
    #         if "OMPR" in algo and plot_name == "sup_dist":
    #             plt.plot(0, -1, **style)
    #             style["linewidth"] *= 1.2
    #             style.pop("label")
    #         if plot_name == "f_mse_y":
    #             metric = metrics[plot_name].reshape(*solution.shape[:-1]).mean(axis=0)
    #             if "OMPR" in algo:
    #                 plt.plot(0, -1, **style)
    #                 style["linewidth"] *= 1.2
    #                 style.pop("label")
    #         else:
    #             metric = metrics[plot_name].mean(axis=0)
    #         if downscale is not None:
    #             plt.plot(sparsity[::downscale], metric[::downscale], **style)
    #         else:
    #             plt.plot(sparsity, metric, **style)
    #
    #
    # # Design
    # for plot_name, infos in plots.items():
    #     fig = plt.figure(plot_name)
    #     # Reorder legend
    #     if infos.get("legend1") is not None:
    #         handles, labels = plt.gca().get_legend_handles_labels()
    #         order = np.argsort([get_legend_order(label) for label in labels])
    #         ax = plt.gca()
    #         first_legend = ax.legend([handles[idx] for idx in order[:9]], [labels[idx] for idx in order[:9]],
    #                                  **infos["legend1"]
    #                                  )
    #         ax.add_artist(first_legend)  # Add the legend manually to the Axes.
    #         plt.legend([handles[idx] for idx in order[9:]], [labels[idx] for idx in order[9:]], ncol=1,
    #                    loc='lower right', bbox_to_anchor=(0.975, 0.28))
    #
    #     plt.ylim(*infos["ylim"])
    #     plt.xlim(1, 50)
    #     plt.xlabel(r'Sparsity')
    #     plt.ylabel(infos["ylabel"], loc="top" if plot_name == "sota" else None)
    #     if plot_name == "sota":
    #         plt.yticks((0, 0.5))
    #
    #     fig.tight_layout(pad=0, rect=infos["rect"])
    #     plt.savefig(plot_dir / f"{plot_name}_{downscale}.svg")
    #     if "n_supports" in plot_name:
    #         plt.yscale("log")
    #         plt.savefig(plot_dir / f"{plot_name}_{downscale}_log.svg")
    plt.close("all")


if __name__ == '__main__':
    plot_dcv_step_size_paper("step_size", 50)
    # plot_ml_paper(('cal_housing', 'comp-activ-harder', 'ijcnn1', 'letter', 'slice', 'year',), (256,))
    #
    # plot_dt_paper(expe_name="unifr1000", threshold=0.95, factors=(256, ))
    # plot_dt_paper_zoom(expe_name="unifr1000", threshold=0.95, factors=(256, ))
    # plot_dt_paper(expe_name="unifr1000_noisy_n1e2", threshold=0.95, factors=(256,))
    # plot_dt_paper_zoom(expe_name="unifr1000_noisy_n1e2", threshold=0.95, factors=(256,))
    #
    # dcv_expe_name = "dcv_hist_1e5"
    # plot_dcv_paper(expe_name=dcv_expe_name, spars_max=50)
    # plot_dcv_n_supports(expe_name=dcv_expe_name, spars_max=50)
    #
    # dcv_expe_name_noisy = "dcv_hist_noisy_1e5"
    # plot_dcv_paper(expe_name=dcv_expe_name_noisy, spars_max=50)
    # plot_dcv_n_supports(expe_name=dcv_expe_name_noisy, spars_max=50)
    #
    # signal_expe_name = "test_noiseless"
    # signal_expe_name_noisy = "noisy"
    # for expe_name in (signal_expe_name, signal_expe_name_noisy):
    #     plot_signal_paper(expe_name=expe_name)
    #     plot_signal_paper_full(expe_name=expe_name)
    #     iterations_dcv(expe_name=expe_name)
    #     iterations_sup_dcv(expe_name=expe_name)
    #     plot_signal_paper_light(expe_name=expe_name)
    #
    # plt.close("all")
    # dcv_expe_name_noisy = "dcv_hist_noisy_1e5"
    # signal_expe_name_noisy = "noisy"
    # for file in chain((RESULT_FOLDER / dcv_expe_name_noisy / "icml").glob("*"),
    #                   Path(f"figures/exp_deconv/{signal_expe_name_noisy}/icml").glob("*")):
    #     if not file.stem.endswith("noisy"):
    #         # rename file
    #         file.rename(file.parent / f"{file.stem}_noisy{file.suffix}")

