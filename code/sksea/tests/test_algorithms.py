# -*- coding: utf-8 -*-
from unittest import TestCase
import math
import numpy as np
import scipy
import itertools
from sklearn.utils.estimator_checks import check_estimator

from sksea.sparse_coding import SparseSupportOperator

from sksea.algorithms import (els_fast, iht, ista, omp_fast, ompr_fast, sea, omp, ompr, htp_fast, htp, els, sea_fast,
                              SEA, niht)


# class TestAux(TestCase):
#
#
class TestAlgorithms(TestCase):
    # TODO add exact recovery in simple cases

    def test_ista(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                n_iter = 100
                D, y = get_random_problem()
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm = ista(linop=D, y=y,
                                           alpha=10 ** -6, n_iter=n_iter)
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_iht(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                n_iter = 100
                D, y = get_random_problem(i_run)
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        for algo in (iht, niht):
                            x, res_norm = algo(linop=D, y=y,
                                               n_nonzero=n_nonzero, n_iter=n_iter,
                                               f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                               grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y))
                            x2, res_norm2 = algo(linop=D, y=y,
                                                 n_nonzero=n_nonzero, n_iter=n_iter,
                                                 f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                 grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y))
                            err_msg = f'Run {i_run}, n_nonzero={n_nonzero}, algo={algo.__name__}'
                            np.testing.assert_array_equal(x, x2, err_msg=err_msg)
                            # self.assertTrue((x - x2 == 0).all(), msg=err_msg)
                            self.assertEqual(1, x.ndim, msg=err_msg)
                            self.assertEqual(n_cols, x.size, msg=err_msg)
                            self.assertLessEqual(np.count_nonzero(x), n_nonzero,
                                                 msg=err_msg)
                            # eps = 10 ** -6
                            # np.testing.assert_array_less(np.diff(res_norm), eps,
                            #                              err_msg=err_msg)

    def test_sea(self):
        n_runs = 50
        sea_sklearn = SEA()
        check_estimator(sea_sklearn)
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                n_iter = 100
                D, y = get_random_problem(i_run)
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x_old, res_norm_old = sea(linop=D, y=y,
                                                  n_nonzero=n_nonzero, n_iter=n_iter,
                                                  return_best=False,
                                                  f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                  grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                                  optimize_sea="all", optimizer='cg'
                                                  )

                        sea_params = dict(linop=D, y=y,
                                          n_nonzero=n_nonzero,
                                          n_iter=n_iter,
                                          f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                          grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                          return_history=False, optimizer='cg')

                        x_fast, res_norm_fast = sea_fast(**sea_params, return_best=False)
                        x_best, res_norm_best = sea_fast(**sea_params, return_best=True)

                        sea_params["return_history"] = True
                        sea_params["n_iter"] = None
                        x_n_nonzero, _, hist_n_nonzero = sea_fast(**sea_params, return_best=False)

                        sea_sklearn = SEA(n_nonzero, n_iter, random_state=i_run)
                        sea_sklearn.fit(D.matrix, y)
                        x_sklearn = sea_sklearn.coef_
                        res_norm_sklearn = sea_sklearn.res_norm_

                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        for x, res_norm in zip((x_best, x_old, x_fast, x_sklearn),
                                               (res_norm_best, res_norm_old, res_norm_fast, res_norm_sklearn)):
                            self.assertEqual(1, x.ndim, msg=err_msg)
                            self.assertEqual(n_cols, x.size, msg=err_msg)
                            self.assertLessEqual(np.count_nonzero(x), n_nonzero,
                                                 msg=err_msg)

                        # Compare residues, support and sparse iterate between implementations
                        np.testing.assert_array_almost_equal(res_norm_old[:-1], res_norm_fast[:-1], err_msg=err_msg)
                        if res_norm_old[-1] > 1e-4 or res_norm_fast[-1] > 1e-4:
                            # Tolerance of conjugate gradient descent is 1e-5 on normalized operator.
                            self.assertAlmostEqual(res_norm_old[-1], res_norm_fast[-1], msg=err_msg)
                        self.assertCountEqual(x_old.nonzero()[0], x_fast.nonzero()[0], msg=err_msg)
                        np.testing.assert_array_almost_equal(x_old, x_fast, decimal=4)

                        # Check if best version is better than regular version
                        np.testing.assert_array_almost_equal(res_norm_fast[:len(res_norm_best)], res_norm_best)
                        # np.testing.assert_array_almost_equal(res_norm_sklearn, res_norm_best)
                        self.assertLessEqual(res_norm_best[-1], min(res_norm_fast), msg=err_msg)
                        self.assertLessEqual(np.linalg.norm(D(x_best) - y), np.linalg.norm(D(x_fast) - y), msg=err_msg)
                        np.testing.assert_array_almost_equal(x_sklearn, x_best)

                        # Check if n_iter_is_n_support works
                        if hist_n_nonzero.get_n_supports(best=False) != n_nonzero + 1:
                            self.assertEqual(hist_n_nonzero.get_n_supports(), math.comb(n_cols, n_nonzero), msg=err_msg)

    def test_omp(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem(i_run)
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm = omp(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                          f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                          grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                          optimizer='cg')
                        x2, res_norm2 = omp(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                            f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                            grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                            optimizer='cg')
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertTrue((x - x2 == 0).all())
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_omp_fast(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem(i_run)
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm, history = omp_fast(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                                        f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                        grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                                        optimizer='cg')
                        x2, res_norm2 = omp(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                            f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                            grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                            optimizer='cg')
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertTrue((x - x2 == 0).all())
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_ompr(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem()
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm = ompr(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                           f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                           grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                           )
                        x2, res_norm2 = ompr(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                             f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                             grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                             )
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertTrue((x - x2 == 0).all())
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_els(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem()
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm = els(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                          f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                          grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                          )
                        x2, res_norm2 = els(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                            f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                            grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                            )
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertTrue((x - x2 == 0).all())
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_els_fast(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem(seed=i_run)
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm, history = els_fast(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                                        f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                        grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                                        )
                        x2, res_norm2 = els(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                            f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                            grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                            )
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertTrue((x - x2 == 0).all())
                        np.testing.assert_allclose(x, x2, err_msg=err_msg)
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_ompr_fast(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem(seed=i_run)
                n_rows, n_cols = D.shape
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        x, res_norm, history = ompr_fast(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                                         f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                         grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                                         )
                        x2, res_norm2 = ompr(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                             f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                             grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                             )
                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'
                        self.assertTrue((x - x2 == 0).all())
                        np.testing.assert_allclose(x, x2, err_msg=err_msg)
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_fast_succession(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem(seed=i_run)
                n_rows, n_cols = D.shape
                algos_init = [(omp_fast, omp), (els_fast, els)]
                algos = [(htp_fast, htp), (sea_fast, sea)]
                for algo_init, algo in itertools.product(algos_init, algos):
                    with self.subTest(algo_init=algo_init, algo=algo):
                        x, res_norm, history = algo[0](linop=D, y=y, n_nonzero=n_cols, n_iter=n_iter,
                                                       f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                       grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                                       algo_init=algo_init[0],
                                                       )
                        x2, res_norm2 = algo[1](linop=D, y=y, n_nonzero=n_cols, n_iter=n_iter,
                                                f=lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y),
                                                grad_f=lambda x_iter, linop: linop.H @ (linop @ x_iter - y),
                                                algo_init=algo_init[1],
                                                )
                        err_msg = f'Run {i_run}, algo_init={algo_init}, algo={algo}'
                        # self.assertTrue((x - x2 == 0).all())
                        np.testing.assert_allclose(x, x2, err_msg=err_msg, atol=1e-3)
                        self.assertEqual(1, x.ndim, msg=err_msg)
                        self.assertEqual(n_cols, x.size, msg=err_msg)
                        eps = 10 ** -6
                        np.testing.assert_array_less(np.diff(res_norm), eps,
                                                     err_msg=err_msg)

    def test_htp_fast(self):
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                # Test copy-pasted from ista
                n_iter = 100
                D, y = get_random_problem(i_run)
                n_rows, n_cols = D.shape
                f = lambda x_iter, linop: np.linalg.norm(linop @ x_iter - y)
                grad_f = lambda x_iter, linop: linop.H @ (linop @ x_iter - y)
                for n_nonzero in (1, n_cols, n_rows // 4):
                    with self.subTest(n_nonzero=n_nonzero):
                        fast_params = dict(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                           f=f, grad_f=grad_f, return_history=False, return_best=True,
                                           optimizer="cg")
                        x_best, res_norm_best = htp_fast(**fast_params)

                        fast_params["return_best"] = False
                        x_fast, res_norm_fast = htp_fast(**fast_params)

                        x_old, res_norm_old = htp(linop=D, y=y, n_nonzero=n_nonzero, n_iter=n_iter,
                                                  f=f, grad_f=grad_f, optimizer="cg")

                        err_msg = f'Run {i_run}, n_nonzero={n_nonzero}'

                        eps = 10 ** -6
                        for x, res_norm in zip((x_best, x_old, x_fast), (res_norm_best, res_norm_old, res_norm_fast)):
                            self.assertEqual(1, x.ndim, msg=err_msg)
                            self.assertEqual(n_cols, x.size, msg=err_msg)
                            np.testing.assert_array_less(np.diff(res_norm), eps,
                                                         err_msg=err_msg)

                        # Compare residues, support and sparse iterate between implementations
                        np.testing.assert_array_almost_equal(res_norm_old[:-1], res_norm_fast[:-1], err_msg=err_msg)
                        if res_norm_old[-1] > 1e-4 or res_norm_fast[-1] > 1e-4:
                            # Tolerance of conjugate gradient descent is 1e-5 on normalized operator.
                            self.assertAlmostEqual(res_norm_old[-1], res_norm_fast[-1], 3, msg=err_msg)
                        self.assertCountEqual(x_old.nonzero()[0], x_fast.nonzero()[0], msg=err_msg)
                        np.testing.assert_array_almost_equal(x_old, x_fast, decimal=4)

                        # Check if best version is better than regular version
                        np.testing.assert_array_almost_equal(res_norm_fast[:len(res_norm_best)], res_norm_best)
                        self.assertLessEqual(res_norm_best[-1], min(res_norm_fast), msg=err_msg)
                        self.assertLessEqual(np.linalg.norm(D(x_best) - y), np.linalg.norm(D(x_fast) - y), msg=err_msg)

    def test_cg(self):
        """
        Test conjugate gradient stability
        """
        n_runs = 100
        for i_run in range(n_runs):
            with self.subTest(i_run=i_run):
                D, y = get_random_problem(i_run)
                n_rows, n_cols = D.shape
                n_repet = 10
                all_out = np.empty((n_repet, n_cols))
                for repet in range(n_repet):
                    all_out[repet], _ = scipy.sparse.linalg.cg(D.T @ D, D.T @ y,
                                                               all_out[repet - 1] if repet != 0 else None, atol=0)
                for out in all_out:
                    np.testing.assert_array_equal(out, all_out[0])


def get_random_problem(seed=None):
    rand = np.random.RandomState(seed)
    n_rows = rand.randint(5, 20)
    n_cols = rand.randint(n_rows, 30)
    n_nonzero = rand.randint(1, n_cols + 1)
    D = rand.randn(n_rows, n_cols)
    rand_mat = rand.randn(n_rows, n_cols)
    x_ref = np.zeros(n_cols)
    s_ref = rand.permutation(n_cols)[:n_nonzero]
    x_ref[s_ref] = rand.rand(n_nonzero) + 1
    x_ref[s_ref] *= (-1) ** rand.randint(0, 2, n_nonzero)
    y = rand_mat @ x_ref
    return SparseSupportOperator(D, y, seed), y
