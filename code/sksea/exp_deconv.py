# Python import
import click
from collections import defaultdict
from itertools import combinations
from pathlib import Path
import pickle
from typing import Tuple, List, Dict

# Modules imports
import numpy as np
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from tabulate import tabulate
from tqdm import tqdm

# Scripts imports
from sksea.algorithms import ExplorationHistory
from sksea.deconvolution import ConvolutionOperator, gen_filter, gen_u
from sksea.exp_phase_transition_diag import NoiseType, gen_noise
from sksea.plot_icml import iterations_dcv, iterations_sup_dcv, plot_signal_paper, plot_signal_paper_full, \
    plot_signal_paper_light, get_best_hist
from sksea.utils import PAPER_LAYOUT, ALGOS_PAPER_DCV_PRECISE
from sksea.training_tasks import select_algo, ALGOS_TYPE


def solve_deconv_problem(h_op, x_len, spike_pos, seed, n_iter, manual=True, noise_factor=None, noise_type=None,
                         min_u=1, max_u=2, k_ratio=1, noise_on_x=False
                         ) -> Tuple[
    Dict[str, np.ndarray], Dict[str, np.ndarray], Dict[str, float],
    Dict[str, np.ndarray], Dict[str, 'ExplorationHistory']
]:
    """
    It generates a signal with a few spikes, convolves it with a random matrix,
    and then tries to recover the spikes using a few algorithms

    :param (ConvolutionOperator) h_op: the linear operator that maps the signal to the observation
    :param (int) x_len: length of the signal
    :param (Union[List[int], Tuple[int]]) spike_pos: the position of the spikes in the signal
    :param (int) seed: the seed for the random number generator
    :param (int) n_iter: number of iterations to run the algorithm for
    :return: A dictionary of the reference solutions and a dictionary of the solutions found by the algorithms.
    """
    # Generate signal
    rand = np.random.RandomState(seed)
    if manual:
        x = np.zeros(x_len)
        for pos in spike_pos:
            x += gen_u(x_len, pos, 3, rand)
    else:
        x = gen_u(x_len, len(spike_pos), 4, rand, min_u=min_u, max_u=max_u)

    y = h_op(x)  # Generate observation
    if noise_factor is not None:
        if noise_on_x:
            x += gen_noise(noise_type, noise_factor, x, rand)
        else:
            y += gen_noise(noise_type, noise_factor, y, rand)
    # Get algorithms we want to run
    algos_studied = [
        "ELSFAST", "OMPRFAST", "OMPFAST",
        "HTPFAST", "HTPFAST-els", "IHT", "IHT-els", "IHT-omp", "HTPFAST-omp",
        "SEAFAST", "SEAFAST-els", "SEAFAST-omp"
    ]
    algorithms = select_algo(ALGOS_TYPE[:4],
                             algos_studied,
                             sea_params=dict(return_both=True))
    solutions = {}
    refs = {f"x_{spike_pos}": x, f"y_{spike_pos}": y}
    best_res = {}
    res_norms = {}
    histories = {}

    # Solve problem with algorithms
    for name, algorithm in algorithms.items():
        label = f"{name}_{spike_pos}"
        out = algorithm(linop=h_op, y=y, n_nonzero=int(k_ratio * len(spike_pos)), n_iter=n_iter,
                        f=lambda x, linop: np.linalg.norm(linop @ x - y),
                        grad_f=lambda x, linop: linop.H @ (linop @ x - y))

        if "SEA" in name or "HTP" in name:
            (_, res_norm, hist), (x_est, res_norm_best, _) = out
            histories[label] = hist
            best_res[label] = res_norm_best[-1] / np.linalg.norm(y)
        else:
            if "FAST".lower() in name.lower():
                x_est, res_norm, hist = out
                histories[label] = hist
            else:
                x_est, res_norm = out
            best_res[label] = res_norm[-1] / np.linalg.norm(y)

        solutions[label] = x_est
        res_norms[label] = res_norm / np.linalg.norm(y)
    return refs, solutions, best_res, res_norms, histories


def plot_results(refs, solutions, best_res, h, n_solutions, pos_list, out_file) -> None:
    """
    Plots the results of the deconvolution experiments

    :param (Dict[str, np.ndarray]) solutions: a dictionary of the form {name_pos: x_sol} where name_pos is the name of
    the algorithm and the position of the spikes, and x_sol is the solution of the algorithm
    :param (Dict[str, np.ndarray]) refs: a dictionary of reference signals (the true signals)
    :param (np.ndarray) h: the filter
    :param (int) n_solutions: number of algorithm used
    :param (List[Union[List[int], Tuple[int]]]) pos_list: list of spike positions
    :param (Union[str, Path]) out_file: the path to the output file
    """
    # Color scheme for curves
    colors = px.colors.qualitative.Plotly
    colors1 = colors[:2]
    colors2 = colors[2:]

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    # Plot solutions of each algo
    for idx, (name_pos, x_sol) in enumerate(solutions.items()):
        name = name_pos.split('_')[0]
        fig.add_trace(go.Scatter(y=x_sol, line=dict(color=colors2[(idx % n_solutions) % len(colors2)]),
                                 name=name, legendgroup=name, visible=True),
                      secondary_y=False)

    # Plot signal and observation
    for idx, (name_pos, x_sol) in enumerate(refs.items()):
        name = name_pos.split('_')[0]
        fig.add_trace(go.Scatter(y=x_sol, line=dict(color=colors1[idx % len(colors1)], dash='dash'),
                                 name=name, visible=True, legendgroup=name),
                      secondary_y=False)
    fig.add_trace(go.Scatter(y=h, line=dict(color=colors[0]), name="filter"), secondary_y=True)

    # Slider (start) --------------------------  https://plotly.com/python/sliders/
    steps = []
    n_data = len(fig.data)
    n = (n_data - 1 - len(pos_list) * len(colors1)) // (len(pos_list))
    n_steps = len(pos_list)
    for idx, pos in enumerate(pos_list):
        performances = [(name_pos.split('_')[0], res)
                        for i, (name_pos, res) in enumerate(best_res.items())
                        ]  # if idx * n <= i < (idx + 1) * n]
        performances.sort(key=lambda couple: couple[1])
        annotation = tabulate(performances, headers=["Algorithm", "Best relative loss"],
                              tablefmt="github").replace("\n", "<br>")
        step = dict(
            # Update method allows us to update both trace and layout properties
            method='update',
            args=[
                # Make the traces from `pos` visible
                {'visible': [idx * n <= i < (idx + 1) * n for i in range(n_steps * n)  # Get solution (x) for each algo
                             ] + [idx * len(colors1) <= i - n_steps * n < (idx + 1) * len(colors1)
                                  for i in range(n_steps * n, n_data - 1)  # Get y_true and x_true for each algo
                                  ] + [True]},  # Keep the filter displayed

                # Set the title for the these traces
                {'title.text': f"Deconvolution experiments for spikes at {pos}",
                 # https://community.plotly.com/t/plotly-py-graph-object-updating-annotation-with-slider/52639
                 'annotations': [
                     go.layout.Annotation(
                         align='left',  # Align text to the left
                         yanchor='top',  # Align text box's top edge
                         text=annotation,  # Set text with '<br>' strings as newlines
                         showarrow=False,  # Hide arrow head
                         width=400,  # Wrap text at around 300 pixels
                         xref='paper',  # Place relative to figure, not axes
                         yref='paper',
                         font={'family': 'Courier'},  # Use monospace font to keep nice indentation
                         x=1,  # Place on right edge
                         y=1  # Place on the top edge
                     )
                 ]}],
            label=str(pos)
        )
        steps.append(step)
    sliders = [go.layout.Slider(
        active=1,
        # len=0.5,
        currentvalue={"prefix": "Spike positions: "},
        steps=steps
    )]
    # Slider (stop) --------------------------

    fig.update_layout(
        title="Deconvolution experiment. Use the slider to display the figures.",
        legend_title="Algorithms",
        sliders=sliders,
    )
    # Set y-axes titles
    fig.update_yaxes(title_text="Signals", secondary_y=False)
    fig.update_yaxes(title_text="Filter", secondary_y=True)

    out_file = Path(out_file)
    out_file.parent.mkdir(exist_ok=True, parents=True)
    fig.write_html(out_file)


def pad_signal(signal):
    y_pad = []
    x_pad = []
    y_scat = []
    x_scat = []
    for idx, val in enumerate(signal):
        if val == 0:
            y_pad.append(0)
            x_pad.append(idx)
        else:
            y_pad.extend((0, val, 0))
            x_pad.extend((idx, idx, idx))
            y_scat.append(val)
            x_scat.append(idx)
    return dict(y=y_pad, x=x_pad), dict(y=y_scat, x=x_scat)


def plot_results_paper(refs, solutions, out_file, nips=False):
    fig = go.Figure()

    mapping_nips = {
        "IHT": "$\\text{IHT}$",
        "HTP": "$\\text{HTP}$",
        "ELS": "$\\text{ELS, OMP, OMPR}, \\text{IHT}_{\\text{OMP}}, \\text{HTP}_{\\text{ELS}}$",
        "SEAFAST-els": "$\\text{SEA}_0, \\text{SEA}_{\\text{OMP}}, \\text{SEA}_{\\text{ELS}}$"
    }

    size = 15
    size_x = 17
    width = 1
    marker_line_width = 1

    grey_level = 245
    fig.add_trace(go.Scatter(y=[-7, 7, 7, -7, -7], x=[378, 378, 439, 439, 378],
                             fill='toself', fillcolor=f'rgb({grey_level},{grey_level},{grey_level})',
                             line_color='rgba(0,0,0,0)', showlegend=False))

    legendrank = defaultdict(lambda: 1000)
    legendrank.update(
        {
            "ELS": 1001,
            "SEAFAST-els": 1002,
            "IHT": 1000,
            "HTP": 1000,
        })
    # Plot signal and observation
    for idx, (name_pos, x_sol) in enumerate(refs.items()):
        name = name_pos.split('_')[0]
        if name_pos.startswith('x'):
            continue
        else:
            fig.add_trace(go.Scatter(**dict(y=x_sol), line=dict(color='#24405e', width=2),  # , dash='dash'),
                                     name=f"${name}$"))

    # Plot solutions of each algo
    for idx, (name_pos, x_sol) in enumerate(list(solutions.items())[::-1]):
        name = name_pos.split('_')[0]
        try:
            line, points = pad_signal(x_sol)
            info = ALGOS_PAPER_DCV_PRECISE[name]
            display_name = mapping_nips[name] if nips else info["name"]
            marker_info = info["marker"]
            marker_info.update(dict(size=size, line_width=marker_line_width))
            line_info = info["line"]
            line_info.update(dict(width=width))
            fig.add_trace(go.Scatter(**line, line=line_info, showlegend=False,
                                     name=display_name, legendgroup=info["name"], legendrank=legendrank[name]))
            fig.add_trace(go.Scatter(**points, line=line_info, mode='markers', marker=marker_info,
                                     name=display_name, legendgroup=info["name"], legendrank=legendrank[name]))
        except Exception as e:
            print(f"Do not plot {name} in the paper version of the figure")

    # Plot signal and observation
    for idx, (name_pos, x_sol) in enumerate(refs.items()):
        name = name_pos.split('_')[0]
        if name_pos.startswith('x'):
            line, points = pad_signal(x_sol)
            fig.add_trace(go.Scatter(**line, line=dict(color='#FF6692', dash='dash', width=width),
                                     name=name, showlegend=False, legendgroup=name))
            fig.add_trace(go.Scatter(**points, line=dict(color='#FF6692', dash='dash'),
                                     mode='markers', marker=dict(symbol=100, size=size_x, line_width=marker_line_width),
                                     name=r"$x^*$", legendgroup=name, legendrank=999))
        else:
            continue

    fig.update_layout(
        **PAPER_LAYOUT
    )
    tick_array = np.arange(19, 500, 20)
    legend_position1 = dict(
        y=1.05,
        x=0,
        xanchor="left",
        yanchor="top",
        bgcolor='rgba(0,0,0,0)'
    )
    fig.update_layout(
        xaxis=dict(
            showgrid=False,
            showline=False,
            position=0.515,
            anchor="free",
            ticks="outside",
            tickvals=tick_array,
            tickmode="array",
            ticktext=[f"{i + 1 if (i + 1) % 100 == 0 and (i + 1) != 300 else ''}" for i in tick_array],
            nticks=50,
            tickwidth=2,
            ticklen=10,
            showticklabels=True,
        ),
        margin=dict(
            autoexpand=False,
            r=105,
            l=30
        ),
        legend=dict(
            title=None,
            orientation='h',
            **legend_position1,
            entrywidth=0
        ),
        showlegend=True,
    )

    out_file = Path(out_file)
    out_file.parent.mkdir(exist_ok=True, parents=True)
    fig.write_html(out_file, include_mathjax='cdn')
    fig.update_layout(showlegend=False)
    fig.write_html(out_file.parent / (out_file.stem + "_no_legend.html"), include_mathjax='cdn')


def plot_sea_loss(res_norms, out_dir, loss_fig):
    # algo_order = ["SEAFAST-els", "SEAFAST", "HTP", "IHT",]
    # legend_ranks = ["HTP", "IHT", "SEAFAST", "SEAFAST-els"]
    algo_order = ["SEAFAST-els", "SEAFAST", "HTPFAST", "ELSFAST", ]
    legend_ranks = ["HTPFAST", "ELSFAST", "SEAFAST", "SEAFAST-els"]
    colors = ["#EF553B", "#19D3F3", "#FFA15A", "#636EFA", ]
    keys_list = list(res_norms.keys())
    keys_list.sort(key=lambda key: algo_order.index(key.split('_')[0]))
    for idx_algo, name_pos in enumerate(keys_list):
        loss = res_norms[name_pos]
        fig = go.Figure()
        name = name_pos.split('_')[0]
        info = ALGOS_PAPER_DCV_PRECISE[name]
        for idx, current_fig in enumerate((fig, loss_fig)):
            if hasattr(loss, "shape") and loss.shape[0] < 1000:
                loss = np.pad(loss, (0, 1000 - loss.shape[0]), 'edge')
            current_fig.add_trace(go.Scatter(y=loss,
                                             name=r"$x^t$" if idx == 0 else r"$x^t_{" + info["name"][1:-1] + "}$",
                                             legendrank=legend_ranks.index(name),
                                             line={"width": 1.5, "color": colors[idx_algo]}))
            if "SEA" in name or "ELS" in name:
                color = "indigo" if "els" in name_pos else "blue" if "ELS" in name else "green"
                current_fig.add_trace(go.Scatter(y=get_best_hist(loss),
                                                 line={"dash": "dash", "width": 1.5, "color": color},
                                                 name=r"$x^{t_{\text{BEST}}}_{" + info["name"][1:-1] + "}$",
                                                 legendrank=legend_ranks.index(name)))
        fig.update_layout(
            **PAPER_LAYOUT,
            yaxis_title=r"$\ell_{2, \text{rel}}$",
            xaxis_title="Explored supports" if str(out_dir).endswith("hist") else "Iterations",
            legend=dict(
                y=0.95,
                x=0.95,
                xanchor="right",
            )
        )
        fig.update_layout(
            legend_title=r"",
        )
        out_file = out_dir / f"{info['disp_name']}_{'hist' if str(out_dir).endswith('hist') else 'losses'}.html"
        out_file.parent.mkdir(exist_ok=True, parents=True)
        fig.write_html(out_file, include_mathjax='cdn')


def run_and_plot_experiment(x_len, h_len, sigma, n_spikes, seed, n_iter, noise_factor, noise_type, expe_name, min_u,
                            max_u, k_ratio, noise_on_x):
    # Generate filter
    h_len = x_len if h_len is None else h_len
    h = gen_filter(h_len, 3, sigma)
    h_op = ConvolutionOperator(h, x_len)

    # Generate spike position list
    manual = True
    # if n_spikes == 2:
    #     begin, end = (25, 39)
    #     pos_list = [(begin, end - k) for k in range(end - begin)]
    # elif n_spikes == 3:
    #     begin, end = (20, 30)
    #     pos_list = [(begin, *others) for others in combinations(range(begin + 1, end + 1), n_spikes - 1)]
    if n_spikes >= 1:
        manual = False
        begin, end = (None, None)
        pos_list = [[seed] * n_spikes]
    else:
        raise Exception

    root = Path(f"figures/exp_deconv/{expe_name}")
    root.mkdir(parents=True, exist_ok=True)
    out_file = root / f"devonc_{seed}_{n_spikes}_{begin}-{end}.html"

    # Solve problems
    refs = {}
    solutions = {}
    best_res = {}
    res_norms = {}
    histories = {}
    for spike_pos in tqdm(pos_list, "Solving multiple combinations of spikes"):
        refs_pos, solutions_pos, best_res_pos, res_norms_pos, history_pos = solve_deconv_problem(h_op, x_len, spike_pos,
                                                                                                 seed, n_iter, manual,
                                                                                                 noise_factor,
                                                                                                 noise_type, min_u,
                                                                                                 max_u, k_ratio,
                                                                                                 noise_on_x)
        refs.update(refs_pos)
        solutions.update(solutions_pos)
        best_res.update(best_res_pos)
        res_norms.update(res_norms_pos)
        histories.update(history_pos)
    n_solutions = len(solutions_pos)
    solutions_y = {label: h_op(solution) for label, solution in
                   solutions.items()}  # Get y predicted for each algorithm
    with open(root / "refs.pkl", "wb") as f:
        pickle.dump(refs, f)
    with open(root / "solutions.pkl", "wb") as f:
        pickle.dump(solutions, f)
    with open(root / "histories.pkl", "wb") as f:
        pickle.dump(histories, f)
    with open(root / "res_norms.pkl", "wb") as f:
        pickle.dump(res_norms, f)
    # Create and save figures
    plot_results(refs, solutions, best_res, h, n_solutions, pos_list, out_file)
    plot_results(refs, solutions_y, best_res, h, n_solutions, pos_list,
                 out_file.parent / (out_file.stem + "_y" + out_file.suffix))
    #
    # # Figure for paper
    # # sol1, sol2 = {}, {}
    # sol_nips = {}
    # res_norms_sea = {}
    # hist_sea = {}
    # for alg_name, val in solutions.items():
    #     # print(alg_name)
    #     # if "IHT" in alg_name or "SEAFAST" in alg_name:
    #     #     sol1[alg_name] = val
    #     # else:
    #     #     sol2[alg_name] = val
    #     if ("SEA" in alg_name or "IHT" == alg_name.split('_')[0] or "HTPFAST" == alg_name.split('_')[0]
    #         or "ELSFAST" == alg_name.split('_')[0]) and 'omp' not in alg_name:
    #         res_norms_sea[alg_name] = res_norms[alg_name]
    #         hist = histories.get(alg_name, None)
    #         if hist is not None:
    #             hist_sea[alg_name] = hist.get_loss_by_explored_support()
    #     if alg_name.split('_')[0] not in ("SEAFAST", "SEAFAST-omp"):
    #         sol_nips[alg_name] = val
    #
    # # plot_results_paper(refs, sol1, root / "prec1.html")
    # # plot_results_paper(refs, sol2, root / "prec2.html")
    # plot_results_paper(refs, sol_nips, root / "paper_signals.html", nips=True)
    # # plot_results_paper(refs, {}, root / "prec_pb.html")
    # loss_fig = go.Figure()
    # hist_fig = go.Figure()
    # plot_sea_loss(res_norms_sea, root, loss_fig)
    # plot_sea_loss(hist_sea, root / "hist", hist_fig)
    # for fig, fig_name in zip((loss_fig, hist_fig), ("losses", "hist")):
    #     fig.update_layout(
    #         **PAPER_LAYOUT,
    #         yaxis_title=r"$\ell_{2, \text{rel}}$",
    #         xaxis_title="Explored supports" if fig_name == "hist" else "Iterations",
    #     )
    #     fig.update_layout(
    #         legend=dict(title="", y=0.95, x=0.95, xanchor="right", entrywidth=50, orientation='h')
    #     )
    #     fig.write_html(root / f"paper_{fig_name}.html", include_mathjax='cdn')


@click.command(context_settings={'show_default': True, 'help_option_names': ['-h', '--help']})
@click.option('--x_len', '-xl', default=500, help='Size of the signal')
@click.option('--h_len', '-hl', default=None, type=int, help='Size of the gaussian filter')
@click.option('--sigma', '-s', default=3, type=float, help='Variance of the gaussian filter')
@click.option('--n_spikes', '-ns', default=20, type=int, help='Number of spike of the signal')
@click.option('--seed', '-sd', default=118, type=int, help='Random seed of the experiment')
@click.option('--n_iter', '-i', default=1000, type=int, help='Number max of iteration that the algorithm can do')
@click.option('--noise-factor', '-nf', default=None, type=float, help="Variance of the gaussian noise")
@click.option('--noise_type', '-nt', default=None,
              type=click.Choice(dir(NoiseType)[:len(NoiseType)], case_sensitive=False), help="How compute the noise")
@click.option('--expe_name', '-en', default=None)
@click.option('--min_u', '-mnu', default=1, type=int, help='Minimum value of the spikes')
@click.option('--max_u', '-mxu', default=2, type=int, help='Maximum value of the spikes')
@click.option('--k_ratio', '-kr', default=1, type=float, help='Ratio of the number of spikes to recover')
@click.option('--noise_on_x', '-nox', default=False, type=bool, is_flag=True, help='Add noise on x')
def main(x_len, h_len, sigma, n_spikes, seed, n_iter, noise_factor, noise_type, expe_name, min_u, max_u, k_ratio,
         noise_on_x):
    # Run and plot results of the experiment
    print(locals())
    run_and_plot_experiment(x_len, h_len, sigma, n_spikes, seed, n_iter, noise_factor, noise_type, expe_name,
                            min_u, max_u, k_ratio, noise_on_x)
    plot_signal_paper(expe_name=expe_name)
    plot_signal_paper_full(expe_name=expe_name)
    iterations_dcv(expe_name=expe_name)
    iterations_sup_dcv(expe_name=expe_name)
    plot_signal_paper_light(expe_name=expe_name)


# Needed for click CLI
if __name__ == '__main__':
    main()
