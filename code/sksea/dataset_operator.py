# Python imports
from enum import Enum
import inspect
import os
from pathlib import Path
from typing import List, Tuple
import zipfile

# Module imports
import gdown
import numpy as np
import pandas as pd
from loguru import logger
from sklearn.preprocessing import normalize

# Script imports
from sksea.sparse_coding import SparseSupportOperator

ROOT = Path(os.path.abspath(inspect.getfile(inspect.currentframe()))).parent
RESULT_PATH = ROOT / "results/training_tasks"


class Task(Enum):
    """
    Enumerator for type of tasks
    """
    REGRESSION = 0
    BINARY = 1


class DatasetOperator:
    """
    Class for handling datasets as linear operators
    """
    _DATASETS_DL_PATH = ROOT / "downloads/datasets"
    _DATASETS_PATH = ROOT / "datasets"
    REGRESSIONS_NAME = ('cal_housing', 'comp-activ-harder', 'slice', 'year')
    BINARY_NAME = ('letter', 'ijcnn1', 'kddcup04_bio', 'census')
    K_MAX = {
        'kddcup04_bio': 10,
        'cal_housing': 9,
        'census': 12,
        'comp-activ-harder': 12,
        'ijcnn1': 14,
        'letter': 16,
        'slice': 40,
        'year': 40
    }
    MAX_K_MAX = max(K_MAX.values())
    ORDER = ('cal_housing', 'letter', 'comp-activ-harder', 'ijcnn1', 'kddcup04_bio', 'year', 'slice', 'census')

    def __init__(self, name, no_loading=False):
        """

        :param (str) name: Name of the dataset to load
        :param (bool) no_loading: If True, don't load the dataset.
            A call to load method will be needed before any computation
        """
        self.grad_f = None
        self.f = None
        self.linop = None
        self.y = None
        namelist = self.get_dataset_names()
        if name not in namelist:
            raise ValueError(f'Name must be in {namelist}')
        self.name = name
        self.k_max = self.K_MAX[name]

        self.task = Task.REGRESSION if name in self.REGRESSIONS_NAME else Task.BINARY
        self._loaded = False
        if not no_loading:
            self.load()

    def load(self):
        """
        Load dataset in memory
        """
        if not self._loaded:
            data_mat, self.y = self._load_and_preprocess()
            self.linop = SparseSupportOperator(data_mat, self.y)

            if self.task == Task.REGRESSION:
                self.f = lambda x, linop_s=self.linop: (np.linalg.norm(linop_s @ x - self.y) ** 2) / 2
                self.grad_f = lambda x, linop_s=self.linop: linop_s.H @ (linop_s @ x - self.y)
            else:
                from scipy.special import expit  # For parallel computation without importation error
                self.f = lambda x, linop_s=self.linop: -self.y.T @ np.log(expit(linop_s @ x)
                                                                          ) - (1 - self.y).T @ np.log(
                    1 - expit(linop_s @ x
                              ))
                self.grad_f = lambda x, linop_s=self.linop: linop_s.H @ (expit(linop_s @ x) - self.y)
            self._loaded = True

    @classmethod
    def get_dataset_names(cls) -> List[str]:
        """
        Return the list of available dataset names
        """
        cls._check_download()
        return [path.stem for path in cls._DATASETS_PATH.glob('*.json')]

    def _load_and_preprocess(self) -> Tuple[np.ndarray, np.ndarray]:
        """
        Load json file and preprocess according to the paper

        :return: Data matrix and labels
        """
        df = pd.read_json(self._DATASETS_PATH / f'{self.name}.json').fillna(0)
        y = df.label.to_numpy()
        if self.task == Task.BINARY:  # Put labels from {-1, 1} to {0, 1}
            y = np.fmax(0, y)
        x_df = df.drop(columns='label')
        x_df["intercept"] = 1
        x = normalize(x_df.to_numpy(), axis=0)
        return x, y

    @classmethod
    def _check_download(cls) -> None:
        """
        Check that the datasets are downloaded and converted
        """
        if not cls._DATASETS_PATH.is_dir() or len(list(cls._DATASETS_PATH.glob('*.json'))) < 8:
            cls._download_datasets()
            cls._convert_datasets()

    @classmethod
    def _download_datasets(cls) -> None:
        """
        Download datasets from Google Drive, extract them
        """
        archive_path = ROOT / "downloads/datasets.zip"
        archive_path.parent.mkdir(exist_ok=True)
        logger.info("Downloading datasets")
        gdown.download("https://drive.google.com/uc?id=1RDu2d46qGLI77AzliBQleSsB5WwF83TF", str(archive_path))
        if not archive_path.is_file():
            raise FileNotFoundError("The dataset cannot be download automatically from Google Drive. "
                                    "Please download it manualy from "
                                    "https://drive.google.com/file/d/1RDu2d46qGLI77AzliBQleSsB5WwF83TF/view, "
                                    "and place it in code/sksea/downloads/datasets.zip. "
                                    "Then, launch this command again.")
        with zipfile.ZipFile(archive_path, 'r') as zip_ref:
            zip_ref.extractall(cls._DATASETS_DL_PATH)

    @classmethod
    def _convert_datasets(cls) -> None:
        """
        Convert .vm file to .json using vm input format for simple datasets.
        Vm format: https://github.com/VowpalWabbit/vowpal_wabbit/wiki/Input-format
        """
        cls._DATASETS_PATH.mkdir(exist_ok=True)
        for filepath in cls._DATASETS_DL_PATH.glob('*.vw'):
            logger.info(f'Converting {filepath.name}')
            out = ['[']

            # Read VM file
            with open(filepath, 'r') as file:
                lines = file.readlines()

            # Convert to JSON structure
            for line in lines:
                line_stripped = line.strip().replace(' |f', '').replace('+1', '1')  # Remove namespace
                line_labelled = f'label:{line_stripped}'  # Add label title
                features = line_labelled.split(' ')  # Split features
                features_corrected = []

                for feat in features:
                    if ':' not in feat:  # Add default value : 1
                        feat += ':1'
                    key, value = feat.split(':')
                    features_corrected.append(f'"{key}":{value}')  # Add " " to keys
                line_with_default = ', '.join(features_corrected)  # Get the JSON line together
                out.append('{' + line_with_default + '},\n')

            # Remove side effects
            out[-1] = out[-1].strip()[:-1]
            out.append(']')

            # Save to JSON file
            json_path = cls._DATASETS_PATH / filepath.with_suffix('.json').name
            with open(json_path, 'w') as file:
                file.writelines(out)

    def __repr__(self) -> str:
        """
        Add the name of the dataset to the object representation
        """
        return super(DatasetOperator, self).__repr__() + ' - ' + self.name
