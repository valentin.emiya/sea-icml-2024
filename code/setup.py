# -*- coding: utf-8 -*-
from setuptools import setup
from sys import version_info

if version_info.minor == 11:
    setup(name='sksea',
          version='0.1',
          description='Support exploration algorithms',
          license='GPL',
          packages=['sksea'],
          zip_safe=False,
          python_requires='~=3.11',
          extras_require={
              'dev': ['coverage', 'pytest', 'pytest-cov'], },
          install_requires=[
              "click==8.*,>=8.1.7",
              "gdown==5.*,>=5.0.1",
              "hyperopt==0.*,>=0.2.7",
              "loguru==0.*,>=0.7.2",
              "mat4py==0.*,>=0.6.0",
              "matplotlib==3.*,>=3.8.2",
              "numpy==1.*,>=1.26.3",
              "pandas==2.*,>=2.2.0",
              "plotly==5.*,>=5.18.0",
              "pyarrow==15.*,>=15.0.0",
              "pysindy==1.*,>=1.7.5",
              "ray==2.*,>=2.9.1",
              "requests==2.*,>=2.31.0",
              "scikit-learn==1.*,>=1.4.0",
              "scipy==1.*,>=1.12.0,<1.14.0",
              "tabulate==0.*,>=0.9.0",
              "tqdm==4.*,>=4.66.1",
          ],
          )
else:
    setup(name='sksea',
          version='0.1',
          description='Support exploration algorithms',
          license='GPL',
          packages=['sksea'],
          zip_safe=False,
          python_requires='>=3.8,<3.11',
          extras_require={
              'dev': ['coverage', 'pytest', 'pytest-cov'], },
          install_requires=[
              'numpy~=1.24.1',
              'scipy~=1.10.0',
              'matplotlib~=3.6.3',
              'scikit-learn~=1.2.0',
              'mat4py~=0.5',
              'click~=8.1.3',
              'ray~=2.2.0',
              'tqdm~=4.64.1',
              'loguru~=0.6.0',
              'plotly~=5.11.0',
              'gdown>=4.6.0',
              'pandas~=1.5.2',
              'tabulate~=0.9.0',
              'pysindy~=1.7.5',
              'requests~=2.27.1',
              'hyperopt~=0.2.7'
          ],
          )
