"""
Minimal working example of SEA usage
"""
import numpy as np

from sksea.algorithms import sea_fast, omp, SEA, SEASelector  # , els, htp_fast, iht, ompr
from sksea.sparse_coding import SparseSupportOperator
from sksea.utils import hard_thresholding


if __name__ == '__main__':
    # Seed for repeatability
    seed = 0
    random = np.random.RandomState(seed)

    # Problem instantiation
    n_atom = 20
    n_nonzero = 3
    data_mat = random.random((10, n_atom))
    x_full = random.random(n_atom)
    x_sparse = hard_thresholding(x_full, n_nonzero)  # Keep the n_nonzero highest coefficient
    y = data_mat @ x_sparse

    # SEA usage with low-level function
    linop = SparseSupportOperator(data_mat, y, seed)  # Linear Operator instantiation
    f = lambda x, linear_operator: np.linalg.norm(linear_operator @ x - y) / 2  # Function to minimize
    grad_f = lambda x, linear_operator: linear_operator.H @ (linear_operator @ x - y)  # Gradient of the function

    x_sea, res_norm_sea, exploration_history_sea = sea_fast(linop, y, n_nonzero, n_iter=20, f=f, grad_f=grad_f,
                                                            optimizer='cg', return_best=True)
    print(res_norm_sea)

    # SEA usage with sklearn-API
    sea = SEA(n_nonzero, n_iter=20, random_state=seed)
    sea.fit(data_mat, y)
    # x_sea = sea.coef_
    # res_norm_sea = sea.res_norm_
    # explored_supports = sea.exploration_

    # SEA usage with sklearn feature selector API
    sea_selector = SEASelector(n_nonzero, n_iter=20, random_state=seed)
    sea_selector.fit(data_mat, y)
    X_selected = sea_selector.transform(data_mat)
    support_ranking = sea_selector.get_top_p_ranking(p=5)
    print(support_ranking[["rank", "loss", "nonzero_idx"]])
    # print(support_ranking[["rank", "loss", "nonzero_idx", "support", "sparse_vector"]])

    # Exploration history usage
    explored_supports = exploration_history_sea.get_supports()  # Numpy array containing all explored support
    support = explored_supports[0]  # Get one support
    x_estimated, gradient, loss_value = exploration_history_sea.get(support)  # Get insight on one specific support

    # SEA warm-started by omp
    x_sea_omp, res_norm_sea_omp, exploration_history_sea_omp = sea_fast(linop, y, n_nonzero, n_iter=100, f=f, grad_f=grad_f,
                                                                        return_best=True, algo_init=omp)
