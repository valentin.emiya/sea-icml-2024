Guaranteed sparse support recovery using the Straight-Through-Estimator
=======================================================================

Support Exploration Algorithm (SEA)

Read our paper at : https://hal.science/hal-03964976/document

Installation
------------

**All experiments were done with Python 3.8 on Linux.**
**Compatibility tests were done with Python 3.8 on Windows.**
**This project is also working with Python 3.9, 3.10 and 3.11 on Linux.**
There are known issues with Python3.9 on Anaconda for the phase diagram experiment.
We did not try running the experiments with other configurations.
No compatibility test was done.

Go to the ``code`` folder and install locally by running ::

    pip install -e .

Minimal working example
-----------------------

A minimal working example is available in `minimal_example.py`.

Running experiments of the paper
--------------------------------

All plots are saved in svg file.
Some plot have a draft version, which is a html file.
You might want to zoom in or out in order to have a better view of the figure in the draft version of the figures.

Phase diagram experiment
^^^^^^^^^^^^^^^^^^^^^^^^

This experiment has a really long running time. You need to use a device with a lot of CPU cores to reduce the computation time.
Known issue: Using Anaconda on Windows, the program can stop and display `Aborted!` in the terminal.
If this happen, you can run again the command for launching the experiment, it will resume where it stopped.

Noiseless experiment
""""""""""""""""""""

Go to the ``code/sksea`` folder and run ::

    python run_exp_phase_transition.py -en noiseless -na 500 -cpu 5

This will reproduce the phase diagram experiment with the parameters of the paper:

- -en noiseless: The experiment will be named noiseless.
- -na 500: Size of x*.
- -f 256: SEA and IHT based algorithms will run with 256*sparsity iterations.
- -cpu 10: No more than 10 cpus will be used to make computation (The code is fully parallelized, each column of the phase diagram is computed in parallel. For a point of the phase diagram, each problem is computed in batches in parallel).

You can choose the number of problem solve by a single cpu process by using the ``-bs`` option.
By default, the value is set to 200 (``-bs 200``).
If you only want to run a subset of the algorithms, you can use the ``-af`` option.
By default, the value is set to ``-af OMP -af ELS -af OMPR -af IHT -af IHT-els -af IHT-omp -af HTPFAST -af HTPFAST-els -af HTPFAST-omp -af SEAFAST -af SEAFAST-omp -af SEAFAST-els``.
For instance, if you want to run only SEA and SEA-els, you can use ``-af SEAFAST -af SEAFAST-els``.
For speeding up the computation, you can run independently one column of the phase diagram by using the -dv option.
By default, the value is set to ``-dv 0.025 -dv 0.05 -dv 0.075 -dv 0.1 -dv 0.125 -dv 0.15 -dv 0.175 -dv 0.2 -dv 0.225 -dv 0.25 -dv 0.275 -dv 0.3 -dv 0.4 -dv 0.5 -dv 0.6 -dv 0.7 -dv 0.8 -dv 0.9``.
So, for instance, if you want to run only the first column of the phase diagram, you can use ``-dv 0.025 -ncp``.
Adding ``-ncp`` will disable the compilation of the results.
When everything is computed, you can run ``python run_exp_phase_transition.py -en noiseless -cp`` to compile the results.

More help is available using the ``python run_exp_phase_transition.py -h`` command.
Raw computation files are located in ``code/sksea/data_results`` folder.

When computations are done, adding ``-pl`` to the precedent command or running ::

    python run_exp_phase_transition.py -en noiseless -na 500 -pl

will plot the results of the experiment in ``code/sksea/figures/icml`` with the parameters of the paper:

- -pl: Plot results instead of making computations.

Thus you will find in ``code/sksea/figures/icml`` folder:

- ``noiseless.svg`` which is the empirical support recovery phase transition diagram with the level curves of success probabilities 0.95 in the noiseless case shown in the paper.
- ``noiseless_zoom.svg`` which is the empirical support recovery phase transition diagram with the level curves of success probabilities 0.95 in the noiseless case shown in the paper, zoomed in the region shown in the introduction of the paper.

Also, you will find in ``code/sksea/figures/comparison`` folder, ``noiseless_0.0001_hm.html`` which is a draft version of the curve of the paper, with one curve by algorithm.
You might want to zoom in order to see the curves properly.

Noisy experiment
""""""""""""""""

Go to the ``code/sksea`` folder and run ::

    python run_exp_phase_transition.py -en noisy -na 500 -cpu 5 -nf 0.01

This will reproduce the phase diagram experiment with the parameters of the paper:

- -en noisy: The experiment will be named noisy.
- -nf 0.01: :math:`\epsilon` is equal to 0.01 for a noise drawn from a sphere of radius :math:`\epsilon||Ax*||`.
- other parameters are explained above.

More help is available using the ``python run_exp_phase_transition.py -h`` command.
All techniques for speeding up the computation in the noiseless experiment are also available here.
Raw computation files are located in ``code/sksea/data_results`` folder.

When computations are done, adding ``-pl`` to the precedent command or running ::

    python run_exp_phase_transition.py -en noisy -na 500 -pl

will plot the results of the experiment in ``code/sksea/figures/icml`` with the parameters of the paper.

Thus you will find in ``code/sksea/figures/icml`` folder:

- ``noisy.svg`` which is the empirical support recovery phase transition diagram with the level curves of success probabilities 0.95 in the noisy case shown in the paper.
- ``noisy_zoom.svg`` which is the empirical support recovery phase transition diagram with the level curves of success probabilities 0.95 in the noisy case shown in the paper, zoomed in the region shown in the introduction of the paper.

Also, you will find in ``code/sksea/figures/comparison`` folder, ``noisy_0.0001_hm.html`` which is a draft version of the curve of the paper, with one curve by algorithm.
You might want to zoom in order to see the curves properly.

Deconvolution experiment
^^^^^^^^^^^^^^^^^^^^^^^^

Extensive experiment (noiseless)
""""""""""""""""""""""""""""""

Go to the ``code/sksea`` folder and run ::

    python run_exp_deconv.py -en noiseless

This will reproduce the extensive deconvolution experiment with the parameters of the paper:

- -en noiseless: The experiment will be named noiseless.

As in the phase diagram experiment, you can choose to run only a subset of the algorithms by using the ``-af`` option.
Here, by default, the value is set to ``-af OMPFAST -af ELSFAST -af OMPRFAST -af IHT -af IHT-els -af IHT-omp -af HTPFAST -af HTPFAST-els -af HTPFAST-omp -af SEAFAST -af SEAFAST-omp -af SEAFAST-els``.
You can also run each sparsity level using the ``-sp`` option along ``-ncp`` option to disable compilation.
For instance ``-sp 10 -sp 20 -sp 30 -ncp`` will run the experiment for k=10, k=20 and k=30 without compiling the results.
Then, you can run ``python run_exp_deconv.py -en noiseless -cp -nr`` to compile the results without running again the experiments.
More help is available using the ``python run_exp_deconv.py -h`` command.
Raw computation files are located in ``code/sksea/results/deconv/noiseless`` folder.

When computations are done, adding ``-pl`` to the precedent command or running ::

        python run_exp_deconv.py -en noiseless -pl

will plot the results of the experiment in ``code/sksea/results/deconv/noiseless/icml`` folder.
You will find:

- ``sup_dist.svg``: The mean of :math:`dist_{sup}` for all sparsity levels and all algorithms.
- ``sota.svg``: The mean of :math:`dist_{sup}` for a subset of the algorithms to reproduce the figure of the introduction of the paper.
- ``ws.svg``: The mean of the Wasserstein distance for all sparsity levels and all algorithms.
- ``f_mse_y.svg``: The mean of :math:`\ell_{2, \text{rel}\_\text{loss}}` for all sparsity levels and all algorithms.
- ``multi_n_supports_log.svg``: The mean of the number of explored supports for all sparsity levels and all algorithms in a log scale.
- ``multi_n_supports.svg``: The mean of the number of explored supports for all sparsity levels and all algorithms in a linear scale.

Extensive experiment (noisy)
""""""""""""""""""""""""""

Go to the ``code/sksea`` folder and run ::

        python run_exp_deconv.py -nf 0.1 -en noisy

This will reproduce the extensive deconvolution experiment with the parameters of the paper:

- -nf 0.1: :math:`\epsilon` is equal to 0.1 for a noise is drawn from a sphere of radius :math:`\epsilon||Ax*||`.
- -en noisy: The experiment will be named noisy.
- other parameters are explained above.

Raw computation files are located in ``code/sksea/results/deconv/noisy`` folder.
When computations are done, adding ``-pl`` to the precedent command or running ::

        python run_exp_deconv.py -en noisy -pl

will plot the results of the experiment in ``code/sksea/results/deconv/noisy/icml`` folder.
You will find the same files as in the noiseless experiment.

Precise experiment (noiseless)
""""""""""""""""""""""""""""""

Go to the ``code/sksea`` folder and run ::

    python exp_deconv.py -en noiseless

This will reproduce the deconvolution experiment whe k=20 with the parameters of the paper:

- -en noiseless: The experiment will be named noiseless.

When computations are done, you will find in ``code/sksea/figures/expe_deconv/noiseless/icml`` folder:

-``zoom_signal_light.svg``: The noiseless version of the cropped signal showed in the main part of the article.
-``zoom_signal_complete.svg``: The noiseless version of the cropped signal showed in appendices.
- ``full_signal.svg``: The noiseless version of the full signal in appendices.
- ``iter.svg``: The evolution of the loss through iterations for all algorithms.
- ``hist.svg``: The evolution of the loss through explored supports for all algorithms.


Precise experiment (noisy)
""""""""""""""""""""""""""""""

Go to the ``code/sksea`` folder and run ::

    python exp_deconv.py -en noisy -nf 0.1

This will reproduce the deconvolution experiment when k=20 with the parameters of the paper:

- -en noisy: The experiment will be named noisy.
- -nf 0.1: :math:`\epsilon` is equal to 0.1.

When computations are done, you will find in ``code/sksea/figures/expe_deconv/noisy/icml`` folder the same files as above

Machine learning experiment
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Go to the ``code/sksea`` folder and run ::

    python training_tasks.py

This will reproduce the phase diagram experiment with the parameters of the paper

By default, all datasets will used.
For instance, if you want to work on a specific dataset
(cal_housing, comp-activ-harder, slice, year, letter or ijcnn1),
you can use ``-d cal_housing -d letter`` to only work with cal_housing and letter.
You can also use ``-af`` to only run a subset of the algorithms.
Raw computations file are located in ``code/sksea/results/training_tasks``.

**IMPORTANT** The code download automatically the dataset used in `Sparse Convex Optimization via Adaptively Regularized Hard Thresholding`
by Axiotis, Kyriakos and Sviridenko, Maxim from Google Drive.
This download is needed only once.
For some reason, the download can fail and the code throws the following error:

FileNotFoundError: The dataset cannot be download automatically from Google Drive.
Please download it manualy from https://drive.google.com/file/d/1RDu2d46qGLI77AzliBQleSsB5WwF83TF/view,
and place it in code/sksea/downloads/datasets.zip.
Then, launch this command again.

In that case, please download manually the dataset from https://drive.google.com/file/d/1RDu2d46qGLI77AzliBQleSsB5WwF83TF/view
and place it in `code/sksea/downloads/datasets.zip`.

When computations are done, adding ``-pl`` to the precedent command or running ::

    python training_tasks.py -pl

will plot the results of the experiment in ``code/sksea/results/training_tasks_plot_icml``.
Here again, you choose the dataset you want the result from by adding ``-d``.

Running ``python training_tasks.py -pd`` will plot a draft version of the same figures in ``code/sksea/results/training_tasks_plot``.
